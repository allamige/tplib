(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module type T =
sig
  type t
  type vector_t
  type strategy_t = GEN | CONS | LAZY
  val dimension : t -> int
  val copy : t -> t
  val normalize : t -> t
  val set_strategy : strategy_t -> unit
  val get_strategy : unit -> strategy_t
  val apply_strategy : t -> t
  val of_cons : int -> vector_t list -> t
  val of_gen : int -> vector_t list -> t
  val to_cons : t -> vector_t list
  val to_gen : t -> vector_t list
  val is_leq : t -> t -> bool
  val sat_cons : t -> vector_t list -> bool
  val bottom : int -> t
  val is_bottom : t -> bool
  val top : int -> t
  val is_top : t -> bool
  val join : t -> t -> t
  val meet_cons : t -> vector_t list -> t
  val meet : t -> t -> t
  val assign : t -> int list -> vector_t list -> t
  val substitute : t -> int list -> vector_t list -> t
  val add_dimensions : t -> int array -> int -> bool -> t
  val remove_dimensions : t -> int array -> int -> t
  val permute_dimensions : t -> int array option -> t
  val widen : t -> t -> t
  val widen_generators : t -> t -> t
  val narrow : t -> t -> t
  val remove_redundant_constraints : t -> unit
  val print : Format.formatter -> t -> unit
  val polymake_print : string -> Format.formatter -> t -> unit
end

module Make (S: Semiring.T) (V: Vector.T with type scalar_t = S.t)  = struct

  module H = Halfspace.Make(S)(V)
  module Core = Tplib_core.Make(S)(V)(H)

  open S.Infixes

  type vector_t = V.t

  type t = { dim: int;
	     mutable gen: vector_t list option;
	     mutable cons: (bool * vector_t list) option;
	   }

  type strategy_t = GEN | CONS | LAZY

  let strategy = ref LAZY

  let dimension s = s.dim

  let cardinal s =
    let nb_gen = match s.gen with
      | None -> None
      | Some gen -> Some (List.length gen)
    and nb_cons = match s.cons with
      | None -> None
      | Some (_, cons) -> Some (List.length cons)
    in
      (nb_gen, nb_cons)

  let copy s =
    let gen = match s.gen with
      | None -> None
      | Some gen -> Some (List.map V.copy gen)
    in
    let cons = match s.cons with
      | None -> None
      | Some (b, cons) -> Some (b, (List.map V.copy cons))
    in
      { dim = s.dim; gen = gen; cons = cons }


  let support v =
    let rec support_aux i =
      if i >= V.size v then []
      else if V.get v i <>% S.zero then
	i::(support_aux (i+1))
      else
	support_aux (i+1)
    in
      support_aux 0

  let span_residuate g v idx = (* returns the list of index i of idx such that
				 * (im(g) * (im(g)\v))_i < v_i           *)
    let rec span_residuate_aux idx m =
      if idx = [] then []
      else
	match m with
	  | [] -> idx
	  | v'::m' ->
	      try
		let c = V.residuate v' v in
		let rec update_index res' idx =
		  match idx with
		    | [] -> span_residuate_aux res' m'
		    | i::idx' ->
			if (V.get v' i) *% c =% V.get v i then
			  update_index res' idx'
			else
			  update_index (i::res') idx'
		in
		  update_index [] idx
	      with S.Pinfty -> span_residuate_aux idx m'
    in
      span_residuate_aux idx g

  let rec filter g =
    match g with
      | [] -> []
      | v::g' when (V.is_null v) -> filter g'
      | v::g' ->
	  let dim = V.size v in
	  let x = V.get v 0 in
	    if (x <>% S.zero) && (x <>% S.one) then
	      V.mul v (~-% x) v;
	    v::(filter g')

  let belong g v =
    let idx = support v in
      (span_residuate g v idx = [])

  let weakbasis g =
    let rec weakbasis_aux l1 l2 =
      match l2 with
	| [] -> l1
	| v::l2'  ->
	    match l1@l2' with
	      | [] -> [v]
	      | g ->
		  (*if V.equal (span_residuate g v) v then *)
		  if belong g v then
		    weakbasis_aux l1 l2'
		  else
		    weakbasis_aux (v::l1) l2'
    in
      filter (weakbasis_aux [] g)

  let lift f = function
    | None -> None
    | Some x -> Some (f x)

  let normalize s =
    let gen' = match s.gen with
      | None -> None
      | Some gen -> Some (weakbasis gen)
    and cons' = match s.cons with
      | None -> None
      | Some (b,cons) -> Some (b, weakbasis cons)
    in
      {s with gen = gen'; cons = cons'}

  let add_vector (g0, (g, g_idx)) v =
    let idx = support v in
    let idx = span_residuate g0 v idx in
    let idx = span_residuate g v idx in
      if idx = [] then (g0, (g, g_idx))
      else
	let rec update_residuate ((res1, res2) as res) (g,g_idx) =
	  match (g,g_idx) with
	    | ([],[]) -> res
	    | (w::g', w_idx::g_idx') ->
		let w_idx' = span_residuate [v] w w_idx in
		  if w_idx' = [] then
		    update_residuate res (g',g_idx')
		  else
		    update_residuate (w::res1, w_idx'::res2) (g',g_idx')
	    | _ -> assert false
	in
	let (res1, res2) = update_residuate ([], []) (g, g_idx) in
	  (g0, (v::res1, idx::res2))

  let ineqsolve ?(with_order=false) dim gen cons =
    let rec split cons =
      match cons with
	| [] -> ([], [])
	| c::cons' ->
	    let v = V.make dim S.zero
	    and w = V.make dim S.zero in
	      for i = 0 to dim-1 do
		let x = V.get c i
		and y = V.get c (dim+i) in
		  if x >=% y then
		    V.set v i x
		  else
		    V.set w i y
	      done;
	      let (res1, res2) = split cons' in
		(v::res1,w::res2)
    in
    let partition a b gen = (* yields a triple (res1, res2, res3):
			     * res1 is the set {g | ag >= bg} provided with their residuates
			     * res2 is {(ag, g) | ag >= bg}
			     * res3 is {(bg, g) | ag < bg} *)
      let p = ref 0 and q = ref 0 in
      let partition_aux (res1, res2, res3) g =
	let x = V.scalar a g
	and y = V.scalar b g in
	let res1' = if x >=% y then g::res1 else res1
	and res2' = if x >=% y then (incr p; (x,g)::res2) else res2
	and res3' = if x <% y then (incr q; (y,g)::res3) else res3 in
	  (res1', res2', res3')
      in
      let ((res1, res2, res3) as res) = List.fold_left partition_aux ([], [], []) gen in
	((!p)*(!q),res)
    in
    let combine res gen1 gen2 = (* yields the combination y.g + x.h for any
				 * (x, g) in gen1 and (y, h) in gen2, and then
				 * concatenates it to res                         *)
      let rec combine_aux res1 gen1 =
	match gen1 with
	  | [] -> res1
	  | (x,_)::gen1' when x =% S.zero -> combine_aux res1 gen1'
	  | (x,g)::gen1' ->
	      let rec combine_aux2 res2 gen2 =
		match gen2 with
		  | [] -> combine_aux res2 gen1'
		  | (y,h)::gen2' ->
		      let z = ~-% (x+%y) in
		      let x' = z *% x and y' = z *% y in
		      let v = V.make dim S.zero
		      and v' = V.make dim S.zero in
			V.mul v y' g;
			V.mul v' x' h;
			if (V.is_leq v' v) then
			  combine_aux2 res2 gen2'
			else begin
			  V.add v v v';
			  combine_aux2 (add_vector res2 v) gen2'
			end
	      in
		combine_aux2 res1 gen2
      in
      let (res', (res'', _)) = combine_aux (res, ([], [])) gen1 in
	List.rev_append res' res''
    in
    let extract_minimum gen (cons1, cons2) =
      let rec extract_minimum_aux gen (min, part_res, a, b) (sup1, sup2) (cons1,cons2) =
	match (cons1, cons2) with
	  | ([], []) -> (part_res, (sup1, sup2))
	  | (c1::cons1', c2::cons2') ->
	      let (x, y) = partition c1 c2 gen in
		if x < min then
		  extract_minimum_aux gen (x, y, c1, c2) (a::sup1, b::sup2) (cons1', cons2')
		else
		  extract_minimum_aux gen (min, part_res, a, b) (c1::sup1, c2::sup2) (cons1', cons2')
	  | _ -> assert false
      in
	match (cons1, cons2) with
	  | ([], []) -> assert false
	  | (a::cons1', b::cons2') ->
	      let (min, part_res) = partition a b gen in
		if with_order then
		  extract_minimum_aux gen (min, part_res, a, b) ([], []) (cons1', cons2')
		else
		  (part_res, (cons1', cons2'))
	  | _ -> assert false
    in
    let rec ineqsolve_aux gen cons =
      match cons with
	| ([],[]) -> gen
	| _ ->
	    let ((res1, res2, res3), cons') =  extract_minimum gen cons in
	    let gen' = combine res1 res2 res3 in
	      ineqsolve_aux gen' cons'
    in
      ineqsolve_aux gen (split cons)


  let identity n =
    let rec identity_aux i =
      if i >= n then []
      else
	(V.can n i)::(identity_aux (i+1))
    in
      identity_aux 0

  let gen_of_cons dim cons =
    Core.compute_ext_rays ~with_order:false dim cons

  let cons_of_gen dim gen =
    (true, Core.compute_ext_rays_polar ~with_order:false dim gen)

  let build_gen s =
    match (s.gen, s.cons) with
      | (Some gen, _) -> gen
      | (None, Some (_, cons)) ->
	  let gen = gen_of_cons s.dim cons in
	    s.gen <- Some gen; gen
      | _ -> assert false

  let build_cons s =
    match (s.gen, s.cons) with
      | (_, Some (_, cons)) -> cons
      | (Some gen, None) ->
	  let cons = cons_of_gen s.dim gen in
	    s.cons <- Some cons; snd cons
      | _ -> assert false

  let set_strategy x = strategy := x

  let get_strategy () = !strategy

  let apply_strategy s =
    match !strategy with
      | LAZY -> s
      | GEN -> ignore (build_gen s); s
      | CONS -> ignore (build_cons s); s

  let of_cons dim cons =
    apply_strategy {dim = dim; gen = None; cons = Some (false, weakbasis cons)}

  let of_gen dim gen =
    apply_strategy {dim = dim; gen = Some (weakbasis gen); cons = None}

  let to_cons s = build_cons s
  let to_gen s = build_gen s

  let sat_cons s1 cons2 =
    match s1.cons with
      | Some (true, cons1) -> List.for_all (belong cons1) cons2
      | Some (false, cons1) when List.for_all (belong cons1) cons2 -> true
      | _ ->
	  let gen1 = build_gen s1 in
	  let rec split cons2 =
	    match cons2 with
	      | [] -> []
	      | c2::cons2' ->
		  let v = V.make s1.dim S.zero
		  and w = V.make s1.dim S.zero in
		    for i = 0 to s1.dim-1 do
		      V.set v i (V.get c2 i);
		      V.set w i (V.get c2 (s1.dim+i))
		    done;
		    (v,w)::(split cons2')
	  in
	  let cons2' = split cons2 in
	  let sat_cons_aux v =
	    List.for_all (fun (x,y) -> (V.scalar x v >=% V.scalar y v)) cons2'
	  in
	    List.for_all sat_cons_aux gen1

  let is_leq s1 s2 =
    assert (s1.dim = s2.dim);
    match (s1.gen, s2.gen) with
      | (Some gen1, Some gen2) ->
	  List.for_all (belong gen2) gen1
      | (_, None) -> (* if that case, cons2 is available *)
	  let cons2 = build_cons s2 in
	    sat_cons s1 cons2
      | (None, Some gen2) ->
	  match s2.cons with
	    | Some (_, cons2) -> sat_cons s1 cons2
	    | None ->
		let gen1 = build_gen s1 in
		  List.for_all (belong gen2) gen1

  let bottom dim =
    let gen = V.make dim S.zero in
    let cons = identity (2*dim) in
      apply_strategy {dim = dim; gen = Some [gen]; cons = Some (true, cons)}

  let is_bottom s =
    let s' = bottom s.dim in
      is_leq s s'

  let tautology dim =
    let cons = Array.init (2*dim) (fun _ -> V.make (2*dim) S.zero) in
      for i = 0 to dim-1 do
	V.set cons.(i) i S.one;
	V.set cons.(dim+i) i S.one;
	V.set cons.(dim+i) (dim+i) S.one
      done;
      (true, Array.to_list cons)

  let top dim =
    let gen = identity dim
    and cons = tautology dim in
      apply_strategy {dim = dim; gen = Some gen; cons = Some cons}

  let is_top s =
    let s' = top s.dim in
      is_leq s' s

  let join s1 s2 =
    assert (s1.dim = s2.dim);
    let gen1 = build_gen s1
    and gen2 = build_gen s2 in
      apply_strategy {dim = s1.dim; gen = Some (weakbasis (List.rev_append gen1 gen2)); cons = None}


  let meet_cons s1 cons2 =
    let gen1' = match s1.gen with
      | None -> None
      | Some gen1 -> Some (ineqsolve s1.dim gen1 cons2)
    in
    let cons1' = match s1.cons with
      | None -> None
      | Some (_, cons1) -> Some (false, weakbasis (List.rev_append cons1 cons2))
    in
      apply_strategy {dim = s1.dim; gen = gen1'; cons = cons1'}

  let meet s1 s2 =
    assert (s1.dim = s2.dim);
    match (s1.gen, s1.cons, s2.gen, s2.cons) with
      | (Some gen1, None, Some gen2, None) ->
	  let cons1 = build_cons s1
	  and cons2 = build_cons s2 in
	    apply_strategy {dim = s1.dim; gen = None; cons = Some (false, weakbasis (List.rev_append cons1 cons2))}
      | (_, _, _, Some (_, cons2)) -> apply_strategy (meet_cons s1 cons2)
      | (_, Some (_, cons1), _, _) -> apply_strategy (meet_cons s2 cons1)
      | _ -> assert false

  let assign s d e =
    let gen = build_gen s in
    let rec assign_vector d e v = (* perform the assignment on a vector v *)
      match (d,e) with
	| ([], []) -> V.copy v
	| (dim::d', expr::e') ->
	    let w = assign_vector d' e' v in
	      V.set w dim (V.scalar expr v);
	      w
	| (_, _) -> assert false
    in
    let assign_aux res v =
	add_vector res (assign_vector d e v)
    in
      if List.length d > 0 then
	let (res1, (res2, _)) = List.fold_left assign_aux ([], ([], [])) gen in
	apply_strategy {s with gen = Some (List.rev_append res1 res2); cons = None }
      else
	apply_strategy s

  let substitute s d e =
    let t = Array.init s.dim (fun _ -> V.make s.dim S.zero) in
      for i = 0 to s.dim-1 do
	V.set t.(i) i S.one;
      done;
      let rec build_expr d e =
	match (d, e) with
	  | ([], []) -> ()
	  | (dim::d', expr::e') ->
	      t.(dim) <- expr;
	      build_expr d' e'
	  | _ -> assert false
      in
      let substitute_aux v =
	let p = V.size v in
	let w = V.make p S.zero in
	  for i = 0 to s.dim-1 do
	    let w1 = V.make (2*s.dim) S.zero
	    and w2 = V.make (2*s.dim) S.zero in
	      V.concat w1 (t.(i)) (V.make s.dim S.zero);
	      V.concat w2 (V.make s.dim S.zero) (t.(i));
    	      V.addmul w (V.get v i) w1;
    	      V.addmul w (V.get v (s.dim+i)) w2
	  done;
	  w
      in
	if List.length d > 0 then
	  let cons = build_cons s in
	    build_expr d e;
	    let cons' = weakbasis (List.map substitute_aux cons) in
	      apply_strategy {s with gen = None; cons = Some (false, cons')}
	else
	  apply_strategy s

  let add_dimensions s dim intdim constrained =
    let newdim = s.dim+intdim in
      if intdim = 0 then
	apply_strategy s
      else
	let gen' =
	  match s.gen with
	    | None -> None
	    | Some gen ->
		let gen1 = List.map (fun v -> let w = V.make newdim S.zero in
				       V.add_dimensions w v dim intdim; w) gen in
		  if constrained then
		    Some gen1
		  else
		    let gen2 = Array.init intdim (fun _ -> V.make newdim S.zero) in
		    let k = ref (intdim-1) in
		      begin
			try
			  for i = s.dim downto 0 do
			    while (!k >= 0) && (dim.(!k) = i) do
			      V.set gen2.(!k) (i+ !k) S.one;
			      k := !k-1
			    done;
			    if !k < 0 then
			      raise Exit
			  done
			with Exit -> ()
		      end;
		      let gen' = List.rev_append gen1 (Array.to_list gen2) in
			Some gen'
	in
	let cons' =
	  match s.cons with
	    | None -> None
	    | Some (_, cons) ->
		let intdim' = 2*intdim
		and newdim' = 2*newdim in
		let dim' = Array.make intdim' 0 in
		  for i = 0 to intdim-1 do
		    dim'.(i) <- dim.(i);
		    dim'.(intdim+i) <- s.dim+dim.(i)
		  done;
		  let cons1 = List.map (fun v -> let w = V.make newdim' S.zero in
					  V.add_dimensions w v dim' intdim'; w) cons in
		    if not constrained then
		      Some (false, cons1)
		    else
		      let cons2 = Array.init intdim' (fun _ -> V.make newdim' S.zero) in
		      let k = ref (intdim'-1) in
			begin
			  try
			    for i = 2*s.dim downto 0 do
			      while (!k >= 0) && (dim'.(!k) = i) do
				V.set cons2.(!k) (i+ !k) S.one;
				k := !k-1
			      done;
			      if !k < 0 then
				raise Exit
			    done
			  with Exit -> ()
			end;
			Some (false, List.rev_append cons1 (Array.to_list cons2))
	in
	  apply_strategy {dim = newdim; gen = gen'; cons = cons'}

  let remove_dimensions s dim intdim =
    if intdim > 0 then
      let newdim = s.dim-intdim in
      let gen = build_gen s in
      let gen' = List.map (fun v -> let w = V.make newdim S.zero in
			     V.remove_dimensions w v dim intdim; w) gen in
      let gen'' = weakbasis gen' in
	apply_strategy {dim = newdim; gen = Some gen''; cons = None}
    else
      apply_strategy s

  let permute_dimensions s perm =
    match perm with
      | None -> s
      | Some perm ->
	  assert (s.dim = Array.length perm);
	  let gen' =
	    match s.gen with
	      | None -> None
	      | Some gen ->
		  Some (List.map (fun v -> let w = V.make s.dim S.zero in
				    V.permute_dimensions w v perm; w) gen)
	  in
	  let cons' =
	    match s.cons with
	      | None -> None
	      | Some (_, cons) ->
		  let perm' = Array.make (2*s.dim) 0 in
		    for i = 0 to s.dim-1 do
		      perm'.(i) <- perm.(i);
		      perm'.(i+s.dim) <- perm.(i)+s.dim
		    done;
		    Some (false, List.map (fun v -> let w = V.make s.dim S.zero in
					     V.permute_dimensions w v perm'; w) cons)
	  in
	    apply_strategy {dim = s.dim; gen = gen'; cons = cons'}

  let widen s1 s2 =
    assert (s1.dim = s2.dim);
    let cons1 = build_cons s1 in
      let widen_aux res x =
	if sat_cons s2 [x] then x::res
	else res
      in
      let cons = List.fold_left widen_aux [] cons1 in
	apply_strategy {dim = s1.dim; gen = None; cons = Some (false, cons)}

  let widen_generators s1 s2 =
    assert (s1.dim = s2.dim);
    let gen1 = build_gen s1
    and gen2 = build_gen s2 in
    let widen_aux v =
      let idx = span_residuate gen1 v (support v) in
      let res = V.make s1.dim S.zero in
	List.iter (fun i -> V.set res i (V.get v i)) idx;
	res
    in
    let gen2' = List.map widen_aux gen2 in
    let gen = Some (weakbasis (List.rev_append gen1 gen2')) in
      apply_strategy {dim = s1.dim; gen = gen; cons = None}

  let narrow s1 s2 =
    assert (s1.dim = s2.dim);
    let gen1 = build_gen s1 in
    let narrow_aux res x =
      if is_leq (of_gen s1.dim [x]) s2 then x::res
      else res
    in
    let gen = List.fold_left narrow_aux [] gen1 in
      apply_strategy {dim = s1.dim; gen = Some gen; cons = None}

  let remove_redundant_constraints s =
    let cons = build_cons s in
    let dim' = 2*s.dim in
    let rec remove_tautology cons =
      match cons with
	| [] -> []
	| c::cons' ->
	    let res = ref true in
	      for i = 0 to s.dim-1 do
		if V.get c i <% V.get c (s.dim+i) then
		  res := false
	      done;
	      if !res then remove_tautology cons'
	      else c::(remove_tautology cons')
    in
    let simplify_constraint c =
      let cst = ref S.zero in
	try
	  for i = dim'-1 downto 0 do
	    let x = V.get c i in
	      if x <>% S.zero then begin
		cst := S.neg x; raise Exit
	      end
	  done
	with Exit -> V.mul c !cst c
    in
    let rec remove_constraint cons1 cons2 =
      match cons2 with
	| [] -> cons1
	| c::cons2' ->
	    let s' = of_cons s.dim (cons1@cons2') in
	      if is_leq s' s then
		remove_constraint cons1 cons2'
	      else
		remove_constraint (c::cons1) cons2'
    in
    let cons' = remove_tautology cons in
      List.iter simplify_constraint cons';
      s.cons <- Some (false, remove_constraint [] cons')

  let print fmt p =
    let gen = build_gen p in
    List.iter (fun v -> Format.fprintf fmt "%s@." (V.to_string v)) gen

  let polymake_print name fmt p =
    Format.fprintf fmt "$%s = new TropicalPolytope;@." name;
    Format.fprintf fmt "$%s->POINTS=<<\".\";@." name;
    List.iter (fun v -> Format.fprintf fmt "%a@." V.polymake_print v) (to_gen p);
    Format.fprintf fmt ".@."
end
