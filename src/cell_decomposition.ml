(*
  TPLib: Tropical Polyhedra Library 

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module type T = 
sig
  type vector_t
  type cell_t 
  type type_t = (vector_t list) array
  type t = { bounded: cell_t list;
             unbounded: cell_t list }

  val compute_cell_decomposition : int -> vector_t list -> t
  val compute_bounded_cells : ?is_pure:bool -> int -> vector_t list -> cell_t list
  val compute_boundary_cells : ?is_pure:bool -> int -> vector_t list -> cell_t list
  val compute_pseudo_vertices : ?is_pure:bool -> int -> vector_t list -> vector_t list 
  val compute_type_of_vector : vector_t list -> vector_t -> type_t
  val compute_type_of_cell : vector_t list -> cell_t -> type_t
  val compute_extremality_type : vector_t list -> vector_t -> int list
  val hull_dimension : cell_t list -> int
  val is_pure : cell_t list -> bool
  val is_in_boundary : type_t -> bool
  val print_type : Format.formatter -> ?env:(vector_t * int) list -> type_t -> unit
end

module Make (S: Semiring.T) (V: Vector.T with type scalar_t = S.t) 
  (C: Polytrope.T with type scalar_t = S.t and type vector_t = V.t) = struct

    type vector_t = V.t
    type cell_t = C.t
    type type_t = (vector_t list) array

    open S.Infixes

    type t = { bounded: cell_t list;
               unbounded: cell_t list }

    module Internal (S: Semiring.T) (V: Vector.T with type scalar_t = S.t) 
      (C: Polytrope.T with type scalar_t = S.t and type vector_t = V.t) = struct
          
        open S.Infixes
          
        let compute_cell_decomposition dim gen0 =
          match gen0 with
            | [] -> ([], [C.top dim])
            | g0::gen ->
                let build_sector_cons g j = 
                  let x = ~-% (V.get g j) in
                  let m = C.alloc dim in
                  for i = 0 to (dim-1) do
                    C.set dim m i i S.one;
                    C.set dim m i j (x *% (V.get g i))
                  done;
                  C.of_matrix ~is_closed:true dim m
                in
                let have_full_dimensional_intersection c g k = 
                  match C.to_matrix c with
                    | None -> false
                    | Some m -> 
                      (* since c is full dimensional, it has a unique generator
                       * extreme of type k (in the min-plus sense). 
                       * More precisely, this is the maximal element for the p.o. <=_k
                       * where x <=_k y <=> x_j - x_k <= y_j - y_k for all j
                       * It suffices to check that this generator satisfy the min-plus
                       * half-space constraint in a strict manner *)
                        try
                          for j = 0 to (dim-1) do
                            if j <> k && (C.get dim m k j) *% (V.get g j) >=% 
                              (V.get g k) then
                              raise Exit
                          done;
                          true
                        with Exit -> false 
                in      
                let refine_decomposition cells g = 
                  let rec refine_decomposition_aux refined_cells i =
                    if i >= dim then
                      refined_cells
                    else
                      let refined_cells' = 
                        List.fold_left (fun res cell -> 
                          if have_full_dimensional_intersection cell g i then 
                            let cell' = build_sector_cons g i in
                            let cell'' = C.meet cell cell' in
                            cell''::res
                          else 
                            res)
                          refined_cells cells
                      in
                      refine_decomposition_aux refined_cells' (i+1) 
                  in
                  refine_decomposition_aux [] 0
                in
                let rec build_initial_cells res j = 
                  if j >= dim then res
                  else
                    build_initial_cells ((build_sector_cons g0 j)::res) (j+1)
                in
                let cells0 = build_initial_cells [] 0 in
                let cells = List.fold_left refine_decomposition cells0 gen in
                List.partition C.is_bounded cells

      end
      
    module I = Internal(S)(V)(C)
      
    let compute_cell_decomposition dim gen = 
      let (bounded, unbounded) = I.compute_cell_decomposition dim gen in
      { bounded = bounded; unbounded = unbounded }

    let compute_extremality_type gen v = 
      (* gen should not contain any vector proportional to v *)
      let dim = V.size v in
      let is_minimal_of_type i =
        let rec is_minimal_of_type_aux j w =
	  if j >= dim then
	    false
	  else
	    if (V.get v j) *% (~-% (V.get v i)) 
              <% (V.get w j) *% (~-% (V.get w i)) then
	      true
	    else
	      is_minimal_of_type_aux (j+1) w
        in
        List.for_all (fun w -> 
          V.hilbert_distance v w =% S.one || is_minimal_of_type_aux 0 w) gen 
      in
      let rec compute_extremality_type_aux res i =
        if i < 0 then res
        else
          let res' = if is_minimal_of_type i then i::res else res in
          compute_extremality_type_aux res' (i-1)
      in
      compute_extremality_type_aux [] (dim-1)
        
        
    module G = Germ.Make(S)

    module V_germ = Vector.Make(G)
    module C_germ = Polytrope.Make(G)(V_germ)
    module I_germ = Internal(G)(V_germ)(C_germ)
 
    let collect_maximal_cells l c =
      let rec collect_maximal_cells_aux res l = 
        match l with
          | [] -> res
          | c'::l' when C.is_leq c' c -> collect_maximal_cells_aux res l'
          | c'::l' -> collect_maximal_cells_aux (c'::res) l'
      in
      if List.exists (C.is_leq c) l then l
      else c::(collect_maximal_cells_aux [] l)
        
    let compute_bounded_cells ?(is_pure=false) dim gen = 
      if is_pure then
        fst (I.compute_cell_decomposition dim gen)
      else
        let to_germ_vector res gen' x =
          let t = compute_extremality_type gen' x in
          let epsilon = G.of_scalar2 S.one (S.of_int (-1)) in
	  let rec apply_perturbation y t =
	    match t with
	      | [] -> assert false (* the vector v should be extreme, 
				    * hence minimal for a given type *)
	      | [_] -> y::res
	      | i::t' ->
       		  let z = V_germ.copy y in
                  V_germ.set z i (G.mul epsilon (V_germ.get y i));
		  y::(apply_perturbation z t')
	  in
          let y = V_germ.make dim G.zero in
          for i = 0 to (dim-1) do
            V_germ.set y i (G.of_scalar (V.get x i))
          done;
          apply_perturbation y t
        in
        let to_germ_vector_set () = 
          let rec to_germ_vector_set_aux res gen1 gen2 =
            match gen2 with
              | [] -> res
              | g::gen2' -> 
                  let gen' = List.rev_append gen1 gen2' in
                  let res' = to_germ_vector res gen' g in
                  to_germ_vector_set_aux res' (g::gen1) gen2'
          in
          to_germ_vector_set_aux [] [] gen
        in
        let of_germ_cell c =
          match C_germ.to_matrix c with
            | None -> C.bottom dim
            | Some m -> 
                let m' = Array.map G.to_scalar m in
                C.of_matrix ~is_closed:true dim m' 
        in
        (*Format.printf "Conversion...@.";*)
        let gen' = to_germ_vector_set () in
        (*Format.printf "Computation over germs@.";*)
        let (germ_cells,_) = I_germ.compute_cell_decomposition dim gen' in
        (*Format.printf "Back to max-plus@.";*)
        let cells = List.map of_germ_cell germ_cells in
        (*Format.printf "Eliminination of redundant cells@.";*)
        List.fold_left collect_maximal_cells [] cells
     
    let compute_pseudo_vertices ?(is_pure=false) dim gen =
      let cplx = compute_bounded_cells ~is_pure dim gen in
      List.fold_left (fun res cell -> 
        List.fold_left (fun res' v ->
          if List.exists (fun w -> V.hilbert_distance v w =% S.one) res' then
            res'
          else
            v::res') res (C.pseudo_vertices cell)) [] cplx
      
    let compute_type_of_vector gen v = 
      (* computes the min-plus type of v with respect to 
       * vectors in the list gen *)
      let dim = V.size v in
      let res = Array.init dim (fun _ -> []) in
      let argmin g = 
        let rec argmin_aux min arg i =
          if i < 0 then arg
          else
            let x = (V.get v i) *% (~-% (V.get g i)) in
            match S.compare x min with
              | res when res < 0 -> argmin_aux x [i] (i-1)
              | 0 -> argmin_aux min (i::arg) (i-1)
              | _ -> argmin_aux min arg (i-1)
        in
        let x0 = (V.get v (dim-1)) *% (~-% (V.get g (dim-1))) in
        argmin_aux x0 [dim-1] (dim-2)
      in
      let compute_type_aux g = 
        let arg = argmin g in
        List.iter (fun i -> res.(i) <- g::res.(i)) arg
      in
      List.iter compute_type_aux gen;
      res

    let compute_type_of_cell gen c =
      let dim = C.dimension c in
      match C.to_matrix c with
        | None -> Array.init dim (fun _ -> gen)
        | Some m ->
            let scalar g j = (* computes the min-plus inner 
                              * product of M(:,j) with -g *)
              let rec scalar_aux res i = 
                if i >= dim then res
                else 
                  let x = (C.get dim m i j) *% (~-% (V.get g i)) in
                  scalar_aux (S.min x res) (i+1)
              in
              let x0 = (C.get dim m 0 j) *% (~-% (V.get g 0)) in
              scalar_aux x0 1 
            in
            let res = Array.init dim (fun _ -> []) in
            for j = 0 to (dim-1) do
              (* the j-type of the cell is provided by 
                 the j-type of the j-th column *)
              List.iter (fun g -> 
                let x = scalar g j in
                if ((C.get dim m j j) *% (~-% (V.get g j))) =% x then
                  res.(j) <- g::res.(j)) gen
            done;
            res

    let is_in_boundary t =
      let dim = Array.length t in
      let is_in_boundary_aux1 () =
        try
          for i = 0 to (dim-1) do
            match t.(i) with
              | [] -> raise Exit
              | _ -> ()
          done;
          true
        with Exit -> false
      in
      if is_in_boundary_aux1 () then
        let is_in_boundary_aux2 i g =
          try
            for j = 0 to (dim-1) do
              if j <> i && 
                List.exists (V.projectively_equal g) t.(j) then
                raise Exit
            done;
            false
          with Exit -> true
        in
        try
          for i = 0 to (dim-1) do
            if List.for_all (is_in_boundary_aux2 i) t.(i) then
              raise Exit
          done;
          false
        with Exit -> true
      else
        false

    let compute_boundary_cells ?(is_pure=false) dim gen =
      let compute_boundary_cells_aux res c =
        let l = C.facets c in
        List.fold_left (fun res f -> 
          let t = compute_type_of_cell gen f in
          if is_in_boundary t then f::res
          else res) res l
      in
      let (cells1, cells2) = List.partition C.is_full_dimensional 
        (compute_bounded_cells ~is_pure dim gen) in
      let cells1' = List.fold_left compute_boundary_cells_aux [] cells1 in
      List.fold_left collect_maximal_cells [] (List.rev_append cells2 cells1')
        (* TODO: collect_maximal_cells may not be useful
         * when comparing cells1' and cells2 *)

  let hull_dimension l = 
    match l with
      | [] -> -1
      | c::l' -> 
          List.fold_left (fun res c' -> max res (C.hull_dimension c')) 
            (C.hull_dimension c) l'

  let is_pure l = 
    match l with
      | [] -> false
      | c::l' -> 
          let dim = C.dimension c in
          List.for_all (fun c -> C.hull_dimension c = (dim-1)) l

  let print_type fmt ?env t = 
    let print_vector fmt v = 
      match env with
        | None -> Format.fprintf fmt "%s" (V.to_string v)
        | Some env -> Format.fprintf fmt "%d" (List.assoc v env)
    in
    let dim = Array.length t in
    for i = 0 to (dim-1) do
      Format.fprintf fmt "T_%d = { @?" i;
      List.iter (Format.fprintf fmt "%a @?" print_vector) t.(i);
      Format.fprintf fmt "}@."
    done
            
end
