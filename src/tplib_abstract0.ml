(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)


module N = (val (Numeric.of_string Numeric.default_exact_rational_module): Numeric.T)
module Scalar = Semiring.Make(N)
open Scalar.Infixes

module Vector0 = Vector.Make(Scalar)

module Linexpr0 = struct
  type t = Vector0.t

  let make dim = Vector0.make (dim+1) Scalar.zero

  let copy = Vector0.copy
    
  let get_size e = 
    assert (Vector0.size e > 0);
    (Vector0.size e)-1

  let get_cst e = Vector0.get e ((Vector0.size e)-1)
    
  let get_coeff e d = 
    assert (d < (Vector0.size e)-1);
    Vector0.get e d
      
  let set_cst e = Vector0.set e ((Vector0.size e)-1)
    
  let set_coeff e d c = 
    assert (d < (Vector0.size e)-1);
    Vector0.set e d c
      
  let set_list e l c =
    List.iter (fun (x,y) -> set_coeff e y x) l;
    match c with
      | Some c' -> set_cst e c'
      | None -> ()
	  
  let set_array e a c =
    Array.iter (fun (x,y) -> set_coeff e y x) a;
    match c with
      | Some c' -> set_cst e c'
      | None -> ()
	
  let of_list dim l c =
    let e = make dim in
      set_list e l c; e
	
  let of_array dim a c =
    let e = make dim in
      set_array e a c; e
	
  let iter f e = 
    Vector0.iter f 0 ((Vector0.size e)-1) e
      
  let residuate = Vector0.residuate

  let nb_of_terms expr =
    let res = ref 0 in
      iter (fun coeff var -> 
	      if coeff <>% Scalar.zero then
		res := !res+1) expr;
      if (get_cst expr) <>% Scalar.zero then
	res := !res+1;
      !res

  let print assoc fmt expr = 
    Format.fprintf fmt "@[<hov>";
    let n = nb_of_terms expr in
      if n = 0 then
	Format.pp_print_string fmt "-oo"
      else begin
	if n > 1 then
	  Format.pp_print_string fmt "max(";
	begin
	  let first = ref true in
	    iter
	      (fun coeff dim ->
		 if (coeff <>% Scalar.zero) then begin
		   if not !first then begin
		     Format.fprintf fmt "@,";
		     Format.pp_print_string fmt ","
		   end;
		   Format.pp_print_string fmt (assoc dim);
		   if (coeff >% Scalar.one) then 
		     Format.pp_print_string fmt "+";
		   if (coeff <>% Scalar.one) then
		     Format.pp_print_string fmt (Scalar.to_string coeff);
		   first := false
		 end) expr;
	    let cst = get_cst expr in
	      if cst <>% Scalar.zero then begin
		if not !first then begin 
		  Format.fprintf fmt "@,";
		  Format.pp_print_string fmt ","
		end;
		Format.pp_print_string fmt (Scalar.to_string cst)
	      end;
	      Format.fprintf fmt "@]";
	end;
	if n > 1 then
	  Format.pp_print_string fmt ")"
      end
end

module Lincons0 = struct
  type typ = EQ | SUPEQ 
      
  type t = { mutable left0: Linexpr0.t;
	     mutable right0: Linexpr0.t;
	     mutable typ: typ
	   }
      
  let make e1 e2 typ = {left0 = e1; right0 = e2; typ = typ}
    
  let copy c = {left0 = c.left0; right0 = c.right0; typ = c.typ}
    
  let string_of_typ typ = 
    match typ with
      | EQ -> "="
      | SUPEQ -> ">="
	  
  let print assoc fmt c =
    Linexpr0.print assoc fmt c.left0;
    Format.pp_print_string fmt (string_of_typ c.typ);
    Linexpr0.print assoc fmt c.right0
end

module Generator0 = struct
  
  type typ = RAY | VERTEX

  type t = { mutable linexpr0: Linexpr0.t;
	     mutable typ: typ }

  let make e typ = {linexpr0 = e; typ = typ}

  let copy g = {linexpr0 = Linexpr0.copy g.linexpr0; typ = g.typ}
    
  let string_of_typ typ =
    match typ with
      | RAY -> "RAY"
      | VERTEX -> "VTX"
	  
  let residuate g1 g2 =
    let e1 = Linexpr0.copy g1.linexpr0 
    and e2 = Linexpr0.copy g2.linexpr0 in
      begin
	match g1.typ with
	  | VERTEX -> Linexpr0.set_cst e1 Scalar.one
	  | RAY -> Linexpr0.set_cst e1 Scalar.zero
      end;
      begin
	match g2.typ with
	  | VERTEX -> Linexpr0.set_cst e2 Scalar.one
	  | RAY -> Linexpr0.set_cst e2 Scalar.zero
      end;
      Linexpr0.residuate e1 e2
	
  let print assoc fmt g =
    Format.fprintf fmt "@[<hov>";
    begin 
      match g.typ with
	| RAY -> Format.pp_print_string fmt "RAY:["
	| VERTEX -> Format.pp_print_string fmt "VTX:["
    end;
    let first = ref true in
      Linexpr0.iter
	(fun coeff dim ->
	   if not !first then begin
	     Format.fprintf fmt "@,";
	     Format.pp_print_string fmt "; "
	   end;
	   Format.pp_print_string fmt ((assoc dim)^":"^(Scalar.to_string coeff));
	   first := false) g.linexpr0;
      Format.pp_print_string fmt "]"
end

module Poly0 = struct
  module P = Tplib_abstract.Make(Scalar)(Vector0)

  open P

  type t = P.t

  let vector0_of_linexpr0 (e:Linexpr0.t) = (e:Vector0.t)
  let linexpr0_of_vector0 (e:Vector0.t) = (e:Linexpr0.t)
    
  let vector0_of_lincons0 c =
    let left0 = vector0_of_linexpr0 c.Lincons0.left0
    and right0 = vector0_of_linexpr0 c.Lincons0.right0 in
      match c.Lincons0.typ with
	| Lincons0.SUPEQ -> 
	    let v = Vector0.make ((Vector0.size left0)+(Vector0.size right0)) Scalar.zero in
	      Vector0.concat v left0 right0; [v]
	| Lincons0.EQ ->
	    let v1 = Vector0.make ((Vector0.size left0)+(Vector0.size right0)) Scalar.zero 
	    and v2 = Vector0.make ((Vector0.size left0)+(Vector0.size right0)) Scalar.zero in
	      Vector0.concat v1 left0 right0; 
	      Vector0.concat v2 right0 left0; 
	      [v1; v2]
		
  let lincons0_of_vector0 v =
    assert ((Vector0.size v) mod 2 = 0);
    let s = (Vector0.size v)/2 in
    let left0 = Vector0.make s Scalar.zero
    and right0 = Vector0.make s Scalar.zero in
      for i = 0 to s-1 do
	Vector0.set left0 i (Vector0.get v i);
	Vector0.set right0 i (Vector0.get v (s+i))
      done;
      Lincons0.make (linexpr0_of_vector0 left0) (linexpr0_of_vector0 right0) Lincons0.SUPEQ
	
  let generator0_of_vector0 v =
    assert (Vector0.size v > 0);
    let w = Vector0.make (Vector0.size v) Scalar.zero in
    let c = Vector0.get v ((Vector0.size v)-1) in
    let (c',typ) = 
      if Scalar.compare c Scalar.zero = 0 then 
	(Scalar.one, Generator0.RAY) 
      else 
	(Scalar.neg c, Generator0.VERTEX) 
    in
      for i = 0 to (Vector0.size v)-2 do
	Vector0.set w i (Scalar.mul (Vector0.get v i) c')
      done;
      Generator0.make (linexpr0_of_vector0 w) typ
	
  let vector0_of_generator0 g =
    let v = vector0_of_linexpr0 g.Generator0.linexpr0 in
    let w = Vector0.make (Vector0.size v) Scalar.zero in
      for i = 0 to (Vector0.size v)-2 do
	Vector0.set w i (Vector0.get v i)
      done;
      begin
	match g.Generator0.typ with
	  | Generator0.VERTEX -> Vector0.set w ((Vector0.size v)-1) Scalar.one
	  | Generator0.RAY -> Vector0.set w ((Vector0.size v)-1) Scalar.zero
      end;
      w
	
  type strategy_t = P.strategy_t = GEN | CONS | LAZY
  let set_strategy = set_strategy
  let get_strategy = get_strategy
    
  let copy = copy
  let dimension p = (dimension p)-1
    
  let bottom dim = bottom (dim+1)
  let top dim = top (dim+1)
    
  let is_bottom = is_bottom
  let is_top = is_top
  let is_leq = is_leq
    
  let sat_lincons p c = sat_cons p (vector0_of_lincons0 c)
  let to_lincons_array p = Array.of_list (List.map lincons0_of_vector0 (to_cons p))
  let to_generator_array p = 
    Array.of_list (List.map generator0_of_vector0 (to_gen p))
      
  let join = join
  let meet = meet
  let meet_lincons_array p c = 
    let cons = List.fold_left 
      (fun res c -> List.rev_append res (vector0_of_lincons0 c)) [] (Array.to_list c) 
    in
      meet_cons p cons 
	
  let of_lincons_array dim c =
    let cons = List.fold_left 
      (fun res c -> List.rev_append res (vector0_of_lincons0 c)) [] (Array.to_list c) 
    in
      of_cons (dim+1) cons
	
  let of_generator_array dim g =
    let gen = List.map (vector0_of_generator0) (Array.to_list g) in
      of_gen (dim+1) gen
	
  let assign_linexpr_array p d e =
    assign p (Array.to_list d) (List.map vector0_of_linexpr0 (Array.to_list e))
  let substitute_linexpr_array p d e =
    substitute p (Array.to_list d) (List.map vector0_of_linexpr0 (Array.to_list e))
      
  let add_dimensions = add_dimensions
  let remove_dimensions = remove_dimensions
  let permute_dimensions = permute_dimensions
  let widen = widen
  let narrow = narrow
  let widen_generators = widen_generators

  let print assoc fmt p = 
    if is_bottom p then
      Format.pp_print_string fmt "bottom"
    else if is_top p then
      Format.pp_print_string fmt "top"
    else begin
      Format.fprintf fmt "[|@[<hov>";
      let first = ref true in
      let gen = to_generator_array p in
	Array.sort Pervasives.compare gen;
	Array.iter (fun g -> 
		      if !first then 
			first := false 
		      else 
			Format.fprintf fmt ";@ "; 
		      Generator0.print assoc fmt g) gen;
	Format.fprintf fmt "@]|]"
    end
      
  let print_cons ?(reduction=false) assoc fmt p =
    if is_bottom p then
      Format.pp_print_string fmt "bottom"
    else if is_top p then
      Format.pp_print_string fmt "top"
    else begin
      if reduction then 
	remove_redundant_constraints p;
      let cons = to_lincons_array p in
	Array.sort Pervasives.compare cons;
	Format.fprintf fmt "[|@[<hov>";
	let first = ref true in
	  Array.iter (fun c ->
			if !first then
			  first := false
			else
			  Format.fprintf fmt ";@ ";
			Lincons0.print assoc fmt c) cons;
	  Format.fprintf fmt "@]|]"
    end
end
