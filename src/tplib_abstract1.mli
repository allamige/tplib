(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module Scalar : Semiring.T with type t = Tplib_abstract0.Scalar.t
module Linexpr1 :
  sig
    type t 
    val make : Apron.Environment.t -> t
    val copy : t -> t
    val set_coeff : t -> Apron.Var.t -> Scalar.t -> unit
    val set_cst : t -> Scalar.t -> unit
    val get_coeff : t -> Apron.Var.t -> Scalar.t
    val get_cst : t -> Scalar.t
    val set_list : t -> (Scalar.t * Apron.Var.t) list -> Scalar.t option -> unit
    val set_array : t -> (Scalar.t * Apron.Var.t) array -> Scalar.t option -> unit
    val iter : (Scalar.t -> Apron.Var.t -> 'a) -> t -> unit
    val get_env : t -> Apron.Environment.t
    val print : Format.formatter -> t -> unit
  end
module Lincons1 :
  sig
    type side = LEFT | RIGHT
    type t 
    type typ = EQ | SUPEQ
    val make : Linexpr1.t -> Linexpr1.t -> typ -> t
    val copy : t -> t
    val set_coeff : t -> Apron.Var.t -> Scalar.t -> side -> unit
    val set_cst : t -> Scalar.t -> side -> unit
    val get_coeff : t -> Apron.Var.t -> side -> Scalar.t
    val get_cst : t -> side -> Scalar.t
    val set_list : t -> (Scalar.t * Apron.Var.t) list ->
      Scalar.t option -> (Scalar.t * Apron.Var.t) list ->
      Scalar.t option -> unit
    val set_array : t -> (Scalar.t * Apron.Var.t) array ->
      Scalar.t option -> (Scalar.t * Apron.Var.t) array ->
      Scalar.t option -> unit
    val iter :
      (Scalar.t -> Apron.Var.t -> 'a) ->
      (Scalar.t -> Apron.Var.t -> 'b) -> t -> unit
    val get_env : t -> Apron.Environment.t
    val get_left1 : t -> Linexpr1.t
    val get_right1 : t -> Linexpr1.t
    val print : Format.formatter -> t -> unit
  end
module Generator1 :
  sig
    type typ = RAY | VERTEX
    type t 
    val make : Linexpr1.t -> typ -> t
    val copy : t -> t
    val set_coeff : t -> Apron.Var.t -> Scalar.t -> unit
    val set_typ : t -> typ -> unit
    val get_coeff : t -> Apron.Var.t -> Scalar.t
    val get_typ : t -> typ
    val set_list : t -> (Scalar.t * Apron.Var.t) list -> unit
    val set_array : t -> (Scalar.t * Apron.Var.t) array -> unit
    val iter : (Scalar.t -> Apron.Var.t -> 'a) -> t -> unit
    val get_linexpr1 : t -> Linexpr1.t
    val get_env : t -> Apron.Environment.t
    val print : Format.formatter -> t -> unit
  end
module Poly1 :
  sig
    type t 
    type strategy_t = GEN | CONS | LAZY
    val set_strategy : strategy_t -> unit
    val get_strategy : unit -> strategy_t
    val copy : t -> t
    val size : t -> int
    val bottom : Apron.Environment.t -> t
    val top : Apron.Environment.t -> t
    val env : t -> Apron.Environment.t
    val is_bottom : t -> bool
    val is_top : t -> bool
    val is_leq : t -> t -> bool
    val is_eq : t -> t -> bool
    val sat_lincons : t -> Lincons1.t -> bool
    val to_lincons_array : t -> Lincons1.t array
    val meet : t -> t -> t
    val meet_lincons_array : t -> Lincons1.t array -> t
    val join : t -> t -> t
    val assign_linexpr_array :
      t -> Apron.Var.t array -> Linexpr1.t array -> t
    val assign_linexpr : t -> Apron.Var.t -> Linexpr1.t -> t
    val substitute_linexpr_array :
      t -> Apron.Var.t array -> Linexpr1.t array -> t
    val change_environment : t -> Apron.Environment.t -> bool -> t
    val forget_array : t -> Apron.Var.t array -> t
    val of_lincons_array : Apron.Environment.t -> Lincons1.t array -> t
    val widen : t -> t -> t
    val widen_generators : t -> t -> t
    val to_generator_array : t -> Generator1.t array
    val print : Format.formatter -> t -> unit
    val print_cons : ?reduction:bool -> Format.formatter -> t -> unit
    val print_polymake :
      (Apron.Var.t * Scalar.t * Scalar.t)
      list -> Format.formatter -> t -> unit
  end
