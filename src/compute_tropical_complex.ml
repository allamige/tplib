(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

let numerical_data = ref "ocaml_big_rat"
let maxplus = ref true
let polymake_output = ref None

let compute_tropical_complex d =
  let module N = (val (Numeric.of_string (!numerical_data)): Numeric.T) in
  let module S = Semiring.Make(N) in
  let module V = Vector.Make(S) in
  let module C = Polytrope.Make(S)(V) in
  let module CD = Cell_decomposition.Make(S)(V)(C) in
  let dim = int_of_string d in
  let rec read_gen res =
    try
      let v = Scanf.scanf "%s\n" (fun x -> x) in
      read_gen (v::res)
    with End_of_file -> res
  in
  let gen =
    try
      List.map (V.of_string ~neg:(not !maxplus) dim) (read_gen [])
    with Invalid_argument _ ->
      invalid_arg ("Rays should be provided as vectors of size "
		   ^(string_of_int dim)
		   ^". See README for further information.")
  in
  let check_finiteness g =
    let res = ref true in
    for i = 0 to (V.size g) - 1 do
      if S.compare (V.get g i) S.zero = 0 then
	res := false
    done;
    !res
  in
  if List.exists (fun g -> not (check_finiteness g)) gen then
    invalid_arg ("Only vectors with finite entries are currently supported. See README for further information");
  let print_vector fmt v =
    let to_string x =
      if !maxplus then N.to_string x
      else N.to_string (N.neg x)
    in
    Format.fprintf fmt "[%s@?" (N.to_string v.(0));
    for j = 1 to (dim-1) do
      Format.fprintf fmt ",%s@?" (to_string v.(j))
    done;
    Format.fprintf fmt "]"
  in
  let print_adjacency_list fmt l =
    match l with
      | [] -> Format.fprintf fmt "[ ]@."
      | i::l' ->
          Format.fprintf fmt "[%d@?" i;
          List.iter (fun j -> Format.fprintf fmt ",%d@?" j) l';
          Format.fprintf fmt "]"
  in
  let module Tbl = Hashtbl.Make (struct
    type t = N.t array
    let equal v w =
      try
        for i = 0 to (dim-1) do
          if N.compare v.(i) w.(i) <> 0 then raise Exit
        done;
        true
      with Exit -> false

    let hash v =
      let l = ref [] in
      for i = 0 to (dim-1) do
        if N.compare v.(i) N.zero <> 0 then
          l := (i,N.to_string v.(i))::(!l)
      done;
      Hashtbl.hash (!l)
  end)
  in
  let n = ref 0 in
  let tbl = Tbl.create 100 in
  let adjacency_of_cell cell =
    let vertices = List.map (fun v ->
      V.scale v;
      assert (S.compare (V.get v 0) S.one = 0);
      let w = Array.init dim (fun _ -> N.zero) in
      w.(0) <- N.of_int 1;
      for i = 1 to (dim-1) do
        w.(i) <- S.to_num (V.get v i)
      done;
      w) (C.pseudo_vertices cell) in
    let adj_list =
      List.fold_left (fun res v ->
        try
          (Tbl.find tbl v)::res
        with Not_found ->
          Tbl.add tbl v (!n);
          let res' = !n::res in
          n := !n+1;
          res') [] vertices
    in
    List.sort compare adj_list
  in
  let cells = CD.compute_bounded_cells dim gen in
  let adj_list = List.map adjacency_of_cell cells in
  let vertices = Array.init !n (fun _ -> None) in
  Tbl.iter (fun v i -> vertices.(i) <- Some v) tbl;
  match !polymake_output with
    | None ->
        Format.printf "Vertices of the complex:@.";
        for i = 0 to (!n)-1 do
          match vertices.(i) with
            | None -> assert false
            | Some v -> Format.printf "%d: %a@." i print_vector v
        done;
        Format.printf "@.";
        Format.printf "Maximal cells (adjacency matrix):@.";
        List.iter (Format.printf "%a@." print_adjacency_list) adj_list
    | Some name ->
        Format.printf "$%s = new fan::PolyhedralComplex(VERTICES=>[@?" name;
        for i = 0 to (!n)-1 do
          match vertices.(i) with
            | None -> assert false
            | Some v ->
                if i > 0 then
                  Format.printf ",@?";
                Format.printf "%a@?" print_vector v;

        done;
        Format.printf "], MAXIMAL_POLYTOPES=>[@?";
        let first = ref true in
        List.iter (fun l ->
          if not !first then
            Format.printf ",@?"
          else
            first := false;
          Format.printf "%a@?" print_adjacency_list l) adj_list;
        Format.printf "]);@."

let _ =
  Arg.parse
    [
      ("-numerical-data", Arg.String (fun s -> numerical_data := s),
       "Set the type of numerical data used by the algorithm. Values: "
       ^(List.fold_left (fun res s ->
	 if String.length res = 0 then s
	 else res^", "^s) "" (Numeric.get_name_of_modules ())));
      ("-min-plus", Arg.Clear maxplus,
       "Use the min-plus semiring instead of the default max-plus semiring");
      ("-polymake-output", Arg.String (fun s -> polymake_output := Some s),
       "Output under polymake format. Takes an argument, corresponding to the name of the variable containing the complex.")]
    compute_tropical_complex
    ("Computes the tropical complex of a set of vectors with finite entries. Version of TPLib: "^Config.version)
