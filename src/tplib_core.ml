(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)


module type T =
sig
  type vector_t
  type halfspace_t
  val compute_tangent_hypergraph : ?labels:string list -> int -> vector_t list -> vector_t -> Hypergraph.t
  val compute_ext_rays : ?with_order:bool -> int -> vector_t list -> vector_t list
  val compute_ext_rays_polar : ?with_order:bool -> int -> vector_t list -> vector_t list
  val is_redundant_halfspace : halfspace_t list -> halfspace_t -> bool
  val compute_non_redundant_halfspaces : halfspace_t list -> halfspace_t list
  val compute_minimal_external_representations : halfspace_t list -> (vector_t * int list list) list
end

module Make (S: Semiring.T) (V: Vector.T with type scalar_t = S.t)
  (H: Halfspace.T with type vector_t = V.t)  = struct
  open S.Infixes

  type vector_t = V.t

  type halfspace_t = H.t

  module Hashset = struct
    module H = Hashtbl.Make (struct
			       type t = V.t
			       let equal = V.equal
			       let hash = V.hash
			     end)

    let create n = H.create n

    let add h v = H.add h v ()

    let mem h v = H.mem h v
  end

  let identity n =
    let rec identity_aux i =
      if i >= n then []
      else
	(V.can n i)::(identity_aux (i+1))
    in
      identity_aux 0

  let support_fun v =
    let res = Array.init (V.size v) (fun _ -> -1) in
    let rec support_aux l cur i =
      if i >= V.size v then (cur,l,res)
      else if V.get v i <>% S.zero then begin
	res.(i) <- cur;
	support_aux (i::l) (cur+1) (i+1)
      end
      else
	support_aux l cur (i+1)
    in
      support_aux [] 0 0

  let rec split dim cons =
    match cons with
      | [] -> ([], [])
      | c::cons' ->
	  let v = V.make dim S.zero
	  and w = V.make dim S.zero in
	  for i = 0 to dim-1 do
	    let x = V.get c i
	    and y = V.get c (dim+i) in
	    if x >=% y then
	      V.set v i x
	    else
	      V.set w i y
	  done;
	  let (res1, res2) = split dim cons' in
	  (v::res1,w::res2)

  let scalar dim c g =
    let rec argmax max arg count i =
      if i >= dim then (max,arg,count)
      else
	let x = (V.get g i) *% (V.get c i) in
	if S.compare x max > 0 then
	  argmax x [i] 1 (i+1)
	else if S.compare x max = 0 then
	  argmax max (i::arg) (count+1) (i+1)
	else (* if S.compare x max < 0 then *)
	  argmax max arg count (i+1)
    in
    argmax S.zero [] 0 0

  let compute_tangent_hypergraph ?(labels=[]) dim cons v =
    if (labels <> []) && (List.length labels <> List.length cons) then
      invalid_arg "compute_tangent_hypergraph: list of inequalities and labels should have idendical lengths";
    let rec compute_tangent_hypergraph_aux h labels (cons1, cons2) =
      match (cons1, cons2) with
        | ([], []) -> h
        | (a::cons1', b::cons2') ->
            let (av, arg_av, card_av) = scalar dim a v
            and (bv, arg_bv, card_bv) = scalar dim b v in
            if (av =% S.zero) || (card_bv = 0) || (av >% bv) then
              let labels' =
                match labels with
                  | [] -> []
                  | _::labels' -> labels'
              in
              compute_tangent_hypergraph_aux h labels' (cons1', cons2')
            else
              let (h',labels') =
                match labels with
                  | [] -> (Hypergraph.add_hedge h (card_av, arg_av, arg_bv), [])
                  | lbl::labels' ->
                      (Hypergraph.add_hedge h ~label:lbl (card_av, arg_av, arg_bv), labels')
              in
	      compute_tangent_hypergraph_aux h' labels' (cons1', cons2')
        | _ -> assert false
    in
    compute_tangent_hypergraph_aux (Hypergraph.create dim) labels (split dim cons)

  let is_extreme dim (g,arg_ag,arg_bg) =
    let (n,supp,supp_fun) = support_fun g in
    if n = 0 then false
    else
      let supp_fun i = supp_fun.(i) in
      let rec build_hedge h (arg_ag, arg_bg) =
	match (arg_ag, arg_bg) with
	  | ([],[]) -> h
	  | ((ag,arg_ag,card_ag)::arg_ag',(bg,arg_bg,card_bg)::arg_bg')
	      when (ag =% S.zero) || (card_bg = 0) || (ag >% bg) -> build_hedge h (arg_ag', arg_bg')
	  | ((ag,arg_ag,card_ag)::arg_ag',(bg,arg_bg,card_bg)::arg_bg') ->
	      let h' = Hypergraph.add_hedge h
		(card_ag, List.map supp_fun arg_ag, List.map supp_fun arg_bg) in
	      build_hedge h' (arg_ag', arg_bg')
	  | _ -> assert false
      in
      let h = build_hedge (Hypergraph.create n) (arg_ag,arg_bg) in
      Hypergraph.has_greatest_scc h

  let compute_ext_rays ?(with_order=true) dim cons =
    let rec union_list (c1,l1) (c2,l2) =
      match (l1, l2) with
	| (_,[]) -> (c1,l1) | ([],_) -> (c2,l2)
	| (h1::t1,h2::t2) when h1 < h2 ->
	    let (c,l) = union_list (c1-1,t1) (c2,l2) in
	      (c+1,h1::l)
	| (h1::t1,h2::t2) when h1 > h2 ->
	    let (c,l) = union_list (c1,l1) (c2-1,t2) in
	      (c+1,h2::l)
	| (h1::t1,h2::t2) ->
	    let (c,l) = union_list (c1-1,t1) (c2-1,t2) in
	      (c+1,h1::l)
    in
    let (cons1, cons2) = split dim cons in
    let add_vector cache res ((g,arg_ag,arg_bg) as g') =
      if not (Hashset.mem cache g) then begin
	if is_extreme dim g' then begin
	  Hashset.add cache g;
	  g'::res
	end
	else
	  res
      end
      else
	res
    in
    let partition c d gen = (* yields a triple (res1, res2, res3):
			     * res1 is the hashset {g | cg >= dg}
			     * res2 is {(cg, g) | cg >= dg}
			     * res3 is {(dg, g) | cg < dg} *)
      let p = ref 0 and q = ref 0 in
      let partition_aux (res1, res2, res3) (g,arg_ag,arg_bg) =
	let (x,arg_cg,card_cg) = scalar dim c g
	and (y,arg_dg,card_dg) = scalar dim d g in
	let g' = (g, (x, arg_cg, card_cg)::arg_ag, (y, arg_dg, card_dg)::arg_bg) in
	let (res1',res2',res3') =
	  if x >=% y then
	    (g'::res1, (incr p; (x,g')::res2), res3)
	  else
	    (res1, res2, (incr q; (y,g')::res3))
	in
	  (res1', res2', res3')
      in
      let ((res1, res2, res3) as res) = List.fold_left partition_aux ([], [], []) gen in
	((!p)*(!q), res)
    in
    let build_combination lambda (g,arg_ag,arg_bg) mu (h,arg_ah,arg_bh) =
      let build_combination_aux (cg, arg_cg, card_cg) (ch, arg_ch, card_ch) =
	if cg >% ch then
	  (cg, arg_cg, card_cg)
	else if cg <% ch then
	  (ch, arg_ch, card_ch)
	else
	  let (card,arg) = union_list (card_cg,arg_cg) (card_ch, arg_ch) in
	    (cg, arg, card)
      in
      let build_combination_aux2 (ag, arg_ag, card_ag) (bg, arg_bg, card_bg)
	  (ah, arg_ah, card_ah) (bh, arg_bh, card_bh) =
	let (ax, arg_ax, card_ax) = build_combination_aux (lambda *% ag, arg_ag, card_ag)
	  (mu *% ah, arg_ah, card_ah) in
	let bg' = lambda *% bg
	and bh' = mu *% bh in
	  if (ax >% bg' && ax >% bh') then
	    ((ax, arg_ax, card_ax), (S.zero, [], 0))
	  else
	    let (bx, arg_bx, card_bx) = build_combination_aux (bg', arg_bg, card_bg)
	      (bh', arg_bh, card_bh) in
	      ((ax, arg_ax, card_ax), (bx, arg_bx, card_bx))
      in
      let rec build_combination_aux3 l1 l2 l3 l4 =
	match (l1, l2, l3, l4) with
	  | ([], [], [], []) -> ([], [])
	  | (h1::t1, h2::t2, h3::t3, h4::t4) ->
	      let (t1',t2') = build_combination_aux3 t1 t2 t3 t4 in
	      let (h1', h2') = build_combination_aux2 h1 h2 h3 h4 in
		(h1'::t1', h2'::t2')
	  | _ -> assert false
      in
      let v = V.make dim S.zero in
	V.mul v lambda g;
	V.addmul v mu h;
	if V.equal v g then
	  (false, (g, arg_ag, arg_bg))
	else
	  let (arg_av, arg_bv) = build_combination_aux3 arg_ag arg_bg arg_ah arg_bh in
	    (true, (v, arg_av, arg_bv))
    in
    let combine nb_comb res gen1 gen2 =
      (* yields the combination y.g + x.h for any
       * (x, g) in gen1 and (y, h) in gen2, and then
       * concatenates it to res                      *)
      let cache = Hashset.create nb_comb in
      let rec combine_aux res1 gen1 =
	match gen1 with
	  | [] -> res1
	  | (x,_)::gen1' when x =% S.zero -> combine_aux res1 gen1'
	  | (x,g)::gen1' ->
	      let rec combine_aux2 res2 gen2 =
		match gen2 with
		  | [] -> combine_aux res2 gen1'
		  | (y,h)::gen2' ->
		      let z = ~-% (x+%y) in
		      let x' = z *% x and y' = z *% y in
		      let (changed,v) =
			build_combination y' g x' h
		      in
			if not changed then
			  combine_aux2 res2 gen2'
			else
			  combine_aux2 (add_vector cache res2 v) gen2'
	      in
		combine_aux2 res1 gen2
	in
	  combine_aux res gen1
    in
    let extract_minimum gen (cons1, cons2) =
      let rec extract_minimum_aux gen (min, part_res, a, b) (sup1, sup2) (cons1,cons2) =
	match (cons1, cons2) with
	  | ([], []) -> (min, part_res, (a,b), (sup1, sup2))
	  | (c1::cons1', c2::cons2') ->
	      let (x, y) = partition c1 c2 gen in
		if x < min then
		  extract_minimum_aux gen (x, y, c1, c2)
		    (a::sup1, b::sup2) (cons1', cons2')
		else
		  extract_minimum_aux gen (min, part_res, a, b)
		    (c1::sup1, c2::sup2) (cons1', cons2')
	  | _ -> assert false
      in
	match (cons1, cons2) with
	  | ([], []) -> assert false
	  | (a::cons1', b::cons2') ->
	      let (min, part_res) = partition a b gen in
		if with_order then
		  extract_minimum_aux gen (min, part_res, a, b) ([], []) (cons1', cons2')
		else
		  (min, part_res, (a,b), (cons1', cons2'))
	  | _ -> assert false
    in
    let rec ineqsolve_cons_aux gen (cons1, cons2) =
      match (cons1, cons2) with
	| ([], []) -> gen
	| _ ->
	    let (nb_comb, (res1, res2, res3), (new_a, new_b), (cons1', cons2')) =
	      extract_minimum gen (cons1, cons2) in
	    let gen' = combine nb_comb res1 res2 res3 in
	      ineqsolve_cons_aux gen' (cons1', cons2')
    in
    let gen = List.map (fun g -> (g, [], [])) (identity dim) in
    let res = ineqsolve_cons_aux gen (cons1, cons2) in
    let gen' = List.map (fun (x,_,_) -> x) res in
    List.iter V.scale gen';
    gen'

  let is_extreme_polar dim (g,arg_ag,arg_bg) =
    let d = dim/2 in
    let (n, supp, supp_fun) = support_fun g in
      if n = 0 then false
      else
	let (supp1,supp2) = List.fold_left (fun (res1,res2) x -> if x < d then
					      (x::res1,res2)
					    else
					      (res1,x::res2)) ([],[]) supp in
	  match (supp1,supp2) with
	    | ([], []) -> false
	    | ([_], []) -> true
	    | (_, []) -> false
	    | (_, _::_::_) -> false
	    | (_, [_]) ->
		let found = Array.init (n-1) (fun _ -> false) in
		let rec check_hedge res (arg_ag, arg_bg) =
		  if res >= n-1 then true
		  else
		    match (arg_ag, arg_bg) with
		      | ([],[]) -> false
		      | ((ag,arg_ag,card_ag)::arg_ag',(bg,arg_bg,card_bg)::arg_bg')
			  when (ag =% S.zero) || (ag >% bg) || (card_ag >= 2) -> check_hedge res (arg_ag', arg_bg')
		      | ((ag,arg_ag,card_ag)::arg_ag',(bg,arg_bg,card_bg)::arg_bg') ->
			  begin
			    match arg_ag with
			      | [j] ->
				  if not found.(supp_fun.(j)) then begin
				    found.(supp_fun.(j)) <- true;
				    check_hedge (res+1) (arg_ag', arg_bg')
				  end
				  else
				    check_hedge res (arg_ag', arg_bg')
			      | _ -> check_hedge res (arg_ag', arg_bg')
			  end
		      | _ -> assert false
		in
		  check_hedge 0 (arg_ag, arg_bg)

  let compute_ext_rays_polar ?(with_order=true) dim_gen gen =
    let dim = 2*dim_gen in
    let dim' = 4*dim_gen
    and off = 3*dim_gen in
    let build_cons g =
      let c = V.make dim' S.zero in
	for i = 0 to dim_gen-1 do
	  V.set c i (V.get g i);
	  V.set c (i+off) (V.get g i)
	done; c
    in
    let cons = List.map build_cons gen in
    let union_list (c1,l1) (c2,l2) =
      match (l1,l2) with
	| ([], []) -> (0,[])
	| ([_],[]) -> (1,l1)
	| ([],[_]) -> (1,l2)
	| ([h1],[h2]) when h1 = h2 -> (1,l1)
	| (h1::_,h2::_) -> (2,[h1;h2])
	| _ -> assert false
    in
    let rec split cons =
      match cons with
	| [] -> ([], [])
	| c::cons' ->
	    let v = V.make dim S.zero
	    and w = V.make dim S.zero in
	      for i = 0 to dim-1 do
		let x = V.get c i
		and y = V.get c (dim+i) in
		  if x >=% y then
		    V.set v i x
		  else
		    V.set w i y
	      done;
	      let (res1, res2) = split cons' in
		(v::res1,w::res2)
    in
    let (cons1, cons2) = split cons in
    let add_vector cache res ((g,arg_ag,arg_bg) as g') =
      if not (Hashset.mem cache g) then begin
	if is_extreme_polar dim g' then begin
	  Hashset.add cache g;
	  g'::res
	end
	else
	  res
      end
      else
	res
    in
    let scalar c g =
      let rec argmax max arg count i =
	if i >= dim then (max,arg,count)
	else
	  let x = (V.get g i) *% (V.get c i) in
	  if S.compare x max > 0 then
	    argmax x [i] 1 (i+1)
	  else if S.compare x max = 0 then
	    argmax max (i::arg) (count+1) (i+1)
	  else (* if S.compare x max < 0 then *)
	    argmax max arg count (i+1)
      in
	argmax S.zero [] 0 0
    in
    let partition c d gen = (* yields a triple (res1, res2, res3):
			     * res1 is the hashset {g | cg >= dg}
			     * res2 is {(cg, g) | cg >= dg}
			     * res3 is {(dg, g) | cg < dg} *)
      let p = ref 0 and q = ref 0 in
      let partition_aux (res1, res2, res3) (g,arg_ag,arg_bg) =
	let (x,arg_cg,card_cg) = scalar c g
	and (y,arg_dg,card_dg) = scalar d g in
	let g' = (g, (x, arg_cg, card_cg)::arg_ag,(y, arg_dg, card_dg)::arg_bg)	in
	let res1' = if x >=% y then g'::res1 else res1
	and res2' = if x >=% y then (incr p; (x,g')::res2) else res2
	and res3' = if x <% y then (incr q; (y,g')::res3) else res3 in
	  (res1', res2', res3')
      in
      let ((res1, res2, res3) as res) = List.fold_left partition_aux ([], [], []) gen in
	((!p)*(!q), res)
    in
    let build_combination lambda (g,arg_ag,arg_bg) mu (h,arg_ah,arg_bh) =
      (* builds the vector lambda g + mu h, and computes all the new information
       * for the arg_a and arg_b *)
      let build_combination_aux (cg, arg_cg, card_cg) (ch, arg_ch, card_ch) =
	let cg' = lambda *% cg
	and ch' = mu *% ch in
	if cg' >% ch' then
	  (cg', arg_cg, card_cg)
	else if cg' <% ch' then
	  (ch', arg_ch, card_ch)
	else
	  let (card,arg) = union_list (card_cg,arg_cg) (card_ch, arg_ch) in
	    (cg', arg, card)
      in
      let v = V.make dim S.zero in
	V.mul v lambda g;
	V.addmul v mu h;
	if V.equal v g then
	  (false, (g, arg_ag, arg_bg))
	else
	  (true, (v, List.map2 build_combination_aux arg_ag arg_ah, List.map2 build_combination_aux arg_bg arg_bh))
    in
    let combine res gen1 gen2 =
      (* yields the combination y.g + x.h for any
       * (x, g) in gen1 and (y, h) in gen2, and then
       * concatenates it to res                      *)
      let p = List.length gen1
      and q = List.length gen2 in
      let cache = Hashset.create (p*q) in
      let rec combine_aux res1 gen1 =
	match gen1 with
	  | [] -> res1
	  | (x,_)::gen1' when x =% S.zero -> combine_aux res1 gen1'
	  | (x,g)::gen1' ->
	      let rec combine_aux2 res2 gen2 =
		match gen2 with
		  | [] -> combine_aux res2 gen1'
		  | (y,h)::gen2' ->
		      let z = ~-% (x+%y) in
		      let x' = z *% x and y' = z *% y in
		      let (changed,v) =
			build_combination y' g x' h
		      in
			if not changed then
			  combine_aux2 res2 gen2'
			else
			  combine_aux2 (add_vector cache res2 v) gen2'
	      in
		combine_aux2 res1 gen2
      in
	combine_aux res gen1
    in
    let extract_minimum gen (cons1, cons2) =
      let rec extract_minimum_aux gen (min, part_res, a, b) (sup1, sup2) (cons1,cons2) =
	match (cons1, cons2) with
	  | ([], []) -> (part_res, (a,b), (sup1, sup2))
	  | (c1::cons1', c2::cons2') ->
	      let (x, y) = partition c1 c2 gen in
		if x < min then
		  extract_minimum_aux gen (x, y, c1, c2)
		    (a::sup1, b::sup2) (cons1', cons2')
		else
		  extract_minimum_aux gen (min, part_res, a, b)
		    (c1::sup1, c2::sup2) (cons1', cons2')
	  | _ -> assert false
      in
	match (cons1, cons2) with
	  | ([], []) -> assert false
	  | (a::cons1', b::cons2') ->
	      let (min, part_res) = partition a b gen in
		if with_order then
		  extract_minimum_aux gen (min, part_res, a, b) ([], []) (cons1', cons2')
		else
		  (part_res, (a,b), (cons1', cons2'))
	  | _ -> assert false
    in
    let rec ineqsolve_aux gen (cons1, cons2) =
      match (cons1, cons2) with
	| ([], []) -> gen
	| _ ->
	    let ((res1, res2, res3), (new_a, new_b), (cons1', cons2')) =
	      extract_minimum gen (cons1, cons2) in
	    let gen' = combine res1 res2 res3 in
	      ineqsolve_aux gen' (cons1', cons2')
    in
    let gen' = List.map (fun g -> (g, [], [])) (identity dim) in
    let res = ineqsolve_aux gen' (cons1, cons2) in
      List.map (fun (x,_,_) -> x) res

  let is_redundant_halfspace hs_list hs = (* tests whether the half-space hs is
                                           * redundant w.r.t. hs_list          *)
    if List.exists (fun hs' -> not (H.belong hs' hs.H.apex)) hs_list then
      invalid_arg "is_redundant_halfspace: the apex of the half-space (2nd arg) should belong to every half-space of the collection (1st arg)."
    else
      let dim = V.size hs.H.apex in
      let cons = List.map H.to_inequality hs_list in
      let g = compute_tangent_hypergraph dim cons hs.H.apex in
      List.length (Hypergraph.visit_list g hs.H.sectors) = dim

  let compute_non_redundant_halfspaces hs_list =
    (*let hs_list =
      match hs_list with
        | [] ->
            let cons = compute_ext_rays_polar dim gen in
            List.map (H.of_inequality ~gen) cons
        | _ -> hs_list
    in*)
    let rec compute_non_redundant_halfspaces_aux l1 l2 =
      match l2 with
        | [] -> l1
        | hs::l2' ->
            let l = List.rev_append l1 l2' in
            if is_redundant_halfspace l hs then
              compute_non_redundant_halfspaces_aux l1 l2'
            else
              compute_non_redundant_halfspaces_aux (hs::l1) l2'
    in
    compute_non_redundant_halfspaces_aux [] hs_list

  let compute_minimal_external_representations hs_list =
    let hs_list = compute_non_redundant_halfspaces hs_list in
    let hs_cons_list = List.map (fun hs -> (hs, H.to_inequality hs)) hs_list in
    let compute_minimal_external_representations_aux res hs =
      let a = hs.H.apex in
      let dim = V.size a in
      if List.for_all (fun (a', _) -> try V.hilbert_distance a a' >% S.one with S.Pinfty -> true) res then
        let (sectors_list, cons_list) =
          List.fold_left (fun (res1, res2) (hs,c) ->
            try
              if V.hilbert_distance hs.H.apex a =% S.one then
                (hs.H.sectors::res1, res2)
              else
                (res1, c::res2)
            with S.Pinfty -> (res1, c::res2)) ([], []) hs_cons_list
        in
        let g = compute_tangent_hypergraph dim cons_list a in
        let sectors_list' =
          List.map (fun sectors -> List.sort compare (Hypergraph.visit_list g sectors)) sectors_list
        in
        (a, sectors_list)::res
      else
        res
    in
    List.fold_left compute_minimal_external_representations_aux [] hs_list

end
