(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module S = struct
  type t = Q.t
  type num_t = Q.t
  exception Pinfty

  module Num = struct
  let pow x n =
    let rec pow_aux x n =
      assert (n >= 0);
      if n = 0 then
        Q.of_int 1
      else
        let y = pow_aux x (n/2) in
        let z = Q.mul y y in
        if n mod 2 = 0 then
          z
        else
          Q.mul x z
    in
    if n >= 0 then
      pow_aux x n
    else
      pow_aux (Q.inv x) (-n)

    include Q

    let random n =
      let num = Z.of_string (Random_generator.to_string n) in
      let den = Z.of_string (Random_generator.to_string n) in
      Q.make num (Z.add den Z.one)
  end

  let zero = Q.minus_inf

  let one = Q.zero

  let add = Q.max

  let mul = Q.add

  let min = Q.min

  let neg x =
    if compare x zero = 0 then
      raise Pinfty
    else
      Q.neg x

  let compare = Q.compare

  let random n =
    let num = Z.of_string (Random_generator.to_string n) in
    let den = Z.of_string (Random_generator.to_string n) in
    Q.make num den

  let of_int = Q.of_int

  let of_num = (fun x -> x)

  let of_string ?(neg=false) x =
    if not neg then
      if String.compare x "-oo" = 0 then
	zero
      else Q.of_string x
    else
      if String.compare x "+oo" = 0 then
	zero
      else Q.neg (Q.of_string x)

  let to_num = (fun x -> x)

  let to_string ?(neg=false) x =
    if not neg then
      if compare x zero = 0 then "-oo"
      else Q.to_string x
    else
      if compare x zero = 0 then "+oo"
      else Q.to_string (Q.neg x)

  module Infixes = struct
    let ( +% ) = add
    let ( *% ) = mul
    let ( ~-% ) = neg

    let ( =% ) x y = (compare x y = 0)
    let ( <=% ) x y = (compare x y <= 0)
    let ( <% ) x y = (compare x y < 0)
    let ( >=% ) x y = (compare x y >= 0)
    let ( >% ) x y = (compare x y > 0)
    let ( <>% ) x y = (compare x y <> 0)
  end
end
