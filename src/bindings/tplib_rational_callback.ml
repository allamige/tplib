(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module V = Vector_array.Make(Semiring_rational.S)

module P = Tplib_abstract.Make(Semiring_rational.S)(V)

include P

(*let of_cons dim cons =
  List.iter (fun v -> Format.printf "%s@." (V.to_string v)) cons;
  let res = of_cons dim cons in
  Format.printf "res:@.";
  List.iter (fun v -> Format.printf "%s@." (V.to_string v)) (to_gen res);
  Format.printf "toto:@.";
  res*)

let call_gc () = Gc.compact ()

let suffix = "_rational"

let _ =
  Format.printf "Registering...@.";
  Callback.register ("dimension"^suffix) dimension;
  Callback.register ("copy"^suffix) copy;
  Callback.register ("normalize"^suffix) normalize;
  Callback.register ("set_strategy"^suffix) set_strategy;
  Callback.register ("get_strategy"^suffix) get_strategy;
  Callback.register ("apply_strategy"^suffix) apply_strategy;
  Callback.register ("of_cons"^suffix) of_cons;
  Callback.register ("of_gen"^suffix) of_gen;
  Callback.register ("to_cons"^suffix) to_cons;
  Callback.register ("to_gen"^suffix) to_gen;
  Callback.register ("is_leq"^suffix) is_leq;
  Callback.register ("sat_cons"^suffix) sat_cons;
  Callback.register ("bottom"^suffix) bottom;
  Callback.register ("is_bottom"^suffix) is_bottom;
  Callback.register ("top"^suffix) top;
  Callback.register ("is_top"^suffix) is_top;
  Callback.register ("join"^suffix) join;
  Callback.register ("meet_cons"^suffix) meet_cons;
  Callback.register ("meet"^suffix) meet;
  Callback.register ("assign"^suffix) assign;
  Callback.register ("substitute"^suffix) substitute;
  Callback.register ("add_dimensions"^suffix) add_dimensions;
  Callback.register ("remove_dimensions"^suffix) remove_dimensions;
  Callback.register ("permute_dimensions"^suffix) permute_dimensions;
  Callback.register ("widen"^suffix) widen;
  Callback.register ("widen_generators"^suffix) widen_generators;
  Callback.register ("narrow"^suffix) narrow;
  Callback.register ("remove_redundant_constraints"^suffix) remove_redundant_constraints;
  Callback.register ("call_gc"^suffix) call_gc;
  Format.printf "...done@."
