/*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

typedef struct _poly_t poly_t;

typedef enum _strategy_t {
    GEN,
    CONS,
    LAZY
} strategy_t;

typedef struct _matrix_t matrix_t;

void init(void);
void poly_free(poly_t* x);
int dimension(poly_t* x);
poly_t* copy(poly_t* x);
poly_t* normalize(poly_t* x);
void set_strategy(strategy_t s);
strategy_t get_strategy(void);
poly_t* apply_strategy(poly_t* x);
poly_t* of_cons(int dim, matrix_t* cons);
poly_t* of_gen(int dim, matrix_t* gen);
matrix_t* to_cons(poly_t *x);
matrix_t* to_gen(poly_t *x);
int is_leq(poly_t *x1, poly_t *x2);
int sat_cons(poly_t* x, matrix_t* cons);
poly_t* bottom(int dim);
int is_bottom(poly_t* x);
poly_t* top(int dim);
int is_top(poly_t* x);
poly_t* join(poly_t *x1, poly_t *x2);
poly_t* meet_cons(poly_t *x, matrix_t* cons);
poly_t* meet(poly_t *x1, poly_t *x2);
poly_t* assign(poly_t *x, int* var, matrix_t* expr);
poly_t* substitute(poly_t *x, int* var, matrix_t* expr);
poly_t* add_dimensions(poly_t *x, int *dim, int intdim, int constrained);
poly_t* remove_dimensions(poly_t *x, int *dim, int intdim);
poly_t* permute_dimensions(poly_t *x, int *perm);
poly_t* widen(poly_t *x1, poly_t *x2);
poly_t* widen_generators(poly_t *x1, poly_t *x2);
poly_t* narrow(poly_t *x1, poly_t *x2);
void remove_redundant_constraints(poly_t* x);
void call_gc(void);
void print_poly(poly_t *x);

matrix_t* matrix_alloc(int rows, int cols);
void matrix_free(matrix_t* m); 
matrix_t* matrix_of_array(int rows, int cols, mpq_t *coeffs);
mpq_t* matrix_to_array(matrix_t* m);
int matrix_get_nb_rows(matrix_t* m);
int matrix_get_nb_cols(matrix_t* m);
void matrix_get(mpq_t x, matrix_t* m, int row, int col);
void matrix_set(matrix_t* m, int row, int col, mpq_t coeff);
void matrix_set_ui(matrix_t* m, int row, int col, unsigned long int num, unsigned long int den);
void matrix_set_si(matrix_t* m, int row, int col, long int num, unsigned long int den);
void matrix_print(matrix_t *m);

