(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module S = struct
  type t = float
  type num_t = float
  exception Pinfty

  module Num = Numeric.Ocaml_float

  let zero = neg_infinity

  let one = 0.

  let add = max

  let mul = ( +. )

  let min = min

  let neg x =
    if compare x zero = 0 then
      raise Pinfty
    else
      (~-. x)

  let compare = compare

  let random n =
    if n > 64 then
      invalid_arg "Ocaml_float.random: OCaml does not support float of size larger than 64 bits";
    let x = Int64.of_string (Random_generator.to_string n) in
    Int64.float_of_bits x

  let of_int = float_of_int

  let of_num = (fun x -> x)

  let of_string ?(neg=false) x =
    if not neg then
      if String.compare x "-oo" = 0 then
	neg_infinity
      else float_of_string x
    else
      if String.compare x "+oo" = 0 then
	neg_infinity
      else -. (float_of_string x)

  let to_num = (fun x -> x)

  let to_string ?(neg=false) x =
    if not neg then
      if x = neg_infinity then "-oo"
      else string_of_float x
    else
      if x = neg_infinity then "+oo"
      else string_of_float (-. x)

  module Infixes = struct
    let ( +% ) = add
    let ( *% ) = mul
    let ( ~-% ) = neg

    let ( =% ) x y = (compare x y = 0)
    let ( <=% ) x y = (compare x y <= 0)
    let ( <% ) x y = (compare x y < 0)
    let ( >=% ) x y = (compare x y >= 0)
    let ( >% ) x y = (compare x y > 0)
    let ( <>% ) x y = (compare x y <> 0)
  end
end
