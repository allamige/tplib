(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

type t
type vector_t = Q.t array
type strategy_t = GEN | CONS | LAZY
val dimension : t -> int
val copy : t -> t
val normalize : t -> t
val set_strategy : strategy_t -> unit
val get_strategy : unit -> strategy_t
val apply_strategy : t -> t
val of_cons : int -> vector_t list -> t
val of_gen : int -> vector_t list -> t
val to_cons : t -> vector_t list
val to_gen : t -> vector_t list
val is_leq : t -> t -> bool
val sat_cons : t -> vector_t list -> bool
val bottom : int -> t
val is_bottom : t -> bool
val top : int -> t
val is_top : t -> bool
val join : t -> t -> t
val meet_cons : t -> vector_t list -> t
val meet : t -> t -> t
val assign : t -> int list -> vector_t list -> t
val substitute : t -> int list -> vector_t list -> t
val add_dimensions : t -> int array -> int -> bool -> t
val remove_dimensions : t -> int array -> int -> t
val permute_dimensions : t -> int array option -> t
val widen : t -> t -> t
val widen_generators : t -> t -> t
val narrow : t -> t -> t
val remove_redundant_constraints : t -> unit
val call_gc : unit -> unit
