(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module Make(S: Semiring.T) = struct
  open S.Infixes

  type t = S.t array
  type scalar_t = S.t

  let make n dummy = Array.make n dummy
  
  let copy = Array.copy
    
  let get = Array.get

  let set = Array.set

  let size = Array.length

  let equal v w =
    let sz = size v in
    if sz <> size w then invalid_arg "Vector.equal: mismatched dimensions";
    try 
      for i = 0 to sz-1 do
	if v.(i) <>% w.(i) then 
	  raise Exit
      done;
      true
    with Exit -> false

  let scale v =
    let sz = size v in
    let s = ref S.zero in
    try
      for i = 0 to (sz-1) do
        let s' = v.(i) in
        try
          s := ~-% s';
          raise Exit
        with S.Pinfty -> ()
      done
    with Exit -> 
      for i = 0 to (sz-1) do
        v.(i) <- !s *% v.(i)
      done
	
  let is_leq v w =
    let sz = size v in
    if sz <> size w then invalid_arg "Vector.is_leq: mismatched dimensions";
    try
      for i = 0 to sz-1 do
	if v.(i) >% w.(i) then
	  raise Exit
      done;
      true
    with Exit -> false

  let add res v w = (* res := v + w *)
    let sz = size res in
    if not (sz = size v && sz = size w) then invalid_arg "Vector.add: mismatched dimensions";
    for i = 0 to sz-1 do
      res.(i) <- v.(i) +% w.(i)
    done

  let mul res s v = (* res := s.v *)
    let sz = size res in
    if sz <> size v then invalid_arg "Vector.mul: mismatched dimensions";
    for i = 0 to (sz-1) do
      res.(i) <- v.(i) *% s
    done
      
  let addmul res s v = (* res := res + s.v *)
    let sz = size res in
    if sz <> size v then invalid_arg "Vector.addmul: mismatched dimensions";
    for i = 0 to (sz-1) do
      let x = v.(i) *% s in
      if x >% res.(i) then 
	res.(i) <- x
    done

  let scalar v w = (* computes v|w *)
    let sz = size v in
    if sz <> size w then invalid_arg "Vector.scalar: mismatched dimensions";
    let rec scalar_aux i =
      if i >= sz then S.zero
      else
	(v.(i) *% w.(i)) +% (scalar_aux (i+1))
    in
      scalar_aux 0

  let can len i =
    let v = make len S.zero in
    v.(i) <- S.one;
    v

  let is_null v =
    let sz = size v in
    try
      for i = 0 to sz-1 do
	if v.(i) <>% S.zero then
	  raise Exit
      done;
      true
    with Exit -> false

  let residuate v w =
    let sz = size v in
    if sz <> size w then invalid_arg "Vector.residuate: mismatched dimensions";
    let res = ref S.zero 
    and is_pinfty = ref true in
      for i = 0 to (sz-1) do
	if !is_pinfty then
	  try
	    res := (w.(i)) *% (~-% (v.(i)));
	    is_pinfty := false
	  with S.Pinfty -> ()
	else
	  try
	    res := S.min !res (w.(i) *% (~-% (v.(i))))
	  with S.Pinfty -> ()
      done;
      if !is_pinfty then
	raise S.Pinfty
      else
	!res 

  let hilbert_distance v w = 
    (~-% ((residuate v w) *% (residuate w v)))

  let projectively_equal v w = 
    try
      (hilbert_distance v w) =% S.one
    with S.Pinfty -> false

  let iter f inf sup v = 
    for i = inf to (sup-1) do
      f v.(i) i
    done

  let add_dimensions w v dim intdim =
    let sz = size v in
    let k = ref intdim in
      for i = sz downto 0 do
	if i < sz then
	  w.(i + !k) <- v.(i);
	while (!k >= 1) && (dim.(!k-1) = i) do 
	  k := !k-1;
	  w.(i + !k) <- S.zero
	done
      done

  let remove_dimensions w v dim intdim =
    let sz = size w in
    let k = ref 0 in
      for i = 0 to (sz-1) do
	while (!k < intdim) && (dim.(!k) = i+ !k) do
	  k := !k+1
	done;
	w.(i) <- v.(i+ !k)
      done

  let permute_dimensions w v perm =
    for i = 0 to Array.length perm-1 do
      w.(i) <- v.(perm.(i))
    done
      
  let concat res v w =
    let sz = size v
    and sz' = size w in
    for i = 0 to sz-1 do
      res.(i) <- v.(i)
    done;
    for i = 0 to sz'-1 do
      res.(sz+i) <- w.(i)
    done

  let polymake_print fmt v = 
    let sz = size v in
    for i = 0 to (sz-1) do
      Format.fprintf fmt "%s @?" (S.to_string ~neg:true v.(i))
    done

  let to_string ?(neg=false) v =
    let sz = size v in
    let rec to_string_aux i =
      if i >= sz then
	"]"
      else if i = sz-1 then
	(S.to_string ~neg v.(i))^"]"
      else 
	(S.to_string ~neg v.(i))^","
	^(to_string_aux (i+1))
    in
      "["^(to_string_aux 0)

  let of_string ?(neg=false) ?supp len s = 
    let v = make len S.zero in
    let rec of_string_aux s j i1 =
      try
	let i2 = String.index_from s i1 ',' in
	let x = S.of_string ~neg (String.sub s i1 (i2-i1)) in
	  v.(j) <- x;
	  of_string_aux s (j+1) (i2+1)
      with Not_found ->
	let i2 = String.index_from s i1 ']' in
	let x = S.of_string ~neg (String.sub s i1 (i2-i1)) in
	  v.(j) <- x;
	j
    in
      try
	let i = of_string_aux s 0 ((String.index s '[') + 1) in
	match supp with
	  | None -> 
	    if i <> len-1 then
	      invalid_arg ("Vector.of_string: the size of the vector "
			   ^s^" is expected to have size "^(string_of_int len));
	    v
	  | Some x -> 
	    v.(len-1) <- x; v
      with Not_found | Failure _ -> raise (Failure ("Vector "^s^" is not well-formatted"))

  let hash v = Hashtbl.hash (Digest.string (to_string v))

end
