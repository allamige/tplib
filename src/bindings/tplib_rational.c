/*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <caml/alloc.h>
#include <caml/callback.h>
#include <caml/memory.h>
#include <caml/mlvalues.h>
#include <gmp.h>
#include <zarith.h>
#include "tplib_rational.h"

struct _poly_t {
  value ml_val;
};

struct _matrix_t {
  int rows;
  int cols;
  mpq_t *coeffs;
};

void ml_q_mpq_init_set_q(mpq_t res, value v) {
  CAMLparam1(v);
  assert(Is_block(v));
  mpq_init(res);
  ml_z_mpz_set_z(mpq_numref(res),Field(v,0));
  ml_z_mpz_set_z(mpq_denref(res),Field(v,1));
  CAMLreturn0;
}

void ml_q_mpq_set_q(mpq_t res, value v) {
  CAMLparam1(v);
  assert(Is_block(v));
  ml_z_mpz_set_z(mpq_numref(res),Field(v,0));
  ml_z_mpz_set_z(mpq_denref(res),Field(v,1));
  CAMLreturn0;
}

value ml_q_from_mpq(mpq_t x) {
  CAMLparam0();
  CAMLlocal3(res,num,den);
  num = ml_z_from_mpz(mpq_numref(x));
  den = ml_z_from_mpz(mpq_denref(x));
  res = caml_alloc(2,0);
  Store_field(res,0,num);
  Store_field(res,1,den);
  CAMLreturn(res);
}


matrix_t* matrix_alloc(int rows, int cols) {
  matrix_t* res = (matrix_t*) malloc(sizeof(matrix_t));
  int i;
  res->rows = rows;
  res->cols = cols;
  res->coeffs = (mpq_t*) malloc((rows*cols)*sizeof(mpq_t));
  for (i = 0; i < rows*cols; i++)
    mpq_init(res->coeffs[i]);
  return res;
}

matrix_t* matrix_of_array(int rows, int cols, mpq_t *coeffs) {
  matrix_t* res = (matrix_t*) malloc(sizeof(matrix_t));
  res->rows = rows;
  res->cols = cols;
  res->coeffs = coeffs;
  return res;
}

mpq_t* matrix_to_array(matrix_t* m) {
  return m->coeffs;
}

int matrix_get_nb_rows(matrix_t* m) {
  return m->rows;
}

int matrix_get_nb_columns(matrix_t *m) {
  return m->cols;
}

void matrix_get(mpq_t res, matrix_t* m, int row, int col) {
  mpq_set(res, m->coeffs[(row*(m->cols))+col]);
}

void matrix_set(matrix_t* m, int row, int col, mpq_t coeff) {
  mpq_set(m->coeffs[(row*(m->cols))+col],coeff);
}

void matrix_set_ui(matrix_t* m, int row, int col, unsigned long int num, unsigned long int den) {
  mpq_set_ui(m->coeffs[(row*(m->cols))+col],num,den);
}

void matrix_set_si(matrix_t* m, int row, int col, signed long int num, unsigned long int den) {
  mpq_set_si(m->coeffs[(row*(m->cols))+col],num,den);
}

void matrix_print(matrix_t *m) {
  int i, j;
  for (i = 0; i < m->rows; i++) {
    for (j = 0; j < m->cols; j++) {
      if (j == 0)
	gmp_printf("[%Qd",m->coeffs[i*(m->cols)+j]);
      else
	gmp_printf(",%Qd",m->coeffs[i*(m->cols)+j]);
    }
    printf("]\n");
  }
}

void matrix_free(matrix_t* m) {
  int i;
  if (m->coeffs) {
    for (i = 0; i < m->cols * m->rows; i++)
      mpq_clear(m->coeffs[i]);
    free(m->coeffs);
  }
  free(m);
}

matrix_t* to_c_vector_list(value v) {
  CAMLparam1(v);
  CAMLlocal2(hd,tl);
  matrix_t *res;
  mpq_t x;
  int i, j, p;
  int n = 0;

  /* first get the length of the list v */
  tl = v;
  while (tl != Val_emptylist) {
    n++;
    tl = Field(tl,1);
  }

  if (n > 0) {
    /* now get the dimension of the vectors */
    hd = Field(v,0);
    assert (Is_block(hd));
    p = Wosize_val(hd);
     
    /* allocate the result */
    res = matrix_alloc(n,p);

    /* copy the coefficients in the matrix */
    i = 0;
    tl = v;
    while (tl != Val_emptylist) {
      hd = Field(tl,0);
      for (j = 0; j < p; j++) {
	ml_q_mpq_set_q(res->coeffs[i*p+j], Field(hd,j));
      }
      tl = Field(tl,1);
      i++;
    }
  }
  else {
    res = NULL;
  }
  CAMLreturnT(matrix_t*, res);
}

value to_ml_vector_list(matrix_t *m) {
  CAMLparam0();
  CAMLlocal3(res,hd,tl);
  int i,j;
  res = Val_emptylist;
  for (i = m->rows-1; i >= 0; i--) {
    tl = res;
    hd = caml_alloc(m->cols, 0);
    for (j = 0; j < m->cols; j++)
      Store_field(hd, j, ml_q_from_mpq(m->coeffs[i*(m->cols)+j]));
    res = caml_alloc(2,0);
    Store_field(res, 0, hd);
    Store_field(res, 1, tl);
  }
  CAMLreturn(res);
}

value to_ml_int_list(int *x, int len) {
  CAMLparam0();
  CAMLlocal2(res,tl);
  int i;
  res = Val_emptylist;
  for (i = len-1; i >= 0; i--) {
    tl = res;
    res = caml_alloc(2,0);
    Store_field(res, 0, Val_int(x[i]));
    Store_field(res, 1, tl);
  }
  CAMLreturn(res);
}

value to_ml_int_array(int* x, int n) {
  CAMLparam0();
  CAMLlocal1(res);
  int i;
  res = caml_alloc(n, 0);
  for (i = 0; i < n; i++)
    Store_field(res, i, Val_int(x[i]));
  CAMLreturn(res);
}

#define Val_none Val_int(0)

static inline value Val_some( value v )
{   
  CAMLparam1(v);
  CAMLlocal1(some);
  some = caml_alloc(1, 0);
  Store_field(some, 0, v);
  CAMLreturn(some);
}

static poly_t* alloc_poly(value v)
{
  poly_t* x = (poly_t*) malloc(sizeof(poly_t));
  if (x == NULL) return NULL;
  x->ml_val = v;
  caml_register_generational_global_root(&(x->ml_val));
  return x;
}

static void modify_poly(poly_t* x, value v)
{
  if (x != NULL)
    caml_modify_generational_global_root(&(x->ml_val), v);
}

void poly_free(poly_t* x)
{
  if (x != NULL) {
    caml_remove_generational_global_root(&(x->ml_val));
    free(x);
  }
}

int dimension(poly_t* x) {
  CAMLparam0();
  CAMLlocal1(ml_res);
  int res;
  static value *closure_dimension = NULL;
  if (closure_dimension == NULL) {
    closure_dimension = caml_named_value("dimension_rational");
  }
  ml_res = caml_callback(*closure_dimension, x->ml_val);
  res = Int_val(ml_res);
  CAMLreturnT(int,res);
}

poly_t* copy(poly_t* x) {
  CAMLparam0();
  CAMLlocal1(ml_res);
  poly_t *res;
  static value *closure_copy = NULL;
  if (closure_copy == NULL) {
    closure_copy = caml_named_value("copy_rational");
  }
  ml_res = caml_callback(*closure_copy, x->ml_val);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

poly_t* normalize(poly_t* x) {
  CAMLparam0();
  CAMLlocal1(ml_res);
  poly_t *res;
  static value *closure_normalize = NULL;
  if (closure_normalize == NULL) {
    closure_normalize = caml_named_value("normalize_rational");
  }
  ml_res = caml_callback(*closure_normalize, x->ml_val);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

void set_strategy(strategy_t s) {
  CAMLparam0();
  CAMLlocal1(ml_s);
  static value *closure_set_strategy = NULL;
  if (closure_set_strategy == NULL) {
    closure_set_strategy = caml_named_value("set_strategy_rational");
  }
  ml_s = Val_int(s);
  caml_callback(*closure_set_strategy, ml_s);
  CAMLreturn0;
}

strategy_t get_strategy(void) {
  CAMLparam0();
  CAMLlocal1(ml_res);
  static value *closure_get_strategy = NULL;
  if (closure_get_strategy == NULL) {
    closure_get_strategy = caml_named_value("get_strategy_rational");
  }
  ml_res = caml_callback(*closure_get_strategy, Val_unit);
  CAMLreturnT(strategy_t, Int_val(ml_res));
}

poly_t* apply_strategy(poly_t* x) {
  CAMLparam0();
  CAMLlocal1(ml_res);
  poly_t *res;
  static value *closure_apply_strategy = NULL;
  if (closure_apply_strategy == NULL) {
    closure_apply_strategy = caml_named_value("apply_strategy_rational");
  }
  ml_res = caml_callback(*closure_apply_strategy, x->ml_val);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

poly_t* of_cons(int dim, matrix_t* cons) {
  CAMLparam0();
  CAMLlocal2(ml_cons, ml_res);
  poly_t* res;
  static value *closure_of_cons = NULL;
  if (closure_of_cons == NULL) {
    closure_of_cons = caml_named_value("of_cons_rational");
  }
  ml_cons = to_ml_vector_list(cons);
  ml_res = caml_callback2(*closure_of_cons, Val_int(dim), ml_cons);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

poly_t* of_gen(int dim, matrix_t* gen) {
  CAMLparam0();
  CAMLlocal2(ml_gen, ml_res);
  poly_t* res;
  static value *closure_of_gen = NULL;
  if (closure_of_gen == NULL) {
    closure_of_gen = caml_named_value("of_gen_rational");
  }
  ml_gen = to_ml_vector_list(gen);
  ml_res = caml_callback2(*closure_of_gen, Val_int(dim), ml_gen);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

matrix_t* to_cons(poly_t *x) {
  CAMLparam0();
  CAMLlocal1(ml_res);
  int dim = dimension(x);
  matrix_t *res;
  static value *closure_to_cons = NULL;
  if (closure_to_cons == NULL) {
    closure_to_cons = caml_named_value("to_cons_rational");
  }
  ml_res = caml_callback(*closure_to_cons, x->ml_val);
  res = to_c_vector_list(ml_res);
  CAMLreturnT(matrix_t*, res);
}

matrix_t* to_gen(poly_t *x) {
  CAMLparam0();
  CAMLlocal1(ml_res);
  int dim = dimension(x);
  matrix_t *res;
  static value *closure_to_gen = NULL;
  if (closure_to_gen == NULL) {
    closure_to_gen = caml_named_value("to_gen_rational");
  }
  ml_res = caml_callback(*closure_to_gen, x->ml_val);
  res = to_c_vector_list(ml_res);
  CAMLreturnT(matrix_t*, res);
}

int is_leq(poly_t *x1, poly_t *x2) {
  CAMLparam0();
  CAMLlocal1(ml_res);
  static value *closure_is_leq = NULL;
  if (closure_is_leq == NULL) {
    closure_is_leq = caml_named_value("is_leq_rational");
  }
  ml_res = caml_callback2(*closure_is_leq, x1->ml_val, x2->ml_val);
  CAMLreturnT(int, Bool_val(ml_res));
}

int sat_cons(poly_t* x, matrix_t* cons) {
  CAMLparam0();
  CAMLlocal2(ml_cons, ml_res);
  static value *closure_sat_cons = NULL;
  if (closure_sat_cons == NULL) {
    closure_sat_cons = caml_named_value("sat_cons_rational");
  }
  ml_cons = to_ml_vector_list(cons);
  ml_res = caml_callback2(*closure_sat_cons, x->ml_val, ml_cons);
  CAMLreturnT(int, Bool_val(ml_res));
}

poly_t* bottom(int dim) {
  CAMLparam0();
  CAMLlocal2(ml_dim, ml_res);
  poly_t* res;
  static value *closure_bottom = NULL;
  if (closure_bottom == NULL) {
    closure_bottom = caml_named_value("bottom_rational");
  }
  ml_dim = Val_int(dim);
  ml_res = caml_callback(*closure_bottom, ml_dim);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

int is_bottom(poly_t* x) {
  CAMLparam0();
  CAMLlocal1(ml_res);
  static value *closure_is_bottom = NULL;
  if (closure_is_bottom == NULL) {
    closure_is_bottom = caml_named_value("is_bottom_rational");
  }
  ml_res = caml_callback(*closure_is_bottom, x->ml_val);
  CAMLreturnT(int, Bool_val(ml_res));
}

poly_t* top(int dim) {
  CAMLparam0();
  CAMLlocal2(ml_dim, ml_res);
  poly_t* res;
  static value *closure_top = NULL;
  if (closure_top == NULL) {
    closure_top = caml_named_value("top_rational");
  }
  ml_dim = Val_int(dim);
  ml_res = caml_callback(*closure_top, ml_dim);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

int is_top(poly_t* x) {
  CAMLparam0();
  CAMLlocal1(ml_res);
  static value *closure_is_top = NULL;
  if (closure_is_top == NULL) {
    closure_is_top = caml_named_value("is_top_rational");
  }
  ml_res = caml_callback(*closure_is_top, x->ml_val);
  CAMLreturnT(int, Bool_val(ml_res));
}

poly_t* join(poly_t *x1, poly_t *x2) {
  CAMLparam0();
  CAMLlocal1(ml_res);
  poly_t* res;
  static value *closure_join = NULL;
  if (closure_join == NULL) {
    closure_join = caml_named_value("join_rational");
  }
  ml_res = caml_callback2(*closure_join, x1->ml_val, x2->ml_val);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

poly_t* meet_cons(poly_t *x, matrix_t* cons) {
  CAMLparam0();
  CAMLlocal2(ml_cons, ml_res);
  poly_t* res;
  static value *closure_meet_cons = NULL;
  if (closure_meet_cons == NULL) {
    closure_meet_cons = caml_named_value("meet_cons_rational");
  }
  ml_cons = to_ml_vector_list(cons);
  ml_res = caml_callback2(*closure_meet_cons, x->ml_val, ml_cons);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

poly_t* meet(poly_t *x1, poly_t *x2) {
  CAMLparam0();
  CAMLlocal1(ml_res);
  poly_t* res;
  static value *closure_meet = NULL;
  if (closure_meet == NULL) {
    closure_meet = caml_named_value("meet_rational");
  }
  ml_res = caml_callback2(*closure_meet, x1->ml_val, x2->ml_val);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

poly_t* assign(poly_t *x, int* var, matrix_t* expr) {
  CAMLparam0();
  CAMLlocal3(ml_var, ml_expr, ml_res);
  poly_t* res;
  static value *closure_assign = NULL;
  if (closure_assign == NULL) {
    closure_assign = caml_named_value("assign_rational");
  }
  ml_var = to_ml_int_list(var, expr->rows);
  ml_expr = to_ml_vector_list(expr);
  ml_res = caml_callback3(*closure_assign, x->ml_val, ml_var, ml_expr);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

poly_t* substitute(poly_t *x, int* var, matrix_t* expr) {
  CAMLparam0();
  CAMLlocal3(ml_var, ml_expr, ml_res);
  poly_t* res;
  static value *closure_substitute = NULL;
  if (closure_substitute == NULL) {
    closure_substitute = caml_named_value("substitute_rational");
  }
  ml_var = to_ml_int_list(var, expr->rows);
  ml_expr = to_ml_vector_list(expr);
  ml_res = caml_callback3(*closure_substitute, x->ml_val, ml_var, ml_expr);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

poly_t* add_dimensions(poly_t *x, int *dim, int intdim, int constrained) {
  CAMLparam0();
  CAMLlocal4(ml_dim, ml_intdim, ml_constrained, ml_res);
  value ml_args[4];
  poly_t* res;
  static value *closure_add_dimensions = NULL;
  if (closure_add_dimensions == NULL) {
    closure_add_dimensions = caml_named_value("add_dimensions_rational");
  }
  ml_dim = to_ml_int_array(dim, intdim);
  ml_intdim = Val_int(intdim);
  ml_constrained = Val_bool(constrained);
  ml_args[0] = x->ml_val;
  ml_args[1] = ml_dim;
  ml_args[2] = ml_intdim;
  ml_args[3] = ml_constrained;
  ml_res = caml_callbackN(*closure_add_dimensions, 4, ml_args);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

poly_t* remove_dimensions(poly_t *x, int *dim, int intdim) {
  CAMLparam0();
  CAMLlocal3(ml_dim, ml_intdim, ml_res);
  poly_t* res;
  static value *closure_remove_dimensions = NULL;
  if (closure_remove_dimensions == NULL) {
    closure_remove_dimensions = caml_named_value("remove_dimensions_rational");
  }
  ml_dim = to_ml_int_array(dim, intdim);
  ml_intdim = Val_int(intdim);
  ml_res = caml_callback3(*closure_remove_dimensions, x->ml_val, ml_dim, ml_intdim);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

poly_t* permute_dimensions(poly_t *x, int *perm) {
  CAMLparam0();
  CAMLlocal2(ml_perm, ml_res);
  poly_t *res;
  int dim = dimension(x);
  static value *closure_permute_dimensions = NULL;
  if (closure_permute_dimensions == NULL) {
    closure_permute_dimensions = caml_named_value("permute_dimensions_rational");
  }
  if (perm == NULL) 
    ml_perm = Val_none;
  else
    ml_perm = Val_some(to_ml_int_array(perm, dim));
  ml_res = caml_callback2(*closure_permute_dimensions, x->ml_val, ml_perm);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

poly_t* widen(poly_t *x1, poly_t *x2) {
  CAMLparam0();
  CAMLlocal1(ml_res);
  poly_t* res;
  static value *closure_widen = NULL;
  if (closure_widen == NULL) {
    closure_widen = caml_named_value("widen_rational");
  }
  ml_res = caml_callback2(*closure_widen, x1->ml_val, x2->ml_val);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

poly_t* widen_generators(poly_t *x1, poly_t *x2) {
  CAMLparam0();
  CAMLlocal1(ml_res);
  poly_t* res;
  static value *closure_widen_generators = NULL;
  if (closure_widen_generators == NULL) {
    closure_widen_generators = caml_named_value("widen_generators_rational");
  }
  ml_res = caml_callback2(*closure_widen_generators, x1->ml_val, x2->ml_val);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

poly_t* narrow(poly_t *x1, poly_t *x2) {
  CAMLparam0();
  CAMLlocal1(ml_res);
  poly_t* res;
  static value *closure_narrow = NULL;
  if (closure_narrow == NULL) {
    closure_narrow = caml_named_value("narrow_rational");
  }
  ml_res = caml_callback2(*closure_narrow, x1->ml_val, x2->ml_val);
  res = alloc_poly(ml_res);
  CAMLreturnT(poly_t*, res);
}

void remove_redundant_constraints(poly_t* x) {
  CAMLparam0();
  static value *closure_remove_redundant_constraints = NULL;
  if (closure_remove_redundant_constraints == NULL) {
    closure_remove_redundant_constraints = caml_named_value("remove_redundant_constraints_rational");
  }
  caml_callback(*closure_remove_redundant_constraints, x->ml_val);
  CAMLreturn0;
}

void call_gc(void) {
  CAMLparam0();
  static value *closure_call_gc = NULL;
  if (closure_call_gc == NULL) {
    closure_call_gc = caml_named_value("call_gc_rational");
  }
  caml_callback(*closure_call_gc, Val_unit);
  CAMLreturn0;
}

void print_poly(poly_t *x) {
  int dim;
  matrix_t* gen;
  int i, j;
  if (is_bottom(x)) 
    printf("bottom\n");
  else {
    gen = to_gen(x);
    matrix_print(gen);
    matrix_free(gen);
  }
}

void init(void) {
  char* dummy = '\0';
  caml_main(&dummy);
}
