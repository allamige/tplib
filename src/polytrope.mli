(*
  TPLib: Tropical Polyhedra Library 

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module type T = 
sig
  type scalar_t 
  type vector_t 
  type t
  
  val bottom : int -> t
  val top : int -> t
  val dimension : t -> int
  val is_empty : t -> bool
  val is_leq : t -> t -> bool
  val meet : t -> t -> t
  val is_full_dimensional : t -> bool
  val is_bounded : t -> bool
  val of_gen : int -> vector_t list -> t
  val to_gen : t -> vector_t list
  val of_matrix : ?is_closed:bool -> int -> scalar_t array -> t
  val to_matrix : t -> scalar_t array option
  val hull_dimension : t -> int
  val hull_basis : t -> int list list
  val facets : t -> t list
  val pseudo_vertices : t -> vector_t list
  val centroid : t -> vector_t option

  val alloc : int -> scalar_t array
  val get : int -> scalar_t array -> int -> int -> scalar_t
  val set : int -> scalar_t array -> int -> int -> scalar_t -> unit
  val closure : int -> scalar_t array -> t

  val print : Format.formatter -> t -> unit
  val polymake_print : string -> Format.formatter -> t -> unit
end

module Make :
  functor (S : Semiring.T) -> 
    functor (V: Vector.T with type scalar_t = S.t) -> T with type scalar_t = S.t
                                                        and type vector_t = V.t

