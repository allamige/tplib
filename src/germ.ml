(*
  TPLib: Tropical Polyhedra Library 

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module type T = 
sig
  include Semiring.T

  type scalar_t

  val of_int2 : int -> int -> t
  val of_scalar : scalar_t -> t
  val of_scalar2 : scalar_t -> scalar_t -> t
  val to_scalar : t -> scalar_t
end

module Make (S: Semiring.T) = struct
  type t = Zero | Non_zero of S.t * S.t

  type num_t = S.Num.t

  type scalar_t = S.t

  module Num = S.Num

  open S.Infixes

  exception Pinfty

  let zero = Zero

  let one = Non_zero (S.one, S.one)

  let compare g1 g2 =
    match (g1, g2) with
      | (Zero, Zero) -> 0
      | (Zero, _) -> -1
      | (_, Zero) -> 1
      | (Non_zero (x1, y1), Non_zero (x2, y2)) ->
          if x1 <% x2 then -1
          else if x1 =% x2 then begin
            if y1 <% y2 then -1
            else if y1 =% y2 then 0
            else 1
          end
          else 1

  let add g1 g2 = if compare g1 g2 >= 0 then g1 else g2

  let mul g1 g2 = 
    match (g1, g2) with
      | (Zero, _) | (_, Zero) -> Zero
      | (Non_zero (x1, y1), Non_zero (x2, y2)) -> Non_zero (x1 *% x2, y1 *% y2)

  let min g1 g2 = if compare g1 g2 <= 0 then g1 else g2

  let neg g = 
    match g with
      | Zero -> raise Pinfty
      | Non_zero (x,y) -> Non_zero (~-% x, ~-% y)

  let random n = Non_zero (S.random n, S.one)
    
  let of_int x = Non_zero (S.of_int x, S.one)

  let of_scalar x = 
    if x =% S.zero then Zero
    else Non_zero (x, S.one)
      
  let of_num x = of_scalar (S.of_num x)

  let of_int2 x y = Non_zero (S.of_int x, S.of_int y)

  let of_scalar2 x y = 
    if x =% S.zero || y =% S.zero then Zero
    else Non_zero (x, y)

  let of_string ?(neg=false) s = 
    let s = S.of_string ~neg s in
    if s <>% S.zero then Non_zero (s, S.one)
    else Zero

  let to_string ?(neg=false) g =
    match g with 
      | Zero -> if not neg then "-oo" else "+oo"
      | Non_zero (x, y) ->
          if y =% S.one then S.to_string ~neg x
          else if (y >% S.one && (not neg)) || (y <% S.one && neg) then
            (S.to_string ~neg x)^"+"^(S.to_string ~neg y)^"*eps"
          else
            (S.to_string ~neg x)^(S.to_string ~neg y)^"*eps"
     
  let to_scalar g = 
    match g with
      | Zero -> S.zero
      | Non_zero (x,_) -> x
          
  let to_num g = S.to_num (to_scalar g)
         
  module Infixes = struct
    let ( +% ) = add 
    let ( *% ) = mul
    let ( ~-% ) = neg
      
    let ( =% ) x y = (compare x y = 0)
    let ( <=% ) x y = (compare x y <= 0)
    let ( <% ) x y = (compare x y < 0)
    let ( >=% ) x y = (compare x y >= 0)
    let ( >% ) x y = (compare x y > 0)
    let ( <>% ) x y = (compare x y <> 0)
  end
end
