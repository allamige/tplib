(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module type T = sig
  type vector_t
  type t = { apex : vector_t; sectors : int list; }
  exception Unbounded_apex
  val of_inequality : ?gen:vector_t list -> vector_t -> t
  val to_inequality : t -> vector_t
  val complementary : t -> t
  val belong : t -> vector_t -> bool
  val print : ?neg:bool -> Format.formatter -> t -> unit
  val equal : t -> t -> bool
  val hash : t -> int
end

module Make (S: Semiring.T) (V: Vector.T with type scalar_t = S.t) = struct
  type vector_t = V.t

  type t = { apex : V.t;
	     sectors : int list } (* invariant: sectors is a sorted list *)

  exception Unbounded_apex

  open S.Infixes

  let of_inequality ?gen c = (* TODO: add assertion on the finiteness 
			      * of the entries of the vectors in gen? *)
    let d = (V.size c)/2 in
    let a = V.make d S.zero in
     let rec of_inequality_aux res i =
      if i < 0 then
	res
      else if i <= d-1 then
	let x = V.get c i 
	and y = V.get c (d+i) in
	if x >=% y then begin
	  V.set a i x;
	  of_inequality_aux (i::res) (i-1)
	end
	else begin
	  V.set a i y;
	  of_inequality_aux res (i-1)
	end
      else
	assert false
    in
    let sectors = of_inequality_aux [] (d-1) in
    match gen with
      | None -> begin
	try
	  for i = 0 to (d-1) do
	    V.set a i (S.neg (V.get a i))
	  done;
	  V.scale a;
	  { apex = a; sectors = sectors }
	with S.Pinfty -> raise Unbounded_apex
      end
      | Some gen ->
	let b = V.make d S.zero in 
	let residuate g =
	  let res = ref S.zero 
	  and is_pinfty = ref true in
	  for i = 0 to (d-1) do
	    if !is_pinfty then
	      try
		res := (~-% (V.get a i)) *% (~-% (V.get g i));
		is_pinfty := false
	      with S.Pinfty -> ()
	    else
	      try
		res := S.min !res ((~-% (V.get a i)) *% (~-% (V.get g i)))
	      with S.Pinfty -> ()
	  done;
	  if !is_pinfty then
	    raise S.Pinfty
	  else
	    !res
	in
	try
	  List.iter (fun g -> let c = residuate g in V.addmul b c g) gen;
	  let sectors' = List.filter (fun i ->
	    try
	      V.get b i =% ~-% (V.get a i)
	    with S.Pinfty -> false) sectors in
	  V.scale b;
	  { apex = b; sectors = sectors' }
	with S.Pinfty -> raise Unbounded_apex

  let to_inequality h =
    let d = V.size h.apex in
    let c = V.make (2*d) S.zero in
    let rec to_inequality_aux s i = 
      assert (i >= 0);
      if i >= d then
	()
      else
	match s with
	  | [] -> 
	    V.set c (i+d) (S.neg (V.get h.apex i));
	    to_inequality_aux s (i+1)
	  | j::_ when j > i ->
	    V.set c (i+d) (S.neg (V.get h.apex i));
	    to_inequality_aux s (i+1)
	  | j::s' when j = i ->
	    V.set c i (S.neg (V.get h.apex i));
	    to_inequality_aux s' (i+1)
	  | _ -> assert false
    in
    to_inequality_aux h.sectors 0;
    c
     
  let complementary h =
    let d = V.size h.apex in
    let rec add_sectors res l u =
      if l >= u then res
      else l::(add_sectors res (l+1) u)
    in
    let rec complementary_aux sectors i =
      match sectors with
        | [] -> add_sectors [] i d
        | j::sectors' when i < j ->
          add_sectors (complementary_aux sectors' (j+1)) i j
        | j::sectors' when i = j ->
          complementary_aux sectors' (j+1) 
        | _ -> assert false
    in
    { apex = V.copy h.apex; 
      sectors = complementary_aux h.sectors 0 }
 
  let belong hs v = 
    let dim = V.size hs.apex in
    let rec belong_aux (max1, max2) sect j = 
      if j >= dim then (max1 >=% max2)
      else
        let c = (~-% (V.get hs.apex j)) *% (V.get v j) in
        match sect with
          | [] -> 
              belong_aux (max1, c +% max2) sect (j+1)
          | i::sect' when j <> i ->
              belong_aux (max1, c +% max2) sect (j+1) 
          | i::sect' (* when j = i *) ->
              belong_aux (max1 +% c, max2) sect' (j+1)
    in
    belong_aux (S.zero, S.zero) hs.sectors 0

  let print ?(neg=false) fmt h =
    Format.fprintf fmt "(%s,[" (V.to_string ~neg h.apex);
    let first = ref true in
    List.iter 
      (fun i -> if !first then
	  first := false
	else
	  Format.printf ",";
	Format.printf "%d" i) h.sectors;
    Format.printf "])"
      
  let equal h h' =
    (V.equal h.apex h'.apex) && (h.sectors = h'.sectors)
      
  let hash h =
    Hashtbl.hash (V.hash h.apex,h.sectors)
end
