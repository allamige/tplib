(*
  TPLib: Tropical Polyhedra Library 

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module type T = 
sig
  type scalar_t 
  type vector_t 
  type t
  
  val bottom : int -> t
  val top : int -> t
  val dimension : t -> int
  val is_empty : t -> bool
  val is_leq : t -> t -> bool
  val meet : t -> t -> t
  val is_full_dimensional : t -> bool
  val is_bounded : t -> bool
  val of_gen : int -> vector_t list -> t
  val to_gen : t -> vector_t list 
  val of_matrix : ?is_closed:bool -> int -> scalar_t array -> t
  val to_matrix : t -> scalar_t array option
  val hull_dimension : t -> int
  val hull_basis : t -> int list list
  val facets : t -> t list
  val pseudo_vertices : t -> vector_t list
  val centroid : t -> vector_t option

  val alloc : int -> scalar_t array
  val get : int -> scalar_t array -> int -> int -> scalar_t
  val set : int -> scalar_t array -> int -> int -> scalar_t -> unit
  val closure : int -> scalar_t array -> t

  val print : Format.formatter -> t -> unit
  val polymake_print : string -> Format.formatter -> t -> unit
end

module Make(S: Semiring.T) (V: Vector.T with type scalar_t = S.t) = struct

  open S.Infixes
    
  type scalar_t = S.t
      
  type vector_t = V.t
      
  type t = { dim: int;
             mat: (S.t array) option }

  let alloc dim = 
    Array.init (dim*dim) (fun _ -> S.zero)
      
  let get dim m i j = 
    m.(i*dim + j)
      
  let set dim m i j s = 
    m.(i*dim + j) <- s
      
  let closure dim m = 
    for k = 0 to (dim-1) do
      for i = 0 to (dim-1) do
        for j = 0 to (dim-1) do
          let x = get dim m i j in
          set dim m i j (x +% ((get dim m i k) *% (get dim m k j)))
        done
      done
    done;
    try
      for i = 0 to (dim-1) do
        if get dim m i i >% S.one then
          raise Exit
      done;
      { dim = dim; mat = Some m }
    with Exit -> { dim = dim; mat = None }
      
  let is_empty c = (c.mat = None)

  let is_leq c1 c2 =
    assert (c1.dim = c2.dim);
    match (c1.mat, c2.mat) with
      | (None, _) -> true
      | (_, None) -> false
      | (Some m1, Some m2) ->
          let dim = c1.dim in
          try
            for i = 0 to (dim-1) do
              for j = 0 to (dim-1) do
                if get dim m1 i j <% get dim m2 i j then 
                  raise Exit
              done
            done;
            true
          with Exit -> false
            
  let meet c1 c2 =
    assert (c1.dim = c2.dim);
    match (c1.mat, c2.mat) with
      | (None, _) | (_, None) -> { c1 with mat = None }
      | (Some m1, Some m2) ->
          let dim = c1.dim in
          let m = alloc dim in
          for i = 0 to (dim-1) do
            for j = 0 to (dim-1) do
              set dim m i j ((get dim m1 i j) +% (get dim m2 i j))
            done
          done;
          closure dim m
            
  let is_full_dimensional c =
    match c.mat with
      | None -> false
      | Some m -> 
          let dim = c.dim in
          try
            for i = 0 to (dim-1) do
              for j = i+1 to (dim-1) do
                if (get dim m i j) *% (get dim m j i) =% S.one then
                  raise Exit
              done
            done;
            true
          with Exit -> false
            
  let is_bounded c =
    match c.mat with
      | None -> true
      | Some m -> 
          let dim = c.dim in
          try
            for i = 0 to (dim-1) do
              for j = 0 to (dim-1) do
                if get dim m i j =% S.zero then
                  raise Exit
              done
            done;
            true
          with Exit -> false
            
  let to_gen c =
    let dim = c.dim in
    match c.mat with
      | None -> []
      | Some m ->
          let rec to_abs_aux res j = 
            if j >= dim then
              res
            else
              let v = V.make dim S.zero in
              for i = 0 to (dim-1) do
                V.set v i (get dim m i j)
              done;
              to_abs_aux (v::res) (j+1)
          in
          let rec eliminate_redundant res l =
            match l with
              | [] -> res
              | v::l' -> 
                  if List.exists (fun w -> V.hilbert_distance v w =% S.one) l' then
                    eliminate_redundant res l'
                  else
                    eliminate_redundant (v::res) l'
          in
          let gen = to_abs_aux [] 0 in
          eliminate_redundant [] gen

  let of_gen dim gen = 
    match gen with
      | [] -> { dim = dim; mat = None }
      | _ ->
          let m = alloc dim in
          for i = 0 to (dim-1) do
            for j = 0 to (dim-1) do
              if i <> j then
                let x = List.fold_left (fun res v ->
                  res +% ((V.get v j) *% (~-% (V.get v i)))) S.zero gen in
                set dim m i j (~-% x) (* FIX ME: +oo pb here *)
              else
                set dim m i j S.one
            done
          done;
          { dim = dim; mat = Some m }

  let bottom dim = { dim = dim; mat = None }

  let top dim = 
    let m = alloc dim in
    for i = 0 to (dim-1) do
      set dim m i i S.one
    done;
    { dim = dim; mat = Some m }
    
  let dimension c = c.dim

  let hull_dimension c = 
    match c.mat with
      | None -> -1
      | Some m -> (List.length (to_gen c)) - 1

  let hull_basis c = 
    let gen0 = to_gen c in
    match gen0 with
      | [] -> []
      | g0::gen -> 
          let dim = V.size g0 in 
          let visited = Array.init dim (fun _ -> false) in
          let res = ref [] in
          for i = dim-1 downto 0 do
            if not visited.(i) then begin
              let comp = ref [i] in
              for j = i-1 downto 0 do
                if not visited.(j) then
                  let x0 = (~-% (V.get g0 i)) *% (V.get g0 j) in
                  if List.for_all (fun g -> (~-% (V.get g i)) *% (V.get g j) =% x0) gen then begin
                    visited.(j) <- true;
                    comp := j::(!comp)
                  end
              done;
              res := (!comp)::(!res)
            end
          done;
          !res
            
  let of_matrix ?(is_closed=false) dim m =
    if is_closed then
      { dim = dim; mat = Some m }
    else
      closure dim m

  let facets c = (* we assume here that c is full dimensional *)
    let hull_dim = hull_dimension c in
    (*assert (is_full_dimensional c);*)
    let dim = c.dim in
    match c.mat with
      | None -> []
      | Some m ->
          let res = ref [] in
          let facets_aux i j = 
            let m' = alloc dim in
            for k = 0 to (dim-1) do
              set dim m' k k S.one
            done;
            try
              set dim m' j i (~-% (get dim m i j));
              let c' = { dim = dim; mat = Some m' } in
              let c'' = meet c c' in
              if (hull_dimension c'' = hull_dim-1) && (List.for_all (fun c''' -> not (is_leq c'' c''')) !res) then
                 res := c''::!res
            with S.Pinfty -> ()
          in
          for i = 0 to (dim-1) do
            for j = 0 to (dim-1) do
              facets_aux i j 
            done
          done;
          !res
   
  let to_matrix c = c.mat

  module N = S.Num 

  module Tbl = Hashtbl.Make (struct
    type t = N.t array
    let equal v w =
      let dim = Array.length v in
      assert (Array.length w = dim);
      try
        for i = 0 to (dim-1) do
          if N.compare v.(i) w.(i) <> 0 then raise Exit
        done;
        true
      with Exit -> false
        
    let hash v =
      let dim = Array.length v in
      let l = ref [] in
      for i = 0 to (dim-1) do
        if N.compare v.(i) N.zero <> 0 then
          l := (i,N.to_string v.(i))::(!l)
      done;
      Hashtbl.hash (!l)
  end)

  module Set = struct
    let create n = Tbl.create n
    let add h v = Tbl.add h v ()
    let mem h v = Tbl.mem h v
  end

  let list_inclusion l1 l2 =
  (*List.for_all (fun i -> List.mem i l2) l1*)
    let rec list_inclusion_aux l1 l2 =
      match (l1,l2) with
        | ([],_) -> true
        | (_,[]) -> false
        | (h1::l1', h2::l2') ->
            if h1 < h2 then false
            else if h1 = h2 then list_inclusion_aux l1' l2'
            else (* if h1 > h2 then *) list_inclusion_aux l1 l2'
    in
    list_inclusion_aux l1 l2
      
  let list_intersection l1 l2 =
  (*List.filter (fun i -> List.mem i l2) l1*)
    let rec list_intersection_aux l1 l2 =
      match (l1,l2) with
        | ([],_) | (_,[]) -> []
        | (h1::l1', h2::l2') ->
            if h1 < h2 then 
              list_intersection_aux l1' l2
            else if h1 > h2 then
              list_intersection_aux l1 l2'
            else h1::(list_intersection_aux l1' l2')
    in
    list_intersection_aux l1 l2
      
  let pseudo_vertices cell = 
    match cell.mat with
      | None -> [] 
      | Some m -> 
          if not (is_bounded cell) then
            invalid_arg "Polytrope.pseudo_vertices: the polytrope is supposed to be bounded.";
          let dim = cell.dim in
          let scalar c x = 
            let res = ref N.zero in
            for i = 0 to (dim-1) do
              res := N.add (!res) (N.mul c.(i) x.(i))
            done;
            !res
          in
          let scale g =
            let x = ref N.zero in
            try
              for i = 0 to (dim-1) do
                if N.compare g.(i) N.zero <> 0 then begin
                  x := g.(i);
                  raise Exit
                end
              done
            with Exit ->
              let y = 
                if N.compare !x N.zero > 0 then !x
                else N.neg !x 
              in
              for i = 0 to (dim-1) do
                g.(i) <- N.div (g.(i)) y
              done
          in
          let to_cons () = 
            let res = ref [] in
            for i = 1 to (dim-1) do (* row 0 is handled by the max element *)
              for j = 0 to (dim-1) do
                if j <> i then
                  let y = get dim m i j in
                  if S.compare y S.zero <> 0 then
                    let cons = Array.init dim (fun _ -> N.zero) in
                    cons.(i) <- N.of_int 1;
                    cons.(j) <- N.of_int (-1);
                    cons.(0) <- N.neg (S.to_num y);
                    res := cons::!res
              done
            done;
            !res
    in
    let initial_pointed_cone () =
      let max_elt = V.make dim S.zero in
      for j = 0 to (dim-1) do
        for i = 0 to (dim-1) do
          let x = S.neg (get dim m 0 j) in
          V.set max_elt i (S.add (V.get max_elt i) 
                             (S.mul x (get dim m i j)))
        done
      done;
      assert (S.compare (V.get max_elt 0) S.one = 0);
      let max_elt' = Array.init dim (fun i -> S.to_num (V.get max_elt i)) in
      max_elt'.(0) <- N.of_int 1;
      scale max_elt';
      let sat_array = Array.init dim (fun _ -> []) in
      for i = dim-1 downto 0 do
        for j = 0 to dim-1 do
          if j <> i then
            sat_array.(j) <- i::sat_array.(j)
        done
      done;
      let rec neg_identity res i = 
        if i <= 0 then res
        else 
          let elt = Array.init dim (fun _ -> N.zero) in
          elt.(i) <- N.of_int (-1);
          neg_identity ((elt,sat_array.(i))::res) (i-1) 
      in
      neg_identity [(max_elt',sat_array.(0))] (dim-1) 
    in
    let partition c gen = 
      let partition_aux (res1,res2,res3) g =
        let x = scalar c (fst g) in
        match N.compare x N.zero with
          | y when y > 0 -> (g::res1,res2,res3)
          | 0 -> (res1,g::res2,res3)
          | _ (* when y < 0 *) -> (res1,res2,g::res3)
      in
      List.fold_left partition_aux ([],[],[]) gen
    in
    let are_adjacent gen g1 g2 =
      let sat_inter = list_intersection (snd g1) (snd g2) in
      let n = ref 0 in
      try
        List.iter (fun g -> 
          if list_inclusion sat_inter (snd g) then n := !n+1;
          if !n > 2 then raise Exit) gen;
        (true, sat_inter)
      with Exit -> (false, sat_inter) 
    in
    let combine c g1 g2 =
      let x = scalar c (fst g1)
      and y = N.neg (scalar c (fst g2)) in
      let g = Array.make dim N.zero in
      for i = 0 to (dim-1) do
        g.(i) <- N.add (N.mul x ((fst g2).(i))) (N.mul y ((fst g1).(i)))
      done;
      scale g;
      g
    in
    let inductive_step k c gen =
      let (gen1, gen2, gen3) = partition c gen in
      let gen' = List.fold_left (fun res (v,sat) -> (v,k::sat)::res) gen1 gen2 in
      match (gen1, gen3) with
        | ([], _) | (_, []) -> gen'
        | _ -> 
            let set = Set.create (List.length gen) in
            List.iter (fun (v,_) -> Set.add set v) gen2;
            List.fold_left (fun res g1 -> 
              List.fold_left (fun res g2 ->
                let (adj, sat_inter) = are_adjacent gen g1 g2 in
                if adj then 
                  let g = combine c g1 g2 in
                  if not (Set.mem set g) then begin
                    Set.add set g;
                    (g, k::sat_inter)::res
                  end
                  else res
                else res) res gen3) gen' gen1
    in
    let rec pseudo_vertices_aux res k cons = 
      match cons with
        | [] -> res
        | c::cons' -> pseudo_vertices_aux (inductive_step k c res) (k-1) cons'
    in
    let gen0 = initial_pointed_cone () in
    let cons = to_cons () in
    let res = pseudo_vertices_aux gen0 (-1) cons in
    let res' = List.map (fun (v,_) -> 
      let w = V.make dim S.zero in
      let x = v.(0) in
      assert (N.compare x N.zero <> 0);
      V.set w 0 S.one;
      for i = 1 to (dim-1) do
        V.set w i (S.of_num (N.div v.(i) x))
      done; w) res
    in
    res'
  
  let centroid c = 
    let pseudo_vtx = pseudo_vertices c in
    match pseudo_vtx with
      | [] -> None
      | _ ->
          let dim = c.dim in
          let p = List.length pseudo_vtx in
          let v = V.make dim S.zero in
          for i = 0 to (dim-1) do
            let x = List.fold_left (fun res v -> N.add res (S.to_num (V.get v i))) N.zero pseudo_vtx in 
            V.set v i (S.of_num (N.div x (N.of_int p)))
          done;
          Some v

  let print fmt c =
    let dim = c.dim in
    match c.mat with
      | None -> 
          for i = 0 to (dim-1) do
            for j = 0 to (dim-1) do
              if i = j then
                Format.fprintf fmt "+oo @?"
              else
                Format.fprintf fmt "0 @?"
            done;
            Format.fprintf fmt "@."
          done
      | Some m ->
          for i = 0 to (dim-1) do
            for j = 0 to (dim-1) do
              Format.fprintf fmt "%s @?" (S.to_string (get dim m i j))
            done;
              Format.fprintf fmt "@."
          done

  let polymake_print name fmt c =
    let dim = c.dim in
    Format.fprintf fmt "$%s = new Polytope;@." name;
    Format.fprintf fmt "$%s->INEQUALITIES = <<\".\";@." name;
    match c.mat with
      | None -> 
          Format.fprintf fmt "-1 @?";
          for i = 1 to (dim-2) do
            Format.fprintf fmt "0 @?"
          done;
          Format.fprintf fmt "0.@..@."
      | Some m -> 
          for i = 0 to (dim-1) do 
            for j = 0 to (dim-1) do
              if j <> i then
                let y = get dim m i j in
                if y <>% S.zero then
                  let cons = V.make dim S.one in
                  V.set cons i (S.of_int 1);
                  V.set cons j (S.of_int (-1));
                  V.set cons 0 (~-% y);
                  for k = 0 to (dim-2) do
                    Format.fprintf fmt "%s @?" (S.to_string (V.get cons k))
                  done;
                  Format.fprintf fmt "%s@." (S.to_string (V.get cons (dim-1)))
            done
          done;
          Format.fprintf fmt ".@."           
end
