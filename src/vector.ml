(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module type T =
sig
  type t 
  type scalar_t
  val make : int -> scalar_t -> t
  val copy : t -> t
  val get : t -> int -> scalar_t
  val set : t -> int -> scalar_t -> unit
  val size : t -> int
  val equal : t -> t -> bool
  val projectively_equal : t -> t -> bool
  val scale : t -> unit
  val is_leq : t -> t -> bool
  val add : t -> t -> t -> unit
  val mul : t -> scalar_t -> t -> unit
  val addmul : t -> scalar_t -> t -> unit
  val scalar : t -> t -> scalar_t
  val can : int -> int -> t
  val is_null : t -> bool
  val residuate : t -> t -> scalar_t
  val hilbert_distance : t -> t -> scalar_t
  val iter : (scalar_t -> int -> 'a) -> int -> int -> t -> unit
  val add_dimensions : t -> t -> int array -> int -> unit
  val remove_dimensions : t -> t -> int array -> int -> unit
  val permute_dimensions : t -> t -> int array -> unit
  val concat : t -> t -> t -> unit
  val hash : t -> int
  val polymake_print : Format.formatter -> t -> unit
  val of_string : ?neg:bool -> ?supp:scalar_t -> int -> string -> t
  val to_string : ?neg:bool -> t -> string
end

module Make(S: Semiring.T) = struct
  open S.Infixes

  type scalar_t = S.t
    
  type t = { mutable data : scalar_t array;
	     mutable len : int }

  let make n dummy = 
    { data = Array.make n dummy; len = n }

  let copy v = 
    { data = Array.copy v.data; len = v.len }

  let get v n = 
    if not (n < v.len) then invalid_arg "Vector.get: index out of bounds";
    v.data.(n)
      
  let set v n d = 
    if not (n < v.len) then invalid_arg "Vector.set: index out of bounds";
    v.data.(n) <- d
      
  let size v = v.len

  let equal v w =
    if v.len <> w.len then invalid_arg "Vector.equal: mismatched dimensions";
    try 
      for i = 0 to (v.len-1) do
	if (v.data.(i)) <>% (w.data.(i)) then 
	  raise Exit
      done;
      true
    with Exit -> false

  let scale v =
    let s = ref S.zero in
    try
      for i = 0 to (v.len-1) do
        let s' = v.data.(i) in
        try
          s := ~-% s';
          raise Exit
        with S.Pinfty -> ()
      done
    with Exit -> 
      for i = 0 to (v.len-1) do
        v.data.(i) <- !s *% v.data.(i)
      done

  let is_leq v w =
    if v.len <> w.len then invalid_arg "Vector.is_leq: mismatched dimensions";
    try
      for i = 0 to (v.len-1) do
	if v.data.(i) >% w.data.(i) then
	  raise Exit
      done;
      true
    with Exit -> false

  let add res v w = (* res := v + w *)
    if not (v.len = w.len && res.len = v.len) then invalid_arg "Vector.add: mismatched dimensions";
    for i = 0 to (res.len-1) do
      res.data.(i) <- v.data.(i) +% w.data.(i)
    done

  let mul res s v = (* res := s.v *)
    if res.len <> v.len then invalid_arg "Vector.mul: mismatched dimensions";
    for i = 0 to (res.len-1) do
      res.data.(i) <- v.data.(i) *% s
    done

  let addmul res s v = (* res := res + s.v *)
    if res.len <> v.len then invalid_arg "Vector.addmul: mismatched dimensions";
    for i = 0 to (res.len-1) do
      let x = v.data.(i) *% s in
	if x >% res.data.(i) then 
	  res.data.(i) <- x
    done

  let scalar v w = (* computes v|w *)
    if v.len <> w.len then invalid_arg "Vector.scalar: mismatched dimensions";
    let rec scalar_aux i =
      if i >= v.len then S.zero
      else
	(v.data.(i) *% w.data.(i)) +% (scalar_aux (i+1))
    in
      scalar_aux 0

  let can len i =
    let v = make len S.zero in
      v.data.(i) <- S.one;
      v

  let is_null v = 
    try
      for i = 0 to v.len-1 do
	if v.data.(i) <>% S.zero then
	  raise Exit
      done;
      true
    with Exit -> false

  let residuate v w =
    let res = ref S.zero 
    and is_pinfty = ref true in
      for i = 0 to (v.len-1) do
	if !is_pinfty then
	  try
	    res := (w.data.(i)) *% (~-% (v.data.(i)));
	    is_pinfty := false
	  with S.Pinfty -> ()
	else
	  try
	    res := S.min !res (w.data.(i) *% (~-% (v.data.(i))))
	  with S.Pinfty -> ()
      done;
      if !is_pinfty then
	raise S.Pinfty
      else
	!res 

  let hilbert_distance v w = 
    (~-% ((residuate v w) *% (residuate w v)))

  let projectively_equal v w = 
    try
      (hilbert_distance v w) =% S.one
    with S.Pinfty -> false

  let iter f inf sup v = 
    for i = inf to (sup-1) do
      f v.data.(i) i
    done

  let add_dimensions w v dim intdim =
    let k = ref intdim in
      for i = v.len downto 0 do
	if i < v.len then
	  w.data.(i + !k) <- v.data.(i);
	while (!k >= 1) && (dim.(!k-1) = i) do 
	  k := !k-1;
	  w.data.(i + !k) <- S.zero
	done
      done

  let remove_dimensions w v dim intdim =
    let k = ref 0 in
      for i = 0 to (w.len-1) do
	while (!k < intdim) && (dim.(!k) = i+ !k) do
	  k := !k+1
	done;
	w.data.(i) <- v.data.(i+ !k)
      done

  let permute_dimensions w v perm =
    for i = 0 to Array.length perm-1 do
      w.data.(i) <- v.data.(perm.(i))
    done
      
  let concat res v w =
    for i = 0 to v.len-1 do
      res.data.(i) <- v.data.(i)
    done;
    for i = 0 to w.len-1 do
      res.data.(v.len+i) <- w.data.(i)
    done

  let polymake_print fmt v = 
    for i = 0 to (v.len-1) do
      Format.fprintf fmt "%s @?" (S.to_string ~neg:true v.data.(i))
    done

  let to_string ?(neg=false) v =
    let rec to_string_aux i =
      if i >= v.len then
	"]"
      else if i = v.len - 1 then
	(S.to_string ~neg v.data.(i))^"]"
      else 
	(S.to_string ~neg v.data.(i))^","
	^(to_string_aux (i+1))
    in
      "["^(to_string_aux 0)

  let of_string ?(neg=false) ?supp len s = 
    let v = make len S.zero in
    let rec of_string_aux s j i1 =
      try
	let i2 = String.index_from s i1 ',' in
	let x = S.of_string ~neg (String.sub s i1 (i2-i1)) in
	  v.data.(j) <- x;
	  of_string_aux s (j+1) (i2+1)
      with Not_found ->
	let i2 = String.index_from s i1 ']' in
	let x = S.of_string ~neg (String.sub s i1 (i2-i1)) in
	  v.data.(j) <- x;
	j
    in
      try
	let i = of_string_aux s 0 ((String.index s '[') + 1) in
	match supp with
	  | None -> 
	    if i <> len-1 then
	      invalid_arg ("Vector.of_string: the size of the vector "
			   ^s^" is expected to have size "^(string_of_int len));
	    v
	  | Some x -> 
	    v.data.(len-1) <- x; v
      with Not_found | Failure _ -> raise (Failure ("Vector "^s^" is not well-formatted"))

  let hash v = Hashtbl.hash (Digest.string (to_string v))

end
