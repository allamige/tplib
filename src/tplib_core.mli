(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module type T =
sig
  type vector_t
  type halfspace_t
  val compute_tangent_hypergraph : ?labels:string list -> int -> vector_t list -> vector_t -> Hypergraph.t
  val compute_ext_rays : ?with_order:bool -> int -> vector_t list -> vector_t list
  val compute_ext_rays_polar : ?with_order:bool -> int -> vector_t list -> vector_t list
  val is_redundant_halfspace : halfspace_t list -> halfspace_t -> bool
  val compute_non_redundant_halfspaces : halfspace_t list -> halfspace_t list
  val compute_minimal_external_representations : halfspace_t list -> (vector_t * int list list) list
end

module Make :
  functor (S: Semiring.T) -> 
    functor (V : Vector.T with type scalar_t = S.t) -> 
      functor (H: Halfspace.T with type vector_t = V.t) -> T with type vector_t = V.t
                                                             and type halfspace_t = H.t

