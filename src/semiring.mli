(*
  TPLib: Tropical Polyhedra Library 

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module type T =
sig
  type t 
  type num_t
  exception Pinfty
  module Num : Numeric.T with type t = num_t
  val zero : t
  val one : t
  val add : t -> t -> t
  val mul : t -> t -> t
  val min : t -> t -> t
  val neg : t -> t
  val compare : t -> t -> int
  val random : int -> t
  val of_int : int -> t
  val of_num : num_t -> t
  val of_string : ?neg:bool -> string -> t
  val to_num : t -> num_t
  val to_string : ?neg:bool -> t -> string
  module Infixes : 
  sig
    val ( +% ) : t -> t -> t
    val ( *% ) : t -> t -> t
    val ( ~-% ) : t -> t
    val ( =% ) : t -> t -> bool
    val ( <=% ) : t -> t -> bool
    val ( <% ) : t -> t -> bool
    val ( >=% ) : t -> t -> bool
    val ( >% ) : t -> t -> bool
    val ( <>% ) : t -> t -> bool
  end
end

module Make : functor (N : Numeric.T) -> T with type num_t = N.t
