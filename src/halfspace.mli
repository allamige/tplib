(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module type T = sig
  type vector_t
  type t = { apex : vector_t; sectors : int list; }
  exception Unbounded_apex
  val of_inequality : ?gen:vector_t list -> vector_t -> t
  val to_inequality : t -> vector_t
  val complementary : t -> t
  val belong : t -> vector_t -> bool
  val print : ?neg:bool -> Format.formatter -> t -> unit
  val equal : t -> t -> bool
  val hash : t -> int
end

module Make :
  functor (S : Semiring.T) ->
    functor (V : Vector.T with type scalar_t = S.t) -> T with type vector_t = V.t
