(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module Scalar : Semiring.T
module Linexpr0 :
  sig
    type t
    val make : int -> t
    val copy : t -> t
    val get_size : t -> int
    val get_cst : t -> Scalar.t
    val get_coeff : t -> int -> Scalar.t
    val set_cst : t -> Scalar.t -> unit
    val set_coeff : t -> int -> Scalar.t -> unit
    val set_list : t -> (Scalar.t * int) list -> Scalar.t option -> unit
    val set_array : t -> (Scalar.t * int) array -> Scalar.t option -> unit
    val of_list : int -> (Scalar.t * int) list -> Scalar.t option -> t
    val of_array : int -> (Scalar.t * int) array -> Scalar.t option -> t
    val iter : (Scalar.t -> int -> 'a) -> t -> unit
    val residuate : t -> t -> Scalar.t
    val nb_of_terms : t -> int
    val print : (int -> string) -> Format.formatter -> t -> unit
  end
module Lincons0 :
  sig
    type typ = EQ | SUPEQ
    type t = { mutable left0: Linexpr0.t;
	       mutable right0: Linexpr0.t;
	       mutable typ: typ
	     }
    val make : Linexpr0.t -> Linexpr0.t -> typ -> t
    val copy : t -> t
    val string_of_typ : typ -> string
    val print : (int -> string) -> Format.formatter -> t -> unit
  end
module Generator0 :
  sig
    type typ = RAY | VERTEX
    type t = { mutable linexpr0: Linexpr0.t;
	       mutable typ: typ }
    val make : Linexpr0.t -> typ -> t
    val copy : t -> t
    val string_of_typ : typ -> string
    val residuate : t -> t -> Scalar.t
    val print : (int -> string) -> Format.formatter -> t -> unit
  end
module Poly0 :
  sig
    type t
    type strategy_t = GEN | CONS | LAZY
    val set_strategy : strategy_t -> unit
    val get_strategy : unit -> strategy_t
    val copy : t -> t
    val dimension : t -> int
    val bottom : int -> t
    val top : int -> t
    val is_bottom : t -> bool
    val is_top : t -> bool
    val is_leq : t -> t -> bool
    val sat_lincons : t -> Lincons0.t -> bool
    val to_lincons_array : t -> Lincons0.t array
    val to_generator_array : t -> Generator0.t array
    val join : t -> t -> t
    val meet : t -> t -> t
    val meet_lincons_array : t -> Lincons0.t array -> t
    val of_lincons_array : int -> Lincons0.t array -> t
    val of_generator_array : int -> Generator0.t array -> t
    val assign_linexpr_array : t -> int array -> Linexpr0.t array -> t
    val substitute_linexpr_array : t -> int array -> Linexpr0.t array -> t
    val add_dimensions : t -> int array -> int -> bool -> t
    val remove_dimensions : t -> int array -> int -> t
    val permute_dimensions : t -> int array option -> t
    val widen : t -> t -> t
    val narrow : t -> t -> t
    val widen_generators : t -> t -> t
    val print : (int -> string) -> Format.formatter -> t -> unit
    val print_cons : ?reduction:bool -> (int -> string) -> Format.formatter -> t -> unit
  end
