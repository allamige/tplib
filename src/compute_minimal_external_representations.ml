(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

let numerical_data = ref "ocaml_int"
let with_order = ref true
let maxplus = ref true

let compute_halfspaces d =
  let module N = (val (Numeric.of_string (!numerical_data)): Numeric.T) in 
  let module S = Semiring.Make(N) in
  let module V = Vector.Make(S) in
  let module Hs = Halfspace.Make(S)(V) in
  let module Core = Tplib_core.Make(S)(V)(Hs) in
  let dim = int_of_string d in
  let rec read_gen res =
    try
      let v = Scanf.scanf "%s\n" (fun x -> x) in
	read_gen (v::res)
    with End_of_file -> res
  in
  let gen = 
    try
      List.map (V.of_string ~neg:(not !maxplus) dim) (read_gen []) 
    with Invalid_argument _ -> 
      invalid_arg ("Rays should be provided as vectors of size "
		    ^(string_of_int dim)
		    ^". See README for further information.")
  in
  let check_finiteness g = 
    let res = ref true in
    for i = 0 to (V.size g) - 1 do
      if S.compare (V.get g i) S.zero = 0 then
	res := false
    done;
    !res
  in
  if List.exists (fun g -> not (check_finiteness g)) gen then
    invalid_arg ("Only vectors with finite entries are currently supported. See README for further information");
  let cons = Core.compute_ext_rays_polar ~with_order:!with_order dim gen in
  let hs_list = List.map (Hs.of_inequality ~gen) cons in
  let res = Core.compute_minimal_external_representations hs_list in
  List.iter (fun (a, sectors_list) ->
    Format.printf "Non-redundant apex: %s@." (V.to_string ~neg:(not !maxplus) a);
    Format.printf "Associated principal elements:@.";
    List.iter (fun sectors ->
      let first = ref true in
      List.iter (fun i -> 
        if !first then begin
          Format.printf "[";
	  first := false
        end
	else
	  Format.printf ",";
	Format.printf "%d" i) sectors;
      Format.printf "]@.") sectors_list;
    Format.printf "@.") res
      
let _ = 
  Arg.parse 
    [ 
      ("-numerical-data", Arg.String (fun s -> numerical_data := s), 
       "Set the type of numerical data used by the algorithm. Values: "
       ^(List.fold_left (fun res s -> 
	 if String.length res = 0 then s
	 else res^", "^s) "" (Numeric.get_name_of_modules ())));
      ("-min-plus", Arg.Clear maxplus,
       "Use the min-plus semiring instead of the default max-plus semiring");
      ("-no-ordering", Arg.Clear with_order, 
       "The constraints are not dynamically ordered during the execution") ]
    compute_halfspaces 
    ("Computes the structure underlying to the minimal external representations of a tropical cone given by a generating set. Version of TPLib: "^Config.version)
