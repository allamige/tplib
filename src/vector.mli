(*
  TPLib: Tropical Polyhedra Library 

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module type T =
sig
  type t 
  type scalar_t
  val make : int -> scalar_t -> t
  val copy : t -> t
  val get : t -> int -> scalar_t
  val set : t -> int -> scalar_t -> unit
  val size : t -> int
  val equal : t -> t -> bool
  val projectively_equal : t -> t -> bool
  val scale : t -> unit
  val is_leq : t -> t -> bool
  val add : t -> t -> t -> unit
  val mul : t -> scalar_t -> t -> unit
  val addmul : t -> scalar_t -> t -> unit
  val scalar : t -> t -> scalar_t
  val can : int -> int -> t
  val is_null : t -> bool
  val residuate : t -> t -> scalar_t
  val hilbert_distance : t -> t -> scalar_t
  val iter : (scalar_t -> int -> 'a) -> int -> int -> t -> unit
  val add_dimensions : t -> t -> int array -> int -> unit
  val remove_dimensions : t -> t -> int array -> int -> unit
  val permute_dimensions : t -> t -> int array -> unit
  val concat : t -> t -> t -> unit
  val hash : t -> int
  val polymake_print : Format.formatter -> t -> unit
  val of_string : ?neg:bool -> ?supp:scalar_t -> int -> string -> t
  val to_string : ?neg:bool -> t -> string
end

module Make :
  functor (S : Semiring.T) -> T with type scalar_t = S.t
