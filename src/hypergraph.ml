(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

open Format

type hedge = 
    { id: int;
      c: int; 
      head: int list;
      label: string option } 

type node = hedge list

type t = 
    { nb_node: int;
      nb_hedge: int;
      node: node array }

let create nb_node =
  { nb_node = nb_node; 
    nb_hedge = 0;
    node = Array.init nb_node (fun _ -> []) }

let add_hedge h ?label (c,tail,head) =
  let e = { id = h.nb_hedge; c = c; head = head; label = label } in
  let modify_node n =
    h.node.(n) <- e::h.node.(n)
  in
    List.iter modify_node tail;
    { h with nb_hedge = h.nb_hedge+1 }
      
let copy h =
  { h with node = Array.copy h.node }

(*let print fmt h =
  let print_list fmt l =
    Format.fprintf fmt "{ ";
    List.iter (fun i -> Format.fprintf fmt "%d " i) l;
    Format.fprintf fmt "}"
  in
  let print_hedge fmt hedge = 
    Format.fprintf fmt "--> %a" print_list hedge.head
  in
  for i = 0 to h.nb_node-1 do
    Format.fprintf fmt "%d |-> @." i;
    List.iter (fun hedge -> Format.fprintf fmt "%a" print_hedge hedge;
      match hedge.label with
        | None -> Format.fprintf fmt "@."
        | Some lbl -> Format.fprintf fmt "  (%s)@." lbl) h.node.(i)
  done*)

let print fmt h =
  let print_list fmt l =
    Format.fprintf fmt "{ ";
    List.iter (fun i -> Format.fprintf fmt "%d " i) l;
    Format.fprintf fmt "}"
  in
  let print_hedge j =
    let rec find_hedge i =
      if i >= h.nb_node then
        raise Not_found
      else
        try
          List.find (fun h -> h.id = j) h.node.(i)
        with Not_found -> find_hedge (i+1)
    in
    try
      let hedge = find_hedge 0 in
      let tail = ref [] in
      for i = (h.nb_node-1) downto 0 do
        if List.mem hedge h.node.(i) then
          tail := i::!tail
      done;
      let lbl = match hedge.label with None -> "" | Some lbl -> "     [ "^lbl^" ]" in
      Format.fprintf fmt "%a --> %a%s" print_list !tail print_list hedge.head lbl
    with Not_found -> ()
  in
  Format.fprintf fmt "%d vertices@." h.nb_node;
  for j = 0 to (h.nb_hedge-1) do
    print_hedge j;
    if j < h.nb_hedge - 1 then
      Format.fprintf fmt "@.";
  done

let visit_list h start =
  let visited = Array.init h.nb_node (fun _ -> false) in
  let counter = Array.init h.nb_hedge (fun _ -> 0) in
  let rec visited_aux res stack = 
    match stack with
      | [] -> res
      | n::stack' ->
        let res' = n::res in
        let visit_hedge stack hedge =
          counter.(hedge.id) <- counter.(hedge.id) + 1;
          if counter.(hedge.id) = hedge.c then
            List.fold_left (fun s n' -> 
              if not visited.(n') then begin
                visited.(n') <- true;
                n'::s
              end
              else
                s
            ) stack hedge.head
          else
            stack
        in
        let stack'' = List.fold_left visit_hedge stack' h.node.(n) in
        visited_aux res' stack''
  in
  List.iter (fun n -> visited.(n) <- true) start;
  visited_aux [] start

let visit h start = visit_list h [start]

type call_stack = 
    { mutable stack: (int * int) list;
      in_stack: bool array }
	
let empty_stack n = { stack = []; in_stack = Array.init n (fun _ -> false) }

let is_empty s = (s.stack = [])

let pop s =
  match s.stack with
    | (i,u)::s' -> 
	s.stack <- s';
	s.in_stack.(u) <- false;
	(i,u)
    | _ -> assert false

let push s (i,u) =
  s.in_stack.(u) <- true;
  s.stack <- (i,u)::s.stack
	
let contains s u = s.in_stack.(u)

exception No_greatest_scc

let max_scc_count h =
  let n = ref 0 and nb = ref 0 in
  let s = empty_stack h.nb_node in
  let comp = Array.init h.nb_node (fun _ -> false) in

  let root = Array.init h.nb_node (fun _ -> -1)
  and is_max = Array.init h.nb_node (fun _ -> true) 
  and f = Array.init h.nb_node (fun _ -> []) in

  let r = Array.init h.nb_hedge (fun _ -> -1) 
  and counter = Array.init h.nb_hedge (fun _ -> 0) in

  let p = Union_find.makesets h.nb_node in

  let rec visit_singleton u = 
    let u' = Union_find.find p u in
    let i = !n in root.(u') <- !n; incr n;
      is_max.(u') <- true; push s (i,u');
      let update_hedge hedge =
	if r.(hedge.id) = -1 then
	  r.(hedge.id) <- u';
	let re = Union_find.find p r.(hedge.id) in
	  if contains s re then begin
	    counter.(hedge.id) <- counter.(hedge.id) + 1;
	    if counter.(hedge.id) = hedge.c then
	      f.(re) <- List.rev_append (hedge.head) f.(re) 
	  end
      in
	List.iter update_hedge h.node.(u);
	let dst = f.(u') in f.(u') <- [];
	  visit (i,u') dst
  and visit (i,u) dst = 
    let visit_aux w = 
      let w' = Union_find.find p w in
	if root.(w') = -1 then 
	  visit_singleton w;
	if comp.(w') then
	  is_max.(u) <- false
	else begin
	  root.(u) <- min root.(u) root.(w');
	  is_max.(u) <- is_max.(u) && is_max.(w')
	end
    in
      List.iter visit_aux dst;
      if root.(u) = i then begin
	if is_max.(u) then begin
	  let rec pop_aux (u,dst) (j, v) =
	    if j > i then begin
	      let dst' = List.rev_append f.(v) dst in f.(v) <- [];
		let u' = Union_find.union p u v in
		let (j,v) = pop s in
		  pop_aux (u',dst') (j,v)
	    end
	    else
	      (u,dst) 
	  in
	  let dst = f.(u) in f.(u) <- [];
	    let (j,v) = pop s in
	    let (u',dst') = pop_aux (u,dst) (j,v) in
	      if dst' = [] then begin
		comp.(u) <- true;
		incr nb;
		if !nb >= 2 then
		  raise No_greatest_scc 
	      end
	      else begin
		push s (i,u');
		visit (i,u') dst'
	      end
	end
	else begin
	  let rec pop_aux () = 
	    let (j,v) = pop s in
	      comp.(v) <- true;
	      if j > i then
		pop_aux ()
	  in
	    pop_aux ()
	end
      end
  in
    for u = 0 to h.nb_node-1 do
      if root.(u) = -1 then
	visit_singleton u
    done;
    !nb

let has_greatest_scc h =
  try
    (max_scc_count h = 1) 
  with No_greatest_scc -> false
