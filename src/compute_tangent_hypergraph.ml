(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

let numerical_data = ref "ocaml_int"
let maxplus = ref true
type spec_t = GEN | INEQ | HS
let spec = ref INEQ
	
let list_of_string f sep s = 
  let n = String.length s in
  let rec list_of_string_aux pos = 
    try
      let new_pos = String.index_from s (pos+1) sep in
      (f (String.sub s (pos+1) (new_pos-(pos+1))))::(list_of_string_aux new_pos)
    with Not_found ->
      [f (String.sub s (pos+1) (n-(pos+1)))]
  in
  list_of_string_aux (-1)
      
let halfspace_of_string s =
  let n = String.length s in
  if n = 0 || s.[0] <> '(' || s.[n-1] <> ')' then
    invalid_arg ("The half-space "^s^" does not have the correct format ([x_1,...,x_n],[i_1,...,i_p]).")
  else 
    let s' = String.sub s 1 (n-2) in
    let res = list_of_string (fun x -> x) '[' s' in
    match res with 
      | [s0;s1;s2] when String.length s0 = 0 -> begin
          let n1 = String.length s1
          and n2 = String.length s2 in
          if n1 <= 1 || s1.[n1-2] <> ']' || s1.[n1-1] <> ',' || n2 = 0 || s2.[n2-1] <> ']' then
            invalid_arg ("The half-space "^s^" does not have the correct format ([x_1,...,x_n],[i_1,...,i_p]).");
          let s1' = String.sub s1 0 (n1-1) 
          and s2' = String.sub s2 0 (n2-1) in
          try
            ("["^s1', list_of_string int_of_string ',' s2')
          with _ -> invalid_arg ("The half-space "^s^" does not have the correct format ([x_1,...,x_n],[i_1,...,i_p]), where i_1,...,i_p are integers.")
      end
      | _ -> invalid_arg ("The half-space "^s^" does not have the correct format ([x_1,...,x_n],[i_1,...,i_p]).")

let compute_tangent_hypergraph d x =
  let module N = (val (Numeric.of_string (!numerical_data)): Numeric.T) in 
  let module S = Semiring.Make(N) in
  let module V = Vector.Make(S) in
  let module Hs = Halfspace.Make(S)(V) in
  let module Core = Tplib_core.Make(S)(V)(Hs) in
  let dim = int_of_string d in
  let inverse_inequality c = (* transforms a x <= b x into a x >= b x *)
    for i = 0 to (dim-1) do
      let v = V.get c i in
      V.set c i (V.get c (i+dim));
      V.set c (i+dim) v
    done
  in
  let rec read_input res =
    try
      let v = Scanf.scanf "%s\n" (fun x -> x) in
      read_input (v::res)
    with End_of_file -> res
  in
  let input = List.rev (read_input []) in
  let (labels,cons) = 
    match !spec with
      | INEQ -> 
          let cons = 
            try List.map (V.of_string ~neg:(not !maxplus) (2*dim)) input
            with Invalid_argument _ -> 
              invalid_arg ("Inequalities should be provided as vectors of size "
		           ^(string_of_int (2*dim))
		           ^". See README for further information.")
          in
          if not !maxplus then
            List.iter inverse_inequality cons;
          (input,cons)
      | GEN -> 
          let gen = 
            try List.map (V.of_string ~neg:(not !maxplus) dim) input
            with Invalid_argument _ -> 
              invalid_arg ("Generators should be provided as vectors of size "
		           ^(string_of_int (dim))
		           ^". See README for further information.")
          in
          ([], Core.compute_ext_rays_polar ~with_order:true dim gen) 
      | HS ->
          let input' = List.map halfspace_of_string input in
          let hs = 
            try
              List.map (fun (v,l) -> 
                { Hs.apex = V.of_string ~neg:(not !maxplus) dim v; 
                  Hs.sectors = List.sort compare l }) input'
            with Invalid_argument _ ->
              invalid_arg ("Apices should be provided as vectors of size "
		           ^(string_of_int (dim))
		           ^". See README for further information.")
          in
          (input, List.map Hs.to_inequality hs)
  in
  let v = 
    try
      V.of_string ~neg:(not !maxplus) dim x 
    with Invalid_argument _ ->
      invalid_arg ("The vector at which the tangent hypergrah is computed should have size "
		   ^(string_of_int (dim))
		   ^". See README for further information.")
  in
  let h = Core.compute_tangent_hypergraph ~labels:labels dim cons v in
  Format.printf "%a@." Hypergraph.print h

let _ = 
  let n = ref 0 and d = ref "" and x = ref "" in
  let parse_anonymous_arg s =
    if !n = 0 then 
      d := s
    else if !n = 1 then
      x := s
    else 
      invalid_arg "compute_tangent_hypergraph requires two arguments (dimension and vector)";
    n := !n+1
  in
  Arg.parse 
    [ 
      ("-numerical-data", Arg.String (fun s -> numerical_data := s), 
       "Set the type of numerical data used by the algorithm. Values: "
       ^(List.fold_left (fun res s -> 
	 if String.length res = 0 then s
	 else res^", "^s) "" (Numeric.get_name_of_modules ())));
      ("-generators", Arg.Unit (fun () -> spec := GEN), "Specify the cone from a generating family");
      ("-half-spaces", Arg.Unit (fun () -> spec := HS), "Specify the cone from half-spaces");
      ("-min-plus", Arg.Clear maxplus,
       "Use the min-plus semiring instead of the default max-plus semiring")
    ]
    parse_anonymous_arg 
    ("Computes the tangent directed hypergraph at a given point in a tropical cone (see README). Version of TPLib: "^(Config.version));
  compute_tangent_hypergraph !d !x
