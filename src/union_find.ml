(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

type elt =
  { rank: int;
    parent: int }

type t = elt array

let makesets n =
  Array.init n (fun i -> { rank = 0; parent = -1})

let rec find p x =
  if p.(x).parent = -1 then
    x
  else begin
    let xroot = find p p.(x).parent in
      p.(x) <- { p.(x) with parent = xroot};
      xroot
  end

let union p x y =
  let xroot = find p x 
  and yroot = find p y in
    if p.(xroot).rank > p.(yroot).rank then begin
      p.(yroot) <- { p.(yroot) with parent = xroot };
      xroot
    end
    else if p.(xroot).rank < p.(yroot).rank then begin
      p.(xroot) <- { p.(xroot) with parent = yroot };
      yroot
    end
    else if xroot <> yroot then begin
      p.(yroot) <- { p.(yroot) with parent = xroot };
      p.(xroot) <- { p.(xroot) with rank = p.(xroot).rank+1 };
      xroot
    end
    else
      xroot
	
