(*
  TPLib: Tropical Polyhedra Library 

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module type T = 
sig
  type vector_t
  type cell_t 
  type type_t = (vector_t list) array
  type t = { bounded: cell_t list;
             unbounded: cell_t list }

  val compute_cell_decomposition : int -> vector_t list -> t
  val compute_bounded_cells : ?is_pure:bool -> int -> vector_t list -> cell_t list
  val compute_boundary_cells : ?is_pure:bool -> int -> vector_t list -> cell_t list 
  val compute_pseudo_vertices : ?is_pure:bool -> int -> vector_t list -> vector_t list
  val compute_type_of_vector : vector_t list -> vector_t -> type_t
  val compute_type_of_cell : vector_t list -> cell_t -> type_t
  val compute_extremality_type : vector_t list -> vector_t -> int list
  val hull_dimension : cell_t list -> int
  val is_pure : cell_t list -> bool
  val is_in_boundary : type_t -> bool
  val print_type : Format.formatter -> ?env:(vector_t * int) list -> type_t -> unit
end

module Make : 
  functor (S: Semiring.T) -> 
    functor (V : Vector.T with type scalar_t = S.t) -> 
      functor (C: Polytrope.T  with type scalar_t = S.t and type vector_t = V.t) -> 
        T with type vector_t = V.t and type cell_t = C.t
