(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

let numerical_data = ref "ocaml_int"
let with_order = ref true
let maxplus = ref true

let compute_ext_rays d =
  let module N = (val (Numeric.of_string (!numerical_data)): Numeric.T) in 
  let module S = Semiring.Make(N) in
  let module V = Vector.Make(S) in
  let module H = Halfspace.Make(S)(V) in
  let module Core = Tplib_core.Make(S)(V)(H) in
  let dim = int_of_string d in
  let inverse_inequality c = (* transforms b x >= a x into a x <= b x *)
    for i = 0 to (dim-1) do
      let v = V.get c i in
      V.set c i (V.get c (i+dim));
      V.set c (i+dim) v
    done
  in
  let rec read_gen res =
    try
      let v = Scanf.scanf "%s\n" (fun x -> x) in
	read_gen (v::res)
    with End_of_file -> res
  in
  let gen = 
    try
      List.map (V.of_string ~neg:(not !maxplus) dim) (read_gen []) 
    with Invalid_argument _ -> 
      invalid_arg ("Rays should be provided as vectors of size "
		    ^(string_of_int dim)
		    ^". See README for further information.")
  in
  let cons = Core.compute_ext_rays_polar ~with_order:!with_order dim gen in
  if not !maxplus then
    List.iter inverse_inequality cons;
  (*Format.printf "Found %d extreme rays:@." (List.length cons);*)
  List.iter (fun g -> Format.printf "%s@." (V.to_string ~neg:(not !maxplus) g)) cons
    
let _ = 
  Arg.parse 
    [ 
      ("-numerical-data", Arg.String (fun s -> numerical_data := s), 
       "Set the type of numerical data used by the algorithm. Values: "
       ^(List.fold_left (fun res s -> 
	 if String.length res = 0 then s
	 else res^", "^s) "" (Numeric.get_name_of_modules ())));
      ("-min-plus", Arg.Clear maxplus,
       "Use the min-plus semiring instead of the default max-plus semiring");
      ("-no-ordering", Arg.Clear with_order, 
       "The constraints are not dynamically ordered during the execution") ]
    compute_ext_rays 
    ("Computes the extreme rays of the polar of a tropical cone given by a generating set (see README). Version of TPLib: "^Config.version)

    
