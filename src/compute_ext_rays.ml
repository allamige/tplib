(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

let numerical_data = ref "ocaml_int"
let with_order = ref true
let maxplus = ref true
let polymake_output = ref None
let bounding_box = ref None

let list_of_string f sep s = 
  let n = String.length s in
  let rec list_of_string_aux pos = 
    try
      let new_pos = String.index_from s (pos+1) sep in
      (f (String.sub s (pos+1) (new_pos-(pos+1))))::(list_of_string_aux new_pos)
    with Not_found ->
      [f (String.sub s (pos+1) (n-(pos+1)))]
  in
  list_of_string_aux (-1)
      
let triple_of_string s =
  let n = String.length s in
  if s.[0] <> '(' || s.[n-1] <> ')' then
    invalid_arg ("The triple "^s^" does not have the correct format (x:y:z)")
  else 
    let s' = String.sub s 1 (n-2) in
    let res = list_of_string (fun x -> x) ':' s' in
    match res with 
      | [x;y;z] -> (x,y,z)
      | _ -> invalid_arg ("The triple "^s^" does not have the correct format (x:y:z)")
	
let compute_ext_rays d =
  let module N = (val (Numeric.of_string (!numerical_data)): Numeric.T) in 
  let module S = Semiring.Make(N) in
  let module V = Vector.Make(S) in
  let module H = Halfspace.Make(S)(V) in
  let module Core = Tplib_core.Make(S)(V)(H) in
  let dim = int_of_string d in
  let inverse_inequality c = (* transforms a x <= b x into a x >= b x *)
    for i = 0 to (dim-1) do
      let v = V.get c i in
      V.set c i (V.get c (i+dim));
      V.set c (i+dim) v
    done
  in
  let build_bounding_box () =
    let build_bounding_box_aux (c,l,u) = 
      let c = int_of_string c in
      assert (1 <= c && c < dim);
      let cons = 
	if String.compare l "-oo" = 0 then
	  []
	else
	  let v = V.make (2*dim) S.zero in
	  if !maxplus then begin
	    V.set v c S.one;
	    V.set v dim (S.of_string l)
	  end
	  else begin
	    V.set v 0 (S.of_string ~neg:true l);
	    V.set v (dim+c) S.one
	  end;
	  [v]
      in 
      if String.compare u "+oo" = 0 then
	cons
      else 
	let v = V.make (2*dim) S.zero in
	if !maxplus then begin
	  V.set v (dim+c) S.one;
	  V.set v 0 (S.of_string u)
	end
	else begin
	  V.set v dim (S.of_string ~neg:true u);
	  V.set v c S.one
	end;
	v::cons
    in
    match !bounding_box with
      | None -> []
      | Some s -> 
	  let box = list_of_string triple_of_string ',' s in
	  List.fold_left (fun res (c,l,u) -> 
	  List.rev_append (build_bounding_box_aux (c,l,u)) res) [] box
  in
  let rec read_cons res =
    try
      let v = Scanf.scanf "%s\n" (fun x -> x) in
      read_cons (v::res)
    with End_of_file -> res
  in
  let cons = 
    try
      List.map (V.of_string ~neg:(not !maxplus) (2*dim)) (read_cons [])
    with Invalid_argument _ -> 
      invalid_arg ("Inequalities should be provided as vectors of size "
		   ^(string_of_int (2*dim))
		   ^". See README for further information.")
  in
  if not !maxplus then
    List.iter inverse_inequality cons;
  let cons = List.rev_append (build_bounding_box ()) cons in
  let gen = Core.compute_ext_rays ~with_order:!with_order dim cons in
  (*Format.printf "Found %d extreme rays:@." (List.length gen);*)
  match !polymake_output with 
    | None -> List.iter (fun g -> Format.printf "%s@." (V.to_string ~neg:(not !maxplus) g)) gen
    | Some s ->
      let coord = list_of_string int_of_string ',' s in
      List.iter (fun c -> if c < 0 || c >= dim then
	  invalid_arg ("Coordinates specified for output to polymake should be between 0 and "^d)) coord;		
      let n = List.length coord in
      let gen' = 
	if n = 0 then
	  gen 
	else
	  List.map (fun g -> let g' = V.make n S.zero in
			     let j = ref 0 in
			     List.iter (fun i -> 
			       let c = V.get g i in
			       if S.compare c S.zero = 0 then
				 invalid_arg "The input is not bounded. Please use -bounding-box option to get a valid polymake output.";
			       V.set g' (!j) c; j := (!j)+1) coord;
			     g') gen
      in
      Format.printf "application \"tropical\";@.";
      Format.printf "$p = new TropicalPolytope;@.";
      Format.printf "$p->POINTS=<<\".\";@.";
      List.iter (fun g -> 
	for i = 0 to (n-1) do 
	  Format.printf "%s " (S.to_string ~neg:(!maxplus) (V.get g i))
	done;
	Format.printf "@.") gen';
      Format.printf ".@."

let _ = 
  Arg.parse 
    [ 
      ("-numerical-data", Arg.String (fun s -> numerical_data := s), 
       "Set the type of numerical data used by the algorithm. Values: "
       ^(List.fold_left (fun res s -> 
	 if String.length res = 0 then s
	 else res^", "^s) "" (Numeric.get_name_of_modules ())));
      ("-min-plus", Arg.Clear maxplus,
       "Use the min-plus semiring instead of the default max-plus semiring");
      ("-no-ordering", Arg.Clear with_order, 
       "The constraints are not dynamically ordered during the execution");
      ("-polymake-output", Arg.String (fun s -> polymake_output := Some s),
       "Output under polymake format. Takes an argument, corresponding to a list c_1,...,c_p of coordinates on which the result is projected.");
      ("-bounding-box", Arg.String (fun s -> bounding_box := Some s), 
       "Set a bounding box, specified as a list (separated by commas) of triples (c:l:u), where c is the index of a coordinate, and l and u are lower and upper bounds (-oo/+oo are allowed)")
    ]
    compute_ext_rays 
    ("Computes the extreme rays of a tropical cone given by a system of tropically affine inequalities (see README). Version of TPLib: "^(Config.version))
