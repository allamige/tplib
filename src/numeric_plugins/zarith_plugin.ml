(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module Zarith_int = struct
  include Z
    
  let random n = 
    Z.of_string (Random_generator.to_string n)

end

module Zarith_rat = struct
  let pow x n = 
    let rec pow_aux x n =
      assert (n >= 0);
      if n = 0 then 
        Q.of_int 1
      else
        Q.mul x (pow_aux x (n-1))
      (*if n = 0 then 
        Q.of_int 1
      else 
        let y = pow_aux x (n/2) in
        let z = Q.mul y y in
        let res = 
          if n mod 2 = 0 then
            z
          else
            Q.mul x z
        in
        Format.printf "pow_aux x %d = @?" n;
        Format.printf "%s@." (Q.to_string res);
        res*)
    in
    if n >= 0 then 
      pow_aux x n 
    else 
      pow_aux (Q.inv x) (-n) 

  include Q
        
  let random n =
    let num = Z.of_string (Random_generator.to_string n) in
    let den = Z.add Z.one (Z.of_string (Random_generator.to_string n)) in
    Q.make num den
end

let _ =
  Numeric_plugin.add_module "zarith_int" (module Zarith_int: Numeric_plugin.T);
  Numeric_plugin.add_module "zarith_rat" (module Zarith_rat: Numeric_plugin.T)

