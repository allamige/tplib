(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

open Gmp

let bits_of_string s =
  let s' = String.sub s 2 ((String.length s)-2) in (* remove prefix "0b" *)
  Z.from_string_base ~base:2 s'

module Mlgmp_int =
struct
  type t = Z.t

  let zero = Z.of_int 0
  let max x y = if Z.cmp x y >= 0 then x else y
  let min x y = if Z.cmp x y >= 0 then y else x
  let add = Z.add
  let neg = Z.neg
  let mul = Z.mul
  let div = Z.cdiv_q
  let pow = Z.pow_ui
  let compare = Z.cmp
  let random n =
    bits_of_string (Random_generator.to_string n)
  let of_int = Z.from_int
  let of_string x =
    try
      Z.from_string_base ~base:10 x
    with Invalid_argument _ ->
      raise (Failure ("Mpzf.of_string: "^x^" is not well-formatted"))
  let to_string = Z.to_string_base ~base:10
end

module Mlgmp_rat =
struct
  type t = Q.t

  let zero = Q.from_int 0
  let one = Q.from_int 1
  let max x y = if Q.cmp x y >= 0 then x else y
  let min x y = if Q.cmp x y >= 0 then y else x
  let add = Q.add
  let neg = Q.neg
  let mul = Q.mul
  let div = Q.div

  let pow x n =
    let rec pow_aux x n =
      assert (n >= 0);
      if n = 0 then one
      else
        let y = pow_aux x (n/2) in
        let z = mul y y in
        if n mod 2 = 0 then
          z
        else
          mul x z
    in
    if n >= 0 then
      pow_aux x n
    else
      pow_aux (Q.inv x) (-n)

  let compare = Q.cmp
  let random n =
    let num = bits_of_string (Random_generator.to_string n) in
    let den = bits_of_string (Random_generator.to_string n) in
    Q.div (Q.from_z num) (Q.add Q.one (Q.from_z den))
  let of_int = Q.from_int
  let of_string x =
    try
      let i = String.index x '/' in
      let num = Z.from_string_base ~base:10 (String.sub x 0 i) in
      let den = Z.from_string_base ~base:10 (String.sub x (i+1) ((String.length x)-(i+1))) in
      Q.div (Q.from_z num) (Q.from_z den)
    with Not_found -> Q.from_z (Z.from_string_base ~base:10 x)
  let to_string = Q.to_string
end

let _ =
  Numeric_plugin.add_module "mlgmp_int" (module Mlgmp_int: Numeric_plugin.T);
  Numeric_plugin.add_module "mlgmp_rat" (module Mlgmp_rat: Numeric_plugin.T)
