(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

type state = Random.State.t

let random_state = ref (Random.State.make_self_init ())

let init seed =
  random_state := Random.State.make [| seed |]

let get_state () = !random_state

let set_state s =
  random_state := s

let rec exp n =
  if n = 0 then 1
  else
    let x = exp (n/2) in
    if n mod 2 = 0 then x*x
    else 2*x*x

let to_int n =
  Random.State.int !random_state n

let to_string n =
  let current_state = ref (Random.State.bits !random_state)
  and nb_bits = ref 30 in
  let get_bit () =
    if !nb_bits <= 0 then
      current_state := Random.State.bits !random_state;
    let b = (!current_state) mod 2 in
    current_state := (!current_state) / 2;
    nb_bits := !nb_bits-1;
    (b = 0)
  in
  let res = ref "" in
  for i = 1 to n do
    if get_bit () then
      res := "1"^(!res)
    else
      res := "0"^(!res)
  done;
  "0b"^(!res)
