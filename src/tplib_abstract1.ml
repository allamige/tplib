(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module Var = Apron.Var
module Environment = Apron.Environment

open Tplib_abstract0

module Scalar = Scalar

module Linexpr1 = struct
  type t = {
    mutable linexpr0: Linexpr0.t;
    mutable env: Environment.t
  }   
      
  let make env =
    { linexpr0 = Linexpr0.make (Environment.size env);
      env = env}
      
  let copy e = 
    { linexpr0 = Linexpr0.copy e.linexpr0;
      env = e.env }
      
  let set_coeff e v c =
    let d = Environment.dim_of_var e.env v in
      Linexpr0.set_coeff e.linexpr0 d c
	
  let set_cst e = Linexpr0.set_cst e.linexpr0
    
  let get_coeff e v =
    let d = Environment.dim_of_var e.env v in
      Linexpr0.get_coeff e.linexpr0 d
	
  let get_cst e = Linexpr0.get_cst e.linexpr0
    
  let set_list e l c =
    let l' = List.map (fun (x, v) -> (x, Environment.dim_of_var e.env v)) l in
      Linexpr0.set_list e.linexpr0 l' c
	
  let set_array e a c =
    let a' = Array.map (fun (x, v) -> (x, Environment.dim_of_var e.env v)) a in
      Linexpr0.set_array e.linexpr0 a' c
	
  let iter f e =
    Linexpr0.iter (fun c d -> f c (Environment.var_of_dim e.env d)) e.linexpr0
      
  let get_linexpr0 e = e.linexpr0
    
  let get_env e = e.env
    
  let print fmt e = 
    Linexpr0.print (fun d -> Var.to_string (Environment.var_of_dim e.env d)) fmt e.linexpr0
      
end

module Lincons1 = struct

  type side = LEFT | RIGHT
      
  type t = {
    mutable cons0: Lincons0.t;
    mutable env: Environment.t
  }
      
  type typ = Lincons0.typ = EQ | SUPEQ
      
  let make e1 e2 typ =
    assert (Environment.equal (Linexpr1.get_env e1) (Linexpr1.get_env e2));
    { cons0 = Lincons0.make (Linexpr1.get_linexpr0 e1) (Linexpr1.get_linexpr0 e2) typ;
      env = Linexpr1.get_env e1}
      
  let copy c =
    { cons0 = Lincons0.copy c.cons0; env = c.env}
      
  let set_coeff cons v c s =
    let d = Environment.dim_of_var cons.env v in
      match s with
	| LEFT -> Linexpr0.set_coeff cons.cons0.Lincons0.left0 d c
	| RIGHT -> Linexpr0.set_coeff cons.cons0.Lincons0.right0 d c
	    
  let set_cst cons c s =
    match s with
      | LEFT -> Linexpr0.set_cst cons.cons0.Lincons0.left0 c
      | RIGHT -> Linexpr0.set_cst cons.cons0.Lincons0.right0  c
	  
  let get_coeff cons v s =
    let d = Environment.dim_of_var cons.env v in
      match s with
	| LEFT -> Linexpr0.get_coeff cons.cons0.Lincons0.left0 d
	| RIGHT -> Linexpr0.get_coeff cons.cons0.Lincons0.right0 d
	    
  let get_cst cons s =
    match s with
      | LEFT -> Linexpr0.get_cst cons.cons0.Lincons0.left0
      | RIGHT -> Linexpr0.get_cst cons.cons0.Lincons0.right0
	  
  let set_list cons l1 c1 l2 c2 =
    List.iter (fun (x,y) -> set_coeff cons y x LEFT) l1;
    List.iter (fun (x,y) -> set_coeff cons y x RIGHT) l2;
    begin
      match c1 with
	| Some c1' -> set_cst cons c1' LEFT
	| None -> ()
    end;
    match c2 with
      | Some c2' -> set_cst cons c2' RIGHT
      | None -> ()
	  
  let set_array cons a1 c1 a2 c2 =
    Array.iter (fun (x,y) -> set_coeff cons y x LEFT) a1;
    Array.iter (fun (x,y) -> set_coeff cons y x RIGHT) a2;
    begin
      match c1 with
	| Some c1' -> set_cst cons c1' LEFT
	| None -> ()
    end;
    match c2 with
      | Some c2' -> set_cst cons c2' RIGHT
      | None -> ()
	  
  let iter f1 f2 cons =
    Linexpr0.iter (fun c d -> f1 c (Environment.var_of_dim cons.env d)) cons.cons0.Lincons0.left0;
    Linexpr0.iter (fun c d -> f2 c (Environment.var_of_dim cons.env d)) cons.cons0.Lincons0.right0
      
  let get_env cons = cons.env
  let get_left0 cons = cons.cons0.Lincons0.left0
  let get_right0 cons = cons.cons0.Lincons0.right0
  let get_left1 cons = {Linexpr1.linexpr0 = cons.cons0.Lincons0.left0; Linexpr1.env = cons.env}
  let get_right1 cons = {Linexpr1.linexpr0 = cons.cons0.Lincons0.right0; Linexpr1.env = cons.env}
    
  let print fmt cons = 
    Lincons0.print (fun d -> Var.to_string (Environment.var_of_dim cons.env d)) fmt cons.cons0

end

module Generator1 = struct

  type typ = Generator0.typ = RAY | VERTEX
      
  type t = {
    mutable generator0: Generator0.t; (* the size of generator0 is intended 
				       * to be equal to (size env) + 1        *)
    mutable env: Environment.t
  }   
      
  let make e t =
    { generator0 = Generator0.make (Linexpr1.get_linexpr0 e) t;
      env = Linexpr1.get_env e }
      
  let copy g = 
    { generator0 = Generator0.copy g.generator0;
      env = g.env }
      
  let set_coeff g v c =
    let d = Environment.dim_of_var g.env v in
      Linexpr0.set_coeff g.generator0.Generator0.linexpr0 d c
	
  let set_typ g t = 
    g.generator0.Generator0.typ <- t
      
  let get_coeff g v =
    let d = Environment.dim_of_var g.env v in
      Linexpr0.get_coeff g.generator0.Generator0.linexpr0 d
	
  let get_typ g = g.generator0.Generator0.typ
    
  let set_list g l =
    List.iter (fun (x,y) -> set_coeff g y x) l
      
  let set_array g a =
    Array.iter (fun (x,y) -> set_coeff g y x) a
      
  let iter f g =
    Linexpr0.iter (fun c d -> f c (Environment.var_of_dim g.env d))
      g.generator0.Generator0.linexpr0
      
  let get_linexpr1 g = {Linexpr1.linexpr0 = g.generator0.Generator0.linexpr0; 
			Linexpr1.env = g.env}
  let get_generator0 g = g.generator0
  let get_env g = g.env
    
  let print fmt g = 
    Generator0.print (fun d -> Var.to_string (Environment.var_of_dim g.env d)) fmt 
      g.generator0
end

module Poly1 = struct
  type t = 
      { mutable poly0: Poly0.t;
	mutable env: Environment.t }

  type strategy_t = Poly0.strategy_t = GEN | CONS | LAZY

  let set_strategy = Poly0.set_strategy
  let get_strategy = Poly0.get_strategy
    
  let copy p = 
    { poly0 = Poly0.copy p.poly0;
      env = p.env }

  let size p = Environment.size p.env
    
  let bottom env =
    { poly0 = Poly0.bottom (Environment.size env);
      env = env }
      
  let top env = 
    { poly0 = Poly0.top (Environment.size env);
      env = env }
      
  let env p = p.env
  let poly0 p = p.poly0
    
  let is_bottom p = Poly0.is_bottom p.poly0
  let is_top p = Poly0.is_top p.poly0
    
  let is_leq p q = 
    assert (Environment.equal p.env q.env);
    Poly0.is_leq p.poly0 q.poly0
      
  let is_eq p q =
    assert (Environment.equal p.env q.env);
    (Poly0.is_leq p.poly0 q.poly0) && (Poly0.is_leq q.poly0 p.poly0)
      
  let sat_lincons p cons =
    assert (Environment.equal p.env (Lincons1.get_env cons));
    Poly0.sat_lincons p.poly0 cons.Lincons1.cons0
      
  let to_lincons_array p =
    let cons = Poly0.to_lincons_array p.poly0 in
      Array.map (fun c -> {Lincons1.cons0 = c; Lincons1.env = p.env}) cons
	
  let meet p q =
    assert (Environment.equal p.env q.env);
    { poly0 = Poly0.meet p.poly0 q.poly0;
      env = p.env }
      
  let meet_lincons_array p cons =
    let meet_lincons_array_aux c =
      assert (Environment.equal p.env (Lincons1.get_env c)); (* it should be improved 
							      * by introducing well-suited
							      * arrays or lists of lincons0 
							      * sharing the same environment *)
      c.Lincons1.cons0
    in
    let cons0 = Array.map meet_lincons_array_aux cons in
      { poly0 = Poly0.meet_lincons_array p.poly0 cons0;
	env = p.env }
	
  let join p q = 
    assert (Environment.equal p.env q.env);
    { poly0 = Poly0.join p.poly0 q.poly0;
      env = p.env } 
      
  let assign_linexpr_array p var expr =
    let p0' = 
      Poly0.assign_linexpr_array p.poly0 
	(Array.map (Environment.dim_of_var p.env) var)
	(Array.map Linexpr1.get_linexpr0 expr) in
      {poly0 = p0'; env = p.env} 
	
  let assign_linexpr p var expr = 
    let p0' = 
      Poly0.assign_linexpr_array p.poly0 [| Environment.dim_of_var p.env var |]
	[| Linexpr1.get_linexpr0 expr |] in
      {poly0 = p0'; env = p.env} 
	
  let substitute_linexpr_array p var expr =
    let p0' = Poly0.substitute_linexpr_array p.poly0 
      (Array.map (Environment.dim_of_var p.env) var)
      (Array.map Linexpr1.get_linexpr0 expr) in
      {poly0 = p0'; env = p.env} 
	
  let change_environment p env constrained =
    let (_, dimchange1, dimchange2) = Environment.lce_change p.env env in
    let p0 = match dimchange1 with
      | None -> p.poly0
      | Some dimchange1' -> 
	  Poly0.add_dimensions p.poly0 dimchange1'.Apron.Dim.dim 
	    (dimchange1'.Apron.Dim.intdim + dimchange1'.Apron.Dim.realdim) constrained
    in
    let p0' = match dimchange2 with
      | None -> p0
      | Some dimchange2' -> 
	  let intdim = dimchange2'.Apron.Dim.intdim+dimchange2'.Apron.Dim.realdim in
	    for i = 0 to (intdim-1) do
	      dimchange2'.Apron.Dim.dim.(i) <- dimchange2'.Apron.Dim.dim.(i)+i
	    done;
	    Poly0.remove_dimensions p0 dimchange2'.Apron.Dim.dim intdim 
    in
      {poly0 = p0'; env = env}
	
  let forget_array p var =
    let env' = Environment.remove p.env var in
    let p' = change_environment p env' false in
      change_environment p' p.env false
	
  let of_lincons_array env cons =
    let of_lincons_array_aux c =
      assert (Environment.equal env (Lincons1.get_env c)); (* it should be improved 
							    * by introducing well-suited
							    * arrays or lists of lincons0 
							    * sharing the same environment *)
      c.Lincons1.cons0
    in
    let cons0 = Array.map of_lincons_array_aux cons in
      { poly0 = Poly0.of_lincons_array (Environment.size env) cons0; env = env }
	
  let widen p q =
    assert (Environment.equal p.env q.env);
    { poly0 = Poly0.widen p.poly0 q.poly0;
      env = p.env}

  let widen_generators p q = 
    assert (Environment.equal p.env q.env);
    let res = 
      { poly0 = Poly0.widen_generators p.poly0 q.poly0;
	env = p.env}
    in
      res	
      	  
  let to_generator_array p =
    let gen = Poly0.to_generator_array p.poly0 in
      Array.map (fun g -> {Generator1.generator0 = g; Generator1.env = p.env}) gen
	
  let print fmt p =
    Poly0.print (fun d -> Var.to_string (Environment.var_of_dim p.env d)) fmt p.poly0
      
  let print_cons ?(reduction=false) fmt p =
    Poly0.print_cons ~reduction (fun d -> Var.to_string (Environment.var_of_dim p.env d)) fmt p.poly0
      
  let print_polymake box fmt p =
    let build_cons (v,c1,c2) = 
      let expr_v = Linexpr1.make p.env in
	Linexpr1.set_coeff expr_v v (Scalar.one);
	let expr_c1 = Linexpr1.make p.env in
	  Linexpr1.set_cst expr_c1 c1;
	  let expr_c2 = Linexpr1.make p.env in
	    Linexpr1.set_cst expr_c2 c2;
	    [Lincons1.make expr_v expr_c1 Lincons1.SUPEQ; Lincons1.make expr_c2 expr_v Lincons1.SUPEQ]
    in
    let cons = Array.of_list (List.fold_left (fun res x -> (build_cons x)@res) [] box) in
    let vars = List.map (fun (v,_,_) -> v) box in
    let p' = meet_lincons_array p cons in
      try
	let ilist = List.filter
	  (fun v -> Environment.typ_of_var p.env v = Environment.INT) vars
	and rlist = List.filter
	  (fun v -> Environment.typ_of_var p.env v = Environment.REAL) vars
	in
	let env' = Environment.make (Array.of_list ilist) (Array.of_list rlist) in
	let p'' = change_environment p' env' false in
	let gen = to_generator_array p'' in
	let print_polymake_aux g =
	  begin
	    match Generator1.get_typ g with
	      | Generator1.RAY -> Format.pp_print_string fmt "0 "
	      | Generator1.VERTEX -> Format.pp_print_string fmt "1 "
	  end;
	  Generator1.iter (fun c _ -> Format.pp_print_string fmt 
			       (Scalar.to_string (Scalar.neg c)); 
			       Format.pp_print_string fmt " "
			    ) g;
	  Format.pp_print_newline fmt ()
	in
	  Format.pp_print_string fmt "Polymake output:";
	  Format.pp_print_newline fmt ();
	  Format.pp_print_string fmt "application('tropical');";
	  Format.pp_print_newline fmt ();
	  Format.pp_print_string fmt "$trop_poly=new tropical::TropicalPolytope<Rational>;";
	  Format.pp_print_newline fmt ();
	  Format.pp_print_string fmt "$trop_poly->POINTS=<<\".\";";
	  Format.pp_print_newline fmt ();
	  Array.iter print_polymake_aux gen;
	  Format.pp_print_string fmt ".";
	  Format.pp_print_newline fmt ();
	  Format.pp_print_string fmt "$trop_poly->VISUAL;";
	  Format.pp_print_newline fmt ()
    with Failure _ -> assert false (* variables do not appear in the environment *)

end
