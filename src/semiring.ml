(*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*)

module type T =
sig
  type t 
  type num_t
  exception Pinfty
  module Num : Numeric.T with type t = num_t
  val zero : t
  val one : t
  val add : t -> t -> t
  val mul : t -> t -> t
  val min : t -> t -> t
  val neg : t -> t
  val compare : t -> t -> int
  val random : int -> t
  val of_int : int -> t
  val of_num : num_t -> t
  val of_string : ?neg:bool -> string -> t
  val to_num : t -> num_t
  val to_string : ?neg:bool -> t -> string
  module Infixes : 
  sig
    val ( +% ) : t -> t -> t
    val ( *% ) : t -> t -> t
    val ( ~-% ) : t -> t
    val ( =% ) : t -> t -> bool
    val ( <=% ) : t -> t -> bool
    val ( <% ) : t -> t -> bool
    val ( >=% ) : t -> t -> bool
    val ( >% ) : t -> t -> bool
    val ( <>% ) : t -> t -> bool
  end
end
    
module Make(N: Numeric.T) = struct
  type t = Minfty | Num of N.t 
  
  type num_t = N.t
  
  exception Pinfty

  module Num = N

  let zero = Minfty

  let one = Num N.zero

  let add x y = 
    match (x,y) with
      | (Minfty, _) -> y
      | (_, Minfty) -> x
      | (Num x', Num y') -> Num (N.max x' y')

  let mul x y =
    match (x,y) with
      | (Minfty, _) | (_, Minfty) -> Minfty
      | (Num x', Num y') -> Num (N.add x' y')

  let min x y = (* needed for residuation *)
    match (x,y) with
      | (Minfty, _) | (_, Minfty) -> Minfty
      | (Num x', Num y') -> Num (N.min x' y')

  let neg x = (* needed for residuation *)
    match x with
      | Minfty -> raise Pinfty
      | Num x' -> Num (N.neg x')

  let compare x y =
    match (x,y) with
      | (Minfty, Minfty) -> 0
      | (Minfty, _) -> -1
      | (_, Minfty) -> 1
      | (Num x', Num y') -> N.compare x' y'

  let random n = Num (N.random n)

  let of_int x = Num (N.of_int x)

  let of_num x = Num x
    
  let of_string ?(neg=false) x =
    if not neg then
      if String.compare x "-oo" = 0 then
	Minfty
      else Num (N.of_string x)
    else
      if String.compare x "+oo" = 0 then
	Minfty
      else Num (N.neg (N.of_string x))
	
  let to_num x = 
    match x with
      | Minfty -> assert false (* TO BE FIXED *)
      | Num y -> y

  let to_string ?(neg=false) x =
    if not neg then
      match x with
	| Minfty -> "-oo"
	| Num x' -> N.to_string x'
    else
      match x with
	| Minfty -> "+oo"
	| Num x' -> N.to_string (N.neg x')
	  
  module Infixes = struct
    let ( +% ) = add 
    let ( *% ) = mul
    let ( ~-% ) = neg
      
    let ( =% ) x y = (compare x y = 0)
    let ( <=% ) x y = (compare x y <= 0)
    let ( <% ) x y = (compare x y < 0)
    let ( >=% ) x y = (compare x y >= 0)
    let ( >% ) x y = (compare x y > 0)
    let ( <>% ) x y = (compare x y <> 0)
  end
end
