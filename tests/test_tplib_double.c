/*
  TPLib: Tropical Polyhedra Library

  Copyright (C) 2009-2013 Xavier ALLAMIGEON (xavier.allamigeon at inria.fr)
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "tplib_double.h"

int main(int argc, char **argv)
{
  poly_t *x,*y,*z,*p,*q,*r,*s;  
  int i, var[2] = {1,2}, dims[2] = {1,2}, dims2[3] = {0,2,4}, perm[5] = {1,0,4,2,3};
  matrix_t *gen,*cons,*expr,*cons2;
  init();
  printf("top:\n");  
  x = top(3);
  printf("\ndimension:\n");  
  printf("%d\n",dimension(x));
  printf("\nto_gen:\n");  
  gen = to_gen(x);
  matrix_print(gen);
  printf("\nof_gen:\n");  
  y = of_gen(3,gen);
  matrix_free(gen);
  call_gc();
  printf("\nto_cons:\n");  
  cons = to_cons(y);
  matrix_print(cons);
  expr = matrix_alloc(2,3);
  matrix_set(expr,0,0,1.);
  matrix_set(expr,0,1,-INFINITY);
  matrix_set(expr,0,2,-INFINITY);
  matrix_set(expr,1,0,5.);
  matrix_set(expr,1,1,-INFINITY);
  matrix_set(expr,1,2,-INFINITY);
  printf("\nassign:\n");  
  z = assign(x, var, expr);
  print_poly(z);
  matrix_free(expr);
  poly_free(y);
  poly_free(x);
  call_gc();
  print_poly(z);
  printf("\nadd_dimensions:\n");
  p = add_dimensions(z,dims,2,1);
  print_poly(p);
  poly_free(z);
  printf("\nremove_dimensions:\n");
  q = remove_dimensions(p,dims2,3);
  print_poly(q);
  poly_free(q);
  printf("\npermute_dimensions (null argument):\n");
  q = permute_dimensions(p,NULL);
  print_poly(q);
  poly_free(q);
  printf("\npermute_dimensions (non-null argument):\n");
  q = permute_dimensions(p,perm);
  print_poly(q);
  printf("\njoin:\n");
  r = join(p,q);
  print_poly(r);
  printf("\ncopy:\n");
  s = copy(p);
  print_poly(s);
  printf("\nmeet:\n");
  s = meet(s,r);
  print_poly(s);
  cons = to_cons(top(5));
  printf("\nsat_cons: %d\n",sat_cons(s,cons));
  printf("\nmeet_cons:\n");
  s = meet_cons(s,cons);
  print_poly(s);
  cons2 = matrix_alloc(2,6);
  matrix_set(cons2,0,0,-INFINITY);
  matrix_set(cons2,0,1,0.);
  matrix_set(cons2,0,2,-INFINITY);
  matrix_set(cons2,0,3,-6.);
  matrix_set(cons2,0,4,-INFINITY);
  matrix_set(cons2,0,5,-INFINITY);
  matrix_set(cons2,1,0,-INFINITY);
  matrix_set(cons2,1,1,-INFINITY);
  matrix_set(cons2,1,2,0.);
  matrix_set(cons2,1,3,-1);
  matrix_set(cons2,1,4,-3);
  matrix_set(cons2,1,5,-INFINITY);
  printf("\nof_cons:\n");
  print_poly(of_cons(3,cons2));
  return 0;
}


