prefix = @prefix@
exec_prefix = @exec_prefix@
bindir = @bindir@
sysconfdir = @sysconfdir@
libdir = @libdir@
datarootdir = @datarootdir@
datadir = @datadir@
includedir = @includedir@

CFLAGS=@CFLAGS@
LDFLAGS=@LDFLAGS@
CAT=@CAT@
RM=@RM@
CP=@CP@
AR=@AR@
MKDIR=@MKDIR@
RANLIB=@RANLIB@
INSTALL=@INSTALL@
INSTALL_PROGRAM=@INSTALL_PROGRAM@
INSTALL_DATA=@INSTALL_DATA@

OCAML_HOME_DIR=@OCAMLLIB@
DYNLINK=@OCAML_PKG_dynlink@
ZARITH=@OCAML_PKG_zarith@
MLGMP=@OCAML_PKG_gmp@
HAVE_BINDING=@HAVE_BINDING@
HAVE_GMP=@HAVE_GMP@
gmpinc=@gmpinc@
gmplibs=@gmplibs@

CP = cp
RANLIB = ranlib
BUILD_DIR = _build
OCAMLBUILD = @OCAMLBUILD@ -ocamlc ocp-ocamlc.opt -ocamlopt ocp-ocamlopt.opt -classic-display -no-links -build-dir $(BUILD_DIR) -use-ocamlfind
OCAMLFIND = @OCAMLFIND@
TPLIB = @PACKAGE_NAME@

OCAML_BIN = src/compute_ext_rays src/compute_ext_rays_polar \
	src/compute_halfspaces src/compute_tangent_hypergraph \
	src/compute_minimal_external_representations \
	src/compute_tropical_complex 
OCAML_LIB = src/$(TPLIB)
OCAML_LIB_PLUGIN = 
OCAML_CMI = src/numeric src/semiring src/vector src/halfspace src/hypergraph src/tplib_core src/tplib_abstract
OCAML_TEST = tests/test_core

OCAML_INSTALL_DIR = `$(OCAMLFIND) printconf destdir`
OCAML_BIN_INSTALL_DIR = ${bindir}

ifeq ($(HAVE_BINDING),yes)
	CFLAGS += -I$(OCAML_HOME_DIR)
	LDFLAGS += -L$(OCAML_HOME_DIR) -lm -ldl -lnums
	BINDING_DIR = src/bindings
	BINDING = tplib_double
	C_TEST = tests/test_tplib_double
	ifneq ($(ZARITH),no)
	ifneq ($(HAVE_GMP),no)
		BINDING += tplib_rational
		C_TEST += tests/test_tplib_rational
		CFLAGS += -I$(BINDING_DIR) -I"`$(OCAMLFIND) query $(ZARITH)`"
		CFLAGS += $(gmpinc)
		LDFLAGS += -L"`$(OCAMLFIND) query $(ZARITH)`" -lzarith 
		LDFLAGS += $(gmplibs)
	endif
	endif
endif

ifneq ($(DYNLINK),no)
	ifneq ($(ZARITH),no)
		OCAML_LIB_PLUGIN += src/numeric_plugins/zarith_plugin
	endif
	ifneq ($(MLGMP),no)
		OCAML_LIB_PLUGIN += src/numeric_plugins/mlgmp_plugin
	endif
endif

LIBASMRUN = $(OCAML_HOME_DIR)/libasmrun.a

OCAML_LIB_SRC = $(addsuffix .mllib,$(OCAML_LIB))
OCAML_LIB_PLUGIN_SRC = $(addsuffix .mldylib,$(OCAML_LIB_PLUGIN))
OCAML_CMI_SRC = $(addsuffix .mli,$(OCAML_CMI))
OCAML_BIN_SRC = $(addsuffix .ml,$(OCAML_BIN))
OCAML_BINDING_SRC = $(addprefix $(BINDING_DIR)/,$(addsuffix _callback.ml,$(BINDING)))

OCAML_LIB_TGT = $(addprefix $(BUILD_DIR)/,$(addsuffix .cmxa,$(OCAML_LIB)))
OCAML_LIB_PLUGIN_TGT = $(addprefix $(BUILD_DIR)/,$(addsuffix .cmxs,$(OCAML_LIB_PLUGIN)))
OCAML_CMI_TGT = $(addprefix $(BUILD_DIR)/,$(addsuffix .cmi,$(OCAML_CMI)))
OCAML_BIN_TGT = $(addprefix $(BUILD_DIR)/,$(OCAML_BIN))
OCAML_BINDING_TGT = $(addprefix $(BUILD_DIR)/$(BINDING_DIR)/,$(addsuffix _callback.obj.o,$(BINDING)))

BINDING_TGT = $(addprefix $(BUILD_DIR)/$(BINDING_DIR)/lib,$(addsuffix .a,$(BINDING)))
BINDING_H = $(addprefix $(BINDING_DIR)/,$(addsuffix .h,$(BINDING)))

OCAML_TEST_SRC = $(addsuffix .ml,$(OCAML_TEST))

C_TEST_TGT = $(addprefix $(BUILD_DIR)/,$(C_TEST))

all:	ocaml-all $(BINDING_TGT)

test: 	all ocaml-test $(C_TEST_TGT)

install: install-ocaml-lib install-ocaml-bin install-bindings

uninstall: uninstall-ocaml-lib uninstall-ocaml-bin uninstall-bindings

reinstall: uninstall install

install-ocaml-bin: $(OCAML_BIN_TGT)
	test -d $(OCAML_BIN_INSTALL_DIR) || install -d $(OCAML_BIN_INSTALL_DIR)
	$(INSTALL_PROGRAM) $^ $(OCAML_BIN_INSTALL_DIR)

uninstall-ocaml-bin: 
	$(RM) -f $(addprefix $(OCAML_BIN_INSTALL_DIR)/,$(notdir $(OCAML_BIN_TGT)))

install-ocaml-lib: $(OCAML_LIB_TGT) $(OCAML_CMI_TGT) $(OCAML_LIB_PLUGIN_TGT)
	$(OCAMLFIND) install -destdir $(OCAML_INSTALL_DIR) $(TPLIB) src/META \
	$(OCAML_LIB_TGT:%.cmxa=%.a) $(OCAML_CMI_TGT:%.cmi=%.mli) $^ 

uninstall-ocaml-lib:
	$(OCAMLFIND) remove -destdir $(OCAML_INSTALL_DIR) $(TPLIB)

install-bindings: $(BINDING_TGT)
	$(MKDIR) -p $(includedir)
	$(INSTALL_DATA) $(BINDING_H) $(includedir)
	$(MKDIR) -p $(libdir)
	$(INSTALL_DATA) $^ $(libdir)

uninstall-bindings:
	$(RM) -f $(addprefix $(includedir)/,$(addsuffix .h,$(BINDING)))
	$(RM) -f $(addprefix $(libdir)/lib,$(addsuffix .a,$(BINDING)))

ocaml-all: $(OCAML_LIB_SRC) $(OCAML_CMI_SRC) $(OCAML_LIB_PLUGIN_SRC) $(OCAML_BINDING_SRC) $(OCAML_BIN_SRC) 
	$(OCAMLBUILD) $(OCAML_LIB_SRC:%.mli=%.cmxa) \
		$(OCAML_CMI_SRC:%.mli=%.cmi) \
		$(OCAML_LIB_PLUGIN_SRC:%.mldylib=%.cmxs) \
		$(OCAML_BINDING_SRC:%.ml=%.obj.o) \
		$(OCAML_BIN_SRC:%.ml=%.native)	
	@for x in $(OCAML_BIN); do \
		$(CP) -f $(BUILD_DIR)/$$x.native $(BUILD_DIR)/$$x; \
	done

$(BUILD_DIR)/%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(OCAML_LIB_TGT) $(OCAML_CMI_TGT) $(OCAML_LIB_PLUGIN_TGT) $(OCAML_BIN_TGT): %: 
	@if [ ! test -f $@ ] ; then $(error Please run make first); fi

$(BINDING_TGT): $(BUILD_DIR)/$(BINDING_DIR)/lib%.a: $(BUILD_DIR)/$(BINDING_DIR)/%.o ocaml-all 
	$(CAT) $(LIBASMRUN) > $@
	$(AR) rcs $@ $(BUILD_DIR)/$(BINDING_DIR)/$*_callback.obj.o $<
	$(RANLIB) $@

ocaml-test: $(OCAML_TEST_SRC)
	$(OCAMLBUILD) $(OCAML_TEST_SRC:%.ml=%.native)

$(C_TEST_TGT): $(BUILD_DIR)/tests/test_%: tests/test_%.c 
	$(CC) -o $@ $^ -I$(BINDING_DIR) $(CFLAGS) -L$(BUILD_DIR)/$(BINDING_DIR) -l$* $(LDFLAGS) 

clean: 
	$(OCAMLBUILD) -clean

#.PHONY: all test clean ocaml-all $(OCAML_LIB_PLUGIN_TGT) $(OCAML_LIB_TGT) $(OCAML_BIN_TGT)

