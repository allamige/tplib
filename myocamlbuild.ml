open Ocamlbuild_plugin;;

dispatch begin function
  | After_rules -> 
    let ext_obj = !Options.ext_obj in 
    let x_o = "%"-.-ext_obj in   
    rule "ocaml: cmx* & o* -> .obj.o" 
      ~prod:"%.obj.o"
      ~deps:["%.cmx"; x_o] 
      (Ocamlbuild_pack.Ocaml_compiler.native_link "%.cmx" "%.obj.o");
    flag ["link";"shared"] & A"-linkpkg";
    ocaml_lib "src/tplib"
  | _ -> ()
end
